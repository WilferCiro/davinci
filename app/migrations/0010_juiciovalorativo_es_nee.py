# Generated by Django 3.0.7 on 2020-09-29 09:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_areafundamental_copy_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='juiciovalorativo',
            name='es_nee',
            field=models.BooleanField(default=False),
        ),
    ]
