# Generated by Django 3.1.2 on 2020-10-12 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_estudiante_acudiente_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estudiante',
            name='copy_id',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
