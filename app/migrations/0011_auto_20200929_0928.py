# Generated by Django 3.0.7 on 2020-09-29 09:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_juiciovalorativo_es_nee'),
    ]

    operations = [
        migrations.AlterField(
            model_name='juiciovalorativo',
            name='periodo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.Periodo'),
        ),
    ]
