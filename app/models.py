from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Q
from ckeditor.fields import RichTextField
from django.core.validators import MaxValueValidator, MinValueValidator
from app.utils.macros import *

import datetime
now = datetime.datetime.now()

FECHA_CHOOSER = []
[FECHA_CHOOSER.append([str(date), str(date)]) for date in reversed(range(2016, now.year + 1))]
#(('2016', '2016'), ('2017', '2017'), ('2018', '2018'), ('2019', '2019'), ('2020', '2020'))

class User(AbstractUser):
	ESTUDIANTE = "1";
	DOCENTE = "2";
	ADMIN = "3";
	perfil = models.CharField(
		max_length = 3,
		choices = (
			(ESTUDIANTE, "Estudiante"),
			(DOCENTE, "Docente"),
			(ADMIN, "Admin")
		), default = ESTUDIANTE,
	)
	def get_estudiante(self, year):
		try:
			est = Estudiante.objects.get(Q(user = self) & Q(year = year))
			return est
		except:
			return None

	def estudiante(self):
		try:
			est = Estudiante.objects.filter(Q(user = self)).order_by("-year").last()
			return est
		except:
			return None

	def administrador(self):
		try:
			est = Administrador.objects.filter(Q(user = self)).last()
			return est
		except:
			return None

	def docente(self):
		try:
			est = Docente.objects.filter(Q(user = self)).last()
			return est
		except:
			return None

	def es_estudiante(self):
		if self.estudiante() != None:
			return True
		return False

	def es_docente(self):
		if self.docente() != None:
			return True
		return False

	def es_admin(self):
		if self.administrador() != None:
			return True
		return False

class Sede(models.Model):
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)
	dane = models.CharField(max_length = 100, null=True, blank=True)
	hora_apertura = models.TimeField(auto_now_add = False, null=True, blank = True)
	hora_cierre = models.TimeField(auto_now_add = False, null=True, blank = True)

	def __str__(self):
		return self.titulo

class Grado(models.Model):
	titulo = models.CharField(max_length = 100)
	valor_numerico = models.IntegerField(default = 0)
	observaciones = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.titulo + " (" + str(self.valor_numerico) + ") "


class Periodo(models.Model):
	titulo = models.CharField(max_length = 20)
	fecha_inicio = models.DateTimeField(auto_now_add = False)
	fecha_final = models.DateTimeField(auto_now_add = False)
	fecha_apertura = models.DateTimeField(auto_now_add = False)
	fecha_cierre = models.DateTimeField(auto_now_add = False)
	es_final = models.BooleanField("Es final", default=False, help_text="Define si es un periodo final o no")
	orden = models.IntegerField(default = 0)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.titulo) + " (" + str(self.year) + " - " + str(self.es_final) + " - " + str(self.orden) + ")"

	def isActive(self):
		if now >= self.fecha_inicio or self.es_final:
			return True
		return False

class Docente(models.Model):
	#user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="docente")
	copy_id = models.IntegerField(default = 0)
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	email = models.EmailField(max_length = 100, null=True, blank=True)
	nombre = models.CharField(max_length = 20, null=True, blank=True)
	apellido = models.CharField(max_length = 20, null=True, blank=True)

	telefono = models.CharField(max_length = 20, null=True, blank=True)
	celular = models.CharField(max_length = 20, null=True, blank=True)
	documento = models.CharField(max_length = 20, null=True, blank=True)
	direccion = models.TextField(null=True, blank=True)
	estudios = models.TextField(null=True, blank=True)
	sede = models.ForeignKey(Sede, on_delete=models.SET_NULL, null=True)
	foto = models.ImageField(upload_to='pro/%y/%s', blank = True, null = True)
	MASC = 'M'
	FEM = 'F'
	SEXO_CHOICES = (
		(MASC, 'Masculino'),
		(FEM, 'Femenino')
	)
	sexo = models.CharField(
		max_length = 1,
		choices = SEXO_CHOICES,
		default = MASC,
		null=True, blank=True
	)

	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo')
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)

	class Meta:
		ordering = ('nombre',)

	def __str__(self):
		return self.nombre + " " + self.apellido

	def getSexo(self):
		for data in self.SEXO_CHOICES:
			if data[0] == self.sexo:
				return data[1]
		return ""

	def getEstado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return ""

	def filtroEstudiantesGrupo(self, year):
		fil = Q()
		registers = AsignacionDocente.objects.filter(Q(docente = self) & Q(year = year))
		if registers.count() == 0:
			return Q(pk = None)
		for asig in registers:
			fil = fil | Q(grupo = asig.grupo)
		return fil

	def filtroGrupos(self, year):
		fil = Q()
		registers = AsignacionDocente.objects.filter(Q(docente = self) & Q(year = year))
		if registers.count() == 0:
			return Q(pk = None)
		for asig in registers:
			fil = fil | Q(pk = asig.grupo)
		return fil

	def canAdminGroup(self, grupo, year):
		try:
			asignacion = AsignacionDocente.objects.get(Q(grupo = grupo) & Q(year = year))
			return True
		except:
			pass
		return False

	def filtroGradoAsignatura(self, year):
		fil_grados = Q()
		fil_asignaturas = Q()
		registers = AsignacionDocente.objects.filter(Q(docente = self) & Q(year = year))
		if registers.count() == 0:
			return Q(pk = None)
		for asig in registers:
			fil_grados = fil_grados | Q(grado = asig.grupo.grado)
			fil_asignaturas = fil_asignaturas | Q(asignatura = asig.asignatura)
		return fil_grados & fil_asignaturas

	def get_full_name(self):
		return str(self.nombre) + " " + str(self.apellido)

	def getProfileURL(self):
		return "/docentes/view/" + str(self.pk)

class Grupo(models.Model):
	copy_id = models.IntegerField(default = 0)
	titulo = models.CharField(max_length = 20)
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	sede = models.ForeignKey(Sede, on_delete=models.CASCADE)

	class Meta:
		ordering = ('grado__valor_numerico',)

	def __str__(self):
		return str(self.titulo) + " - " + str(self.GetSede())

	def GetSede(self):
		return str(self.sede)

	def getGrado(self):
		return str(self.grado)

	def getGrupo(self):
		return str(self.titulo) + " - " + str(self.GetSede())

	def getDirector(self, year):
		gr = GrupoDocente.objects.filter(Q(grupo = self) & Q(year = year))
		if len(gr) > 0:
			return str(gr[0].docente.get_full_name())
		return ""

	def getDirectorObj(self, year):
		gr = GrupoDocente.objects.filter(Q(grupo = self) & Q(year = year))
		if len(gr) > 0:
			return gr[0]
		return None

class GrupoDocente(models.Model):
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE, null=True, blank = True)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE, null=True, blank = True)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.docente) + " - " + str(self.grupo) + " - " + str(self.year)

class Estudiante(models.Model):
	copy_id = models.IntegerField(default = 0, blank = True, null = True)
	acudiente_id = models.IntegerField(default = 0, blank = True, null = True)
	#user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name="estudiante")
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	# Datos básicos
	foto = models.ImageField(upload_to='est/%y/%s', blank = True, null = True)
	nombre = models.CharField(max_length = 30, null=True, blank=True)
	apellido = models.CharField(max_length = 30, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)
	grupo = models.ForeignKey(Grupo, on_delete=models.SET_NULL, null=True)
	documento = models.CharField(max_length = 25, null=True, blank=True)
	TIPO_DOC_CHOICES = (
		("", ""),
		("TI", 'Tarjeta de identidad'),
		("CC", 'Cédula de ciudadanía'),
		("PA", 'Pasaporte'),
		("CE", 'Cédula de extrangería'),
		("RC", 'Registro civil'),
		("CO", 'Contraseña'),
		("NU", 'NUIP'),
		("NE", 'NES')
	)
	tipo_documento = models.CharField(
		max_length = 3,
		choices = TIPO_DOC_CHOICES,
		default = "",
		null=True, blank=True
	)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)
	municipio_expedicion = models.CharField(max_length = 30, null=True, blank=True)
	departamento_expedicion = models.CharField(max_length = 30, null=True, blank=True)
	fecha_nacimiento = models.DateField(auto_now_add = False, null = True, blank = True)
	fecha_retiro = models.DateField(auto_now_add = False, null = True, blank = True)
	municipio_nacimiento = models.CharField(max_length = 30, null=True, blank=True)
	departamento_nacimiento = models.CharField(max_length = 30, null=True, blank=True)
	direccion_residencia = models.CharField(max_length = 200, null=True, blank=True)
	municipio_residencia = models.CharField(max_length = 30, null=True, blank=True)
	departamento_residencia = models.CharField(max_length = 30, null=True, blank=True)
	celular1 = models.CharField(max_length = 30, null=True, blank=True)
	celular2 = models.CharField(max_length = 30, null=True, blank=True)
	email = models.CharField(max_length = 40, null=True, blank=True)
	aprobado = models.BooleanField("Aprobado", default=True)
	aprobado_pasado = models.BooleanField("Aprobado el año pasado", default=True)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	fecha_matricula = models.DateTimeField(auto_now_add = False, null=True, blank=True)
	MASC = 'M'
	FEM = 'F'
	SEXO_CHOICES = (
		('',''),
		(MASC, 'Masculino'),
		(FEM, 'Femenino')
	)
	sexo = models.CharField(
		max_length = 1,
		choices = SEXO_CHOICES,
		default = "",
		null=True, blank=True
	)

	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	E_RETIRADO = 'R'
	E_DESERTOR = 'D'
	E_CANCELADO = 'C'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo'),
		(E_RETIRADO, 'Retirado'),
		(E_DESERTOR, 'Desertor'),
		(E_CANCELADO, 'Cancelado'),
	)
	estado = models.CharField(
		max_length = 10,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)
	Z_RURAL = 'R'
	Z_URBANA = 'U'
	ZONA_CHOICES = (
		('',''),
		(Z_RURAL, 'Rural'),
		(Z_URBANA, 'Urbana')
	)
	zona = models.CharField(
		max_length = 1,
		choices = ZONA_CHOICES,
		default = "",
		null=True, blank=True
	)
	def get_full_name(self):
		return str(self.nombre) + " " + str(self.apellido)

	def getProfileURL(self):
		return "/estudiantes/list/view/" + str(self.pk)

	def __str__(self):
		return self.get_full_name()

	def getEdad(self):
		today = now
		age = " "
		if self.fecha_nacimiento != None:
			age = today.year - self.fecha_nacimiento.year - ((today.month, today.day) < (self.fecha_nacimiento.month, self.fecha_nacimiento.day))
		return str(age)

	def getTipoDocumento(self):
		for data in self.TIPO_DOC_CHOICES:
			if data[0] == self.tipo_documento:
				return data[1]
		return ""

	def getZona(self):
		for data in self.ZONA_CHOICES:
			if data[0] == self.zona:
				return data[1]
		return ""

	def getEstado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return ""

	def getSexo(self):
		for data in self.SEXO_CHOICES:
			if data[0] == self.sexo:
				return data[1]
		return ""

	def getNotasActivas(self, year, tabla_notas, periodo_hasta = 6, getAll = False, min_lost = 3, sim_nota = False):
		round_limit = 2

		areas_all = []
		asignaturas_all = []
		promedio_total = 0
		nro_total = 0

		filter_periodo = periodo_hasta
		if sim_nota:
			filter_periodo = 6

		areas_objects = [a for a in AreaFundamental.objects.all()]
		areas_objects.append(None)
		periodos_objects = Periodo.objects.filter(Q(year = year) & (Q(orden__lte = filter_periodo) | Q(es_final = True))).order_by("es_final", "orden")
		total_periodos = Periodo.objects.filter(Q(year = year) & Q(es_final = False)).count()
		for area in areas_objects:
			total_area = 0
			asignaciones = AsignacionDocente.objects.filter(Q(year = year) & Q(grupo = self.grupo) & Q(asignatura__area_fundamental = area))
			if len(asignaciones) > 0:
				asignaturas_area = []
				for asignacion in asignaciones:
					all_periodos = []
					nro = 0
					total = 0
					for periodo in periodos_objects:
						if ((periodo.isActive() or getAll) or sim_nota) and not periodo.es_final:
							if sim_nota and periodo.orden > periodo_hasta:
								nota_valor = (total_periodos * MIN_LOST) - total
								if nota_valor > MAX_NOTA and periodo.orden < total_periodos:
									nota_valor = MAX_NOTA
								nota = tabla_notas(valor = nota_valor)
							else:
								nota = self.getNota(tabla_notas, asignacion.asignatura, periodo)
							all_periodos.append({
								"pk" : periodo.pk,
								"titulo" : periodo.titulo,
								"habilitacion" : nota.habilitacion,
								"valor" : round(nota.valor, round_limit),
								"valor_native" : nota.valor,
								"inasistencias" : nota.inasistencias,
								"valoracion" : get_valoracion(round(nota.habilitacion if nota.valor < min_lost and nota.habilitacion >= min_lost else nota.valor, round_limit)),
								"is_final" : False,
								"perdida" : True if round(nota.valor, round_limit) < min_lost and nota.habilitacion < min_lost else False
							})
							if nota.valor < min_lost and nota.habilitacion >= min_lost:
								total += nota.habilitacion
							else:
								total += nota.valor
							nro += 1

						if periodo.es_final:
							if nro == 0:
								nro = 1

							total = total / nro
							valor = 0
							all_periodos.append({
								"pk" : periodo.pk,
								"titulo" : periodo.titulo,
								"habilitacion" : 0.0,
								"valor" : round(total, round_limit),
								"valor_native" : total,
								"valoracion" : get_valoracion(round(total, round_limit)),
								"inasistencias" : "",
								"is_final" : True,
								"perdida" : True if round(total, round_limit) < min_lost else False
							})

					titulo_asignatura = asignacion.asignatura.titulo
					spl = titulo_asignatura.split("_")
					if len(spl) > 1:
						titulo_asignatura = spl[0]
					data_append = {
						"pk" : asignacion.asignatura.pk,
						"titulo" : titulo_asignatura,
						"hs" : asignacion.hs,
						"porcentaje" : asignacion.asignatura.porcentaje,
						"valor" : round(total, 1),
						"valor_native" : total,
						"valoracion" : get_valoracion(round(total, round_limit)),
						"periodos" : all_periodos,
						"perdida" : True if round(total, round_limit) < min_lost else False
					}

					if area != None:
						asignaturas_area.append(data_append)
					else:
						asignaturas_all.append(data_append)

				periodos_area = []
				for periodo in periodos_objects:
					if periodo.isActive() or getAll:
						total_area = 0
						for asignatura in asignaturas_area:
							periodo_object = next((com for com in asignatura["periodos"] if com["pk"] == periodo.pk), False)
							if periodo_object != False:
								if float(periodo_object["valor_native"]) < min_lost and float(periodo_object["habilitacion"]) >= min_lost:
									total_area += (periodo_object["habilitacion"] * asignatura["porcentaje"]) / 100
								else:
									total_area += (periodo_object["valor_native"] * asignatura["porcentaje"]) / 100
						periodos_area.append({
							"pk" : periodo.pk,
							"titulo" : periodo.titulo,
							"habilitacion" : 0.0,
							"valor" : round(total_area, round_limit),
							"valor_native" : total_area,
							"valoracion" : get_valoracion(round(total_area, round_limit)),
							"inasistencias" : "",
							"is_final" : False,
							"perdida" : True if round(total_area, round_limit) < min_lost else False
						})
				if area != None:
					areas_all.append({
						"pk" : area.pk,
						"titulo" : area.titulo,
						"asignaturas" : asignaturas_area,
						"valor" : round(total_area, round_limit),
						"valor_native" : total_area,
						"valoracion" : get_valoracion(round(total_area, round_limit)),
						"periodos" : periodos_area,
						"perdida" : True if round(total_area, round_limit) < min_lost else False
					})
					promedio_total += total_area
					nro_total += 1

		promedios_all = []
		perdidas_all = []
		for periodo in periodos_objects:
			if periodo.isActive() or getAll:
				total_periodo = 0
				total_nro = 0
				perdidas_internal = 0
				for asignatura in asignaturas_all:
					periodo_object = next((com for com in asignatura["periodos"] if com["pk"] == periodo.pk), False)
					if periodo_object != False:
						total_periodo += periodo_object["valor_native"]
						total_nro += 1
						if periodo_object["perdida"]:
							perdidas_internal += 1
				for area in areas_all:
					periodo_object = next((com for com in area["periodos"] if com["pk"] == periodo.pk), False)
					if periodo_object != False:
						total_periodo += periodo_object["valor_native"]
						total_nro += 1
						if periodo_object["perdida"]:
							perdidas_internal += 1

				if total_nro == 0:
					total_nro = 1
				total_periodo = total_periodo / total_nro
				promedios_all.append({
					"pk" : periodo.pk,
					"titulo" : periodo.titulo,
					"valor" : round(total_periodo, round_limit),
					"valor_native" : total_periodo,
					"valoracion" : get_valoracion(round(total_periodo, round_limit)),
					"is_final" : False,
					"perdida" : True if round(total_periodo, round_limit) < min_lost else False
				})
				perdidas_all.append({
					"pk" : periodo.pk,
					"titulo" : periodo.titulo,
					"valor" : perdidas_internal
				})



		data_return = {
			"areas" : areas_all,
			"asignaturas" : asignaturas_all,
			"promedios" : promedios_all,
			"perdidas" : perdidas_all
		}

		return data_return


	def updateNota(self, asignatura, periodo, tabla_notas, valor, inasistencias, habilitacion = 0.0):
		nota = self.getNota(tabla_notas, asignatura, periodo)
		nota.valor = valor
		nota.inasistencias = inasistencias
		nota.habilitacion = habilitacion
		nota.save()
		return nota

	def getNota(self, tabla_notas, asignatura, periodo):
		nota = tabla_notas.objects.filter(Q(asignatura = asignatura) & Q(periodo = periodo) & Q(estudiante = self))
		if len(nota) == 0:
			nota = tabla_notas(asignatura = asignatura, periodo = periodo, estudiante = self, valor = 0.0, inasistencias = 0)
			nota.save()
		else:
			nota = nota[0]
		return nota

	def getAcudienteData(self):
		return []

	def getAcudiente(self):
		try:
			data = EstudianteAcudiente.objects.get(estudiante = self)
			return data.acudiente
		except EstudianteAcudiente.DoesNotExist:
			acudiente = Acudiente()
			acudiente.year = self.year
			acudiente.save()
			acudiente_estudiante = EstudianteAcudiente(estudiante = self, acudiente = acudiente)
			acudiente_estudiante.save()
			return acudiente

	def getSalud(self):
		try:
			salud = EstudianteSalud.objects.get(estudiante = self)
		except EstudianteSalud.DoesNotExist:
			salud = EstudianteSalud(estudiante = self)
			salud.save()
		return salud

	def getAdicional(self):
		try:
			adicional = EstudianteAdicional.objects.get(estudiante = self)
		except EstudianteAdicional.DoesNotExist:
			adicional = EstudianteAdicional(estudiante = self)
			adicional.save()
		return adicional

	def filtroDocentes(self):
		fil = Q()
		for asig in AsignacionDocente.objects.filter(grupo = self.grupo):
			fil = fil | Q(grupo = asig.docente)
		return fil


class EstudianteSalud(models.Model):
	estudiante = models.OneToOneField(Estudiante, on_delete=models.CASCADE, primary_key=True, related_name="salud")
	# Datos de salud
	SANGRE = (
		("", ""), ("O+", 'O+'), ("O-", 'O-'), ("A+", 'A+'), ("A-", 'A-'), ("B+", 'B+'), ("AB+", 'AB+'), ("AB-", 'AB-')
	)
	tipo_sangre = models.CharField(
		max_length = 3,
		choices = SANGRE,
		default = "", null=True, blank=True
	)
	eps = models.CharField(max_length = 50, null=True, blank=True)
	puntaje_sisben = models.CharField(max_length = 20, null=True, blank=True)
	ars = models.CharField(max_length = 50, null=True, blank=True)
	alergias = models.CharField(max_length = 80, null=True, blank=True)
	medicamentos = models.CharField(max_length = 80, null=True, blank=True)
	enfermedades = models.CharField(max_length = 80, null=True, blank=True)
	discapacidades = models.CharField(max_length = 80, null=True, blank=True)

	def __str__(self):
		return str(self.estudiante.get_full_name())

	def getTipoSangre(self):
		for data in self.SANGRE:
			if data[0] == self.tipo_sangre:
				return data[1]
		return ""

	def getLimitaciones(self):
		limitaciones = []
		for lim in LimitacionEstudiante.objects.filter(Q(estudiante = self.estudiante)):
			limitaciones.append(lim.limitacion)
		return limitaciones

	def getLimitacionesTxt(self):
		limitaciones = self.getLimitaciones()
		limitaciones = [lim.nombre for lim in limitaciones]
		return ", ".join(limitaciones)

class Enfermedades(models.Model):
	nombre = models.CharField(max_length = 80, null=True, blank=True)

	def __str__(self):
		return str(self.nombre)

class EnfermedadesEstudiante(models.Model):
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	enfermedad = models.ForeignKey(Enfermedades, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.estudiante) + " - " + str(self.enfermedad)


class EstudianteAdicional(models.Model):
	estudiante = models.OneToOneField(Estudiante, on_delete=models.CASCADE, primary_key=True, related_name="adicional")
	# Información adicional
	desplazado = models.BooleanField("Desplazado", null=True, blank=True)
	familias_accion = models.BooleanField("Familias en acción", null=True, blank=True)
	victima_conflicto = models.BooleanField("Victima del conflicto", null=True, blank=True)
	alumna_madre_cabeza = models.BooleanField("Alumna madre cabeza de familia", null=True, blank=True)
	beneficiario_fuerza_publica = models.BooleanField("Beneficiario de fuerza pública", null=True, blank=True)
	beneficiario_heroe_nacion = models.BooleanField("Beneficiario de héroe de la nación", null=True, blank=True)
	sabe_nadar = models.BooleanField("¿Sabe nadar?", null=True, blank=True)
	dificultades_aprendizaje = models.BooleanField("¿Ha tenido dificultades en el Aprendizaje?", null=True, blank=True)
	lugar_desplazamiento = models.CharField(max_length = 80, null=True, blank=True)
	vive_con = models.CharField(max_length = 80, null=True, blank=True)
	etnia_indigena = models.CharField(max_length = 40, null=True, blank=True)
	tipo_vivienda = models.CharField(max_length = 40, null=True, blank=True)
	estrato_socioeconomico = models.CharField(max_length = 5, null=True, blank=True)
	ie_proviene = models.CharField(max_length = 60, null=True, blank=True)
	lugar_proviene = models.CharField(max_length = 50, null=True, blank=True)
	tel_familiar_fuera = models.CharField(max_length = 15, null=True, blank=True)
	personas_hogar = models.IntegerField(default = 0)
	hermanos_ie = models.IntegerField(default = 0)

	def __str__(self):
		return str(self.estudiante.get_full_name())

class Acudiente(models.Model):
	nombre = models.CharField(max_length = 40, null=True, blank=True)
	parentesco = models.CharField(max_length = 20, null=True, blank=True)
	edad = models.CharField(max_length = 20, null=True, blank=True)
	identificacion = models.CharField(max_length = 20, null=True, blank=True)
	expedido_en = models.CharField(max_length = 100, null=True, blank=True)
	ocupacion = models.CharField(max_length = 20, null=True, blank=True)
	municipio_residencia = models.CharField(max_length = 20, null=True, blank=True)
	direccion_residencia = models.CharField(max_length = 200, null=True, blank=True)
	celular1 = models.CharField(max_length = 30, null=True, blank=True)
	celular2 = models.CharField(max_length = 30, null=True, blank=True)
	email = models.CharField(max_length = 30, null=True, blank=True)
	cabeza_familia = models.BooleanField("¿Es cabeza de familia?", default=False)

	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.nombre) + " - " + str(self.identificacion)

class EstudianteAcudiente(models.Model):
	estudiante = models.OneToOneField(Estudiante, on_delete=models.CASCADE, null=True)
	acudiente = models.ForeignKey(Acudiente, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return self.estudiante.get_full_name()

class Limitacion(models.Model):
	nombre = models.CharField(max_length = 100)
	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo')
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)

	def __str__(self):
		return str(self.nombre)

class LimitacionEstudiante(models.Model):
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE, null=True)
	limitacion = models.ForeignKey(Limitacion, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return str(self.estudiante) + " - " + str(self.limitacion)

class Anecdotario(models.Model):
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE, null=True)
	anotaciones_negativas = RichTextField(null=True, blank=True)
	anotaciones_positivas = RichTextField(null=True, blank=True)

	def __str__(self):
		return str(self.estudiante)

class Administrador(models.Model):
	#user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="administrador")
	email = models.EmailField(max_length = 100, null=True, blank=True)
	nombre = models.CharField(max_length = 20, null=True, blank=True)
	apellido = models.CharField(max_length = 20, null=True, blank=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	telefono = models.CharField(max_length = 20, null=True, blank=True)
	celular = models.CharField(max_length = 20, null=True, blank=True)
	estudios = models.TextField(null=True, blank=True)
	direccion = models.TextField(null=True, blank=True)
	sede = models.ForeignKey(Sede, on_delete=models.SET_NULL, null=True)
	foto = models.ImageField(upload_to='pro/%y/%s', blank = True, null = True)
	MASC = 'M'
	FEM = 'F'
	SEXO_CHOICES = (
		(MASC, 'Masculino'),
		(FEM, 'Femenino')
	)
	sexo = models.CharField(
		max_length = 1,
		choices = SEXO_CHOICES,
		default = MASC,
	)
	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo')
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)

	def getEstado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return ""

	def getSexo(self):
		for data in self.SEXO_CHOICES:
			if data[0] == self.sexo:
				return data[1]
		return ""

	def __str__(self):
		return str(self.nombre) + " " + str(self.apellido)

	def get_full_name(self):
		return str(self.nombre) + " " + str(self.apellido)

class AreaFundamental(models.Model):
	copy_id = models.IntegerField(default = 0)
	titulo = models.CharField(max_length = 60)
	abreviatura = models.CharField(max_length = 30)

	def __str__(self):
		return str(self.titulo)

class Asignatura(models.Model):
	copy_id = models.IntegerField(default = 0)
	titulo = models.CharField(max_length = 60)
	abreviatura = models.CharField(max_length = 30)
	area_fundamental = models.ForeignKey(AreaFundamental, on_delete=models.SET_NULL, null=True, blank = True)
	promedio = models.BooleanField("Promedio", default=True, help_text="Define si se tiene en cuenta a la hora de calcular el promedio")
	comportamiento = models.BooleanField("Comportamiento", default=False, help_text="Define si es una asignatura de comportamiento")
	porcentaje = models.IntegerField(default = 100)
	orden = models.IntegerField(default = 100)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return_text = str(self.titulo) + "-" + str(self.year)
		if self.porcentaje < 100:
			return_text += " (" + str(self.porcentaje) + "% - " + str(self.area_fundamental) + ")"
		return return_text


class ConfiguracionInstitucion(models.Model):
	nombre_rector = models.CharField(max_length = 100, blank = True, null = True)
	identificacion_rector = models.CharField(max_length = 100, blank = True, null = True)
	nombre_institucion = models.CharField(max_length = 100, null=True, blank=True)
	nit = models.CharField(max_length = 100, blank = True, null = True)
	dane = models.CharField(max_length = 100, blank = True, null = True)
	telefonos = models.CharField(max_length = 100, blank = True, null = True)
	email = models.CharField(max_length = 100, blank = True, null = True)
	ciudad = models.CharField(max_length = 100, blank = True, null = True)
	direccion = models.TextField(null=True, blank=True)
	descripcion_corta = models.TextField(null=True, blank=True)
	mision = models.TextField(null=True, blank=True)
	vision = models.TextField(null=True, blank=True)
	historia = models.TextField(null=True, blank=True)
	manual_convivencia = models.FileField(upload_to='manualC/%s', null=True, blank=True)
	escudo = models.ImageField(upload_to='config/escudo/%s', blank = True, null = True)
	mostrar_docentes = models.BooleanField("Mostrar docentes en blog", default=True, help_text="Define si se muestra el perfil de los docentes en la parte del blog")
	himno = RichTextField(null=True, blank=True)
	adicional = RichTextField(null=True, blank=True)

	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.nombre_institucion) + " - " + str(self.year)

class ConfiguracionInformes(models.Model):
	cabecera_informe_comun = RichTextField(null=True, blank=True)
	constancia_estudio = RichTextField("Constancia de estudio", null=True, blank=True)
	constancia_familias_accion = RichTextField("Constancia de estudio familais en acción", null=True, blank=True)
	certificado_notas = RichTextField("Certificado de notas", null=True, blank=True)
	hoja_matricula_1 = RichTextField(null=True, blank=True)
	hoja_matricula_2 = RichTextField(null=True, blank=True)

	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return "Configuración informe - " + str(self.year)

class Valoracion(models.Model):
	nombre = models.CharField(max_length = 50, blank = True, null = True)
	titulo = models.CharField(max_length = 10, blank = True, null = True)
	min = models.FloatField(default=0.0)
	max = models.FloatField(default=1)

	def __str__(self):
		return str(self.titulo)


class AsignacionDocente(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE)
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
	hs = models.IntegerField(default = 1)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)
	def __str__(self):
		return str(self.grupo.getGrupo()) + " - " + str(self.asignatura)


class JuicioValorativo(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE, null=True, blank=True)
	descripcion = models.TextField(null=True, blank=True)
	es_nee = models.BooleanField(default=False)

	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo')
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.descripcion)

	def getEstado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return ""

class TemasDocInstitucionales(models.Model):
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.titulo


class Formato(models.Model):
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE, null=True, blank = True)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='pl_ar/%Y/%s')
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.docente) + " - " + str(self.observaciones)

class Fallo(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank = True)
	observaciones = models.TextField(null=True, blank=True)
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	E_ACTIVO = 'A'
	E_INACTIVO = 'I'
	ESTADO_CHOICES = (
		(E_ACTIVO, 'Activo'),
		(E_INACTIVO, 'Inactivo')
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_ACTIVO,
	)

	def getEstado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return ""

	def __str__(self):
		return str(self.user) + " - " + str(self.observaciones)

class Salida(models.Model):
	archivo = models.FileField(upload_to='salida/%Y/%s')
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	direccion = models.TextField(null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return str(self.direccion) + " - " + str(self.observaciones)

class DocInstitucionales(models.Model):
	tema = models.ForeignKey(TemasDocInstitucionales, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='doc_ins/%Y/%s')
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return self.titulo

class PlanArea(models.Model):
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE, null=True, blank = True)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='pl_ar/%Y/%s')
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return self.asignatura + " - " + str(self.observaciones)

class PlanActividad(models.Model):
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE, null=True, blank = True)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='pl_act/%Y/%s')
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	year = models.CharField(
		max_length = 4,
		choices = FECHA_CHOOSER,
		default = str(now.year)
	)

	def __str__(self):
		return self.asignatura + " - " + str(self.observaciones)

class PlanActividadGrupo(models.Model):
	plan = models.ForeignKey(PlanActividad, on_delete=models.CASCADE)
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)

	def __str__(self):
		return self.plan + " - " + str(self.grupo)

### Planeador
class PlaneadorTemas(models.Model):
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	duracion = models.IntegerField(default = 0)
	estandar = models.CharField(max_length = 100)
	competencia = models.CharField(max_length = 100)

	def __str__(self):
		return str(self.grado) + " - " + str(self.asignatura) + " - " + str(self.titulo)

class PlaneadorActividades(models.Model):
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	valoracion = models.ForeignKey(Valoracion, on_delete=models.CASCADE)
	metodologia = models.CharField(max_length = 100)
	fecha = models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return str(self.grado) + " - " + str(self.asignatura) + " - " + str(self.titulo)

class PlaneadorEvaluacion(models.Model):
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	valoracion = models.ForeignKey(Valoracion, on_delete=models.CASCADE)
	tipo = models.CharField(max_length = 100)
	fecha = models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return str(self.grado) + " - " + str(self.asignatura) + " - " + str(self.titulo)

class PlaneadorSuperacion(models.Model):
	grado = models.ForeignKey(Grado, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	valoracion = models.ForeignKey(Valoracion, on_delete=models.CASCADE)
	tipo = models.CharField(max_length = 100)
	fecha = models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return str(self.grado) + " - " + str(self.asignatura) + " - " + str(self.titulo)

### Materiales
class MaterialApoyo(models.Model):
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='material_apoyo/%Y/%s')
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	def __str__(self):
		return self.titulo


class MaterialRecuperacion(models.Model):
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='material_recuperacion/%Y/%s')
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	def __str__(self):
		return self.titulo


class AnuncioActividades(models.Model):
	grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	fecha_realizacion = models.DateTimeField(auto_now_add = False)
	titulo = models.CharField(max_length = 100)
	observaciones = models.TextField(null=True, blank=True)
	archivo = models.FileField(upload_to='anuncio_actividades/%Y/%s', null = True, blank = True)

	def __str__(self):
		return self.titulo


class BiliotecaVirtual(models.Model):
	fecha_ingreso = models.DateTimeField(auto_now_add = True)
	titulo = models.CharField(max_length = 100)
	url = models.URLField(null=True, blank=True, max_length=200)
	observaciones = models.TextField(null=True, blank=True)
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)

	def __str__(self):
		return self.titulo

class EscuelaPadres(models.Model):
	titulo = models.CharField(max_length = 100)
	imagen = models.ImageField(upload_to='escuela/%Y/%s')
	cuerpo = RichTextField(null=True, blank=True)
	vistas = models.IntegerField(default = 0)
	fecha_publicacion = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.titulo

class NoticiasBlog(models.Model):
	titulo = models.CharField(max_length = 100)
	imagen = models.ImageField(upload_to='blog/noticias/%Y/%s')
	cuerpo = RichTextField(null=True, blank=True)
	vistas = models.IntegerField(default = 0)
	fecha_publicacion = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.titulo

class Icfes(models.Model):
	cuerpo = RichTextField(null=True, blank=True)
	A = models.TextField(null=True, blank=True)
	B = models.TextField(null=True, blank=True)
	C = models.TextField(null=True, blank=True)
	D = models.TextField(null=True, blank=True)
	RESPUESTAS_CHOICE = (
		('A', 'A'),
		('B', 'B'),
		('C', 'C'),
		('D', 'D')
	)
	correcta = models.CharField(
		max_length = 1,
		choices = RESPUESTAS_CHOICE,
		null=True, blank=True
	)
	AREAS_CHOICE = (
		('mat', 'Matemáticas'),
		('ing', 'Inglés'),
		('nat', 'Ciencias Naturales'),
		('lit', 'Literatura'),
		('ciu', 'Competencias ciudadanas')
	)
	area = models.CharField(
		max_length = 3,
		choices = AREAS_CHOICE,
		null=True, blank=True
	)

	def __str__(self):
		return str(self.area)


class Calendario(models.Model):
	fecha_inicio = models.DateField(null=True)
	fecha_fin = models.DateField(null=True)
	descripcion = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.descripcion


########## Asistencia y demás
class Inasistencia(models.Model):
	fecha = models.DateField(null=True)
	horas = models.IntegerField(default = 1)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	excusa = models.BooleanField(default=False)

	def __str__(self):
		return str(self.fecha) + " - " + str(self.estudiante)

class InasistenciaDocente(models.Model):
	fecha = models.DateField(null=True)
	horas = models.IntegerField(default = 1)
	docente = models.ForeignKey(Docente, on_delete=models.CASCADE)
	motivo = models.TextField(null=True, blank=True)

	def __str__(self):
		return str(self.fecha) + " - " + str(self.docente)

########## Tabla de notas

class Nota2016(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.DecimalField(max_digits=4, decimal_places=2)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)


	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2017(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)


	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2018(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)

	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2019(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)

	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2020(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)


	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2021(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)


	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2022(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)


	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class Nota2023(models.Model):
	asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
	valor = models.FloatField(default=0.0, validators=[MinValueValidator(MIN_NOTA), MaxValueValidator(MAX_NOTA)])
	inasistencias = models.IntegerField(default = 0)
	habilitacion = models.FloatField(default=0.0)
	fecha_moficiacion = models.DateTimeField(auto_now = True)

	def __str__(self):
		return str(self.valor) + " - " + str(self.estudiante) + " - " + str(self.periodo) + " - " + str(self.asignatura) + " - " + str(self.habilitacion) + "*"

class ContactoBlog(models.Model):
	email = models.CharField(max_length = 30, null=True, blank=True)
	nombre = models.CharField(max_length = 30, null=True, blank=True)
	asunto = models.CharField(max_length = 60, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)
	fecha = models.DateTimeField(auto_now_add = True)

	def __str__(self):
		return str(self.asunto) + " de " + str(self.nombre)


def get_valoracion(nota):
	data = Valoracion.objects.filter(Q(max__gt = nota) & Q(min__lte = nota))
	if len(data) > 0:
		return data[0].titulo
	return "-"
