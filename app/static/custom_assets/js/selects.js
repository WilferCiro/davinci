class selects_view extends PageHandler {
	constructor() {
		super();
		$('.select_docentes').select2({
			allowClear: true,
			placeholder: "Seleccione el docente",
			ajax: {
				url: '/json/docentes',
				dataType: 'json',
				quietMillis: 250
			}

		});
	}

	aditional_proccess_json (json_data, array_objects){

	}
}

const selects_js = new selects_view();
