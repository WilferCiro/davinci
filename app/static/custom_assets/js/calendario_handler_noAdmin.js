
class calendario extends PageHandler {
	constructor() {
		super();

		this.last_start = "";
		this.last_end = "";
		this.last_revertFunc = null
		//self.configureAjax(self);
		this.calendar =  $('#calendar').fullCalendar({
			header: {
				left: 'title',
				//center: 'agendaDay,agendaWeek,month',
				center: 'month',
				right: 'prev,next today'
			},
			firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
			defaultView: 'month',

			axisFormat: 'h:mm',
			columnFormat: {
                month: 'dddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
			allDaySlot: false,
			selectHelper: true,

			events:
			{
				url: '/adicional/calendario/',
				type: 'POST',
				dataType: 'json',
				data: {
					ajaxRequest : 'yes',
					ajax_action : 'request_data'
				},
				error: function() {
					alert('Hubo un error al obtener los eventos');
				},
				color: 'blue',   // a non-ajax option
				textColor: 'black' // a non-ajax option
			},

		});


	}

	delete_event (event_id){
		$("#delete_object_id").val(event_id);
		$("#modalDelete").modal("show");
	}

	aditional_proccess_json (json_data, array_objects){
		$('#calendar').fullCalendar('refetchEvents');
	}

}

const cal = new calendario();
