
class asistencia_class extends PageHandler {
	constructor() {
		super();
		$(".horas").change(this.changeHoras);
		$(".save_inasistencia").click(this.saveInasistencia)
	}

	changeHoras(e){
		var value = $(this).val();
		var last_value = $(this).attr("last_value");
		var nro = $(this).attr("nro");
		$("#indicador" + nro).text(value);
		if (value != last_value){
			$("#save_data" + nro).removeAttr("disabled");
		}
		else{
			$("#save_data" + nro).attr("disabled", true);
		}
	}

	saveInasistencia(){
		var nro = $(this).attr("nro");
		var horas = $("#horas"+nro).val();
		var excusa = $("#excusa"+nro).is(":checked");
		var estudiante_id = $("#estudiante"+nro).val();
		var fecha = $("#fecha_valid").val();
		var data_send = {
			"horas" : horas,
			"excusa" : excusa,
			"estudiante_id" : estudiante_id,
			"fecha" : fecha,
			"ajaxRequest" : "Yes",
			"ajax_action" : "editAsistencia"
		}
		var url = "/estudiantes/asistencia";
		$("#save_data" + nro).attr("disabled", true).tooltip("hide");
		self.sendData(self, data_send, [nro, horas], url);
	}

	proccess_error (objects) {
		var nro = objects;
		//$("[nota_id='nota_" + String(nota_id) + "']").attr("title", "Hubo un error al guardar la nota").tooltip("update");
	}

	proccess_json (json, data) {
		if (json.concept == "success") {
			$("#horas" + data[0]).attr("last_value", data[1]);
			if (data[1] == "1"){
				$("#concepto" + data[0]).text("Retardo");
			}
			else if (data[1] == "0"){
				$("#concepto" + data[0]).text("Sin registro");
			}
			else{
				$("#concepto" + data[0]).text("Inasistencia");
			}
		}
		else {
			$("#horas" + data[0]).attr("value", data[1]);
		}
	}
}

const asis = new asistencia_class();
