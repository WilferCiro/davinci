
class tablas_lista extends PageHandler {
	constructor() {
		super();

		$("#viewControlsCheck").change({self : this}, this.toggleControls);
		$("#solo_activos").click(this.solo_activos);
		$("#reloadTable").click(this.reloadTable);

		self.url = $("#form_action").val();

		self.table = $('#post_list').DataTable({
			"bProcessing": true,
		 	"serverSide": true,
		 	//stateSave: true,
	        "deferRender": true,
		 	ajax:{
				url : self.url,
				type: "POST",
				data: function ( d ) {
					d.solo_activos = $("#solo_activos").is(':checked');
				},
				error: function(){
					$("#post_list_processing").css("display","none");
				}
		  	},
        	"order": [[ 0, "desc" ]],
			"language": {
				"url": "/static/assets/json/Spanish.json"
			}
		});

		$('a.toggle-vis').on( 'click', function (e) {
		    e.preventDefault();
		    // Get the column API object
		    var column = self.table.column( $(this).attr('data-column') );

		    // Toggle the visibility
		    column.visible( ! column.visible() );
		} );

		/*$('#post_list tbody').on( 'click', 'tr', function () {
		    if ( !$(this).hasClass('selected') ) {
		        self.table.$('tr.selected').removeClass('selected');
		        $(this).addClass('selected');
		        $(this).find(".input_check").prop("checked", true);
		    }
		} );*/

	}

	solo_activos(){
		self.reloadTable();
	}
	reloadTable(){
		self.table.ajax.reload(null, false);
	}

	editarFila(id) {
		var data = {"getObjectData" : "get", object_id : id};
		self.sendData(self, data, [], self.url)
	}

	eliminarFila(id)	{
		$("#modalDelete").modal("show");
		$("#delete_object_id").val(id);
	}

	proccess_json (json, data){
		if (json.get_json){
			$.each(json,function(index, value){
				console.log(index, value);
				$("#id_edit-" + index).val(value);
				$("select#id_edit-" + index).val(value).change();
				$("textarea#id_edit-" + index).text(value);
			});
			$("#modalEdit").modal("show");
		}
		else{
			self.table.ajax.reload();
		}
	}

}

const im = new tablas_lista();

function editar_fila(id){
	im.editarFila(id);
}
function eliminar_fila(id){
	im.eliminarFila(id);
}
