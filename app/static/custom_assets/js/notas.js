
class notas_class extends PageHandler {
	constructor() {
		super();
		$(".input-nota").keypress(this.add_nota);
		$(".input-nota").blur(this.focus_lost_nota);
		$("input").tooltip();
		$(".input-nota").focus(function(){
			$(this).select();
		})
		$(".input-inas").keypress(this.add_inasistencia);
		$(".input-inas").blur(this.focus_lost_inasistencia);
		$(".input-inas").focus(function(){
			$(this).select();
		})
	}

	focus_lost_nota(e) {
		e.preventDefault();
		var last_value = parseFloat($(this).attr("last_value"));
		var valor_nota = parseFloat($(this).val());
		if (last_value != valor_nota) {
			$(this).attr("last_value", valor_nota);
			self.save_nota(this);
		}
	}
	focus_lost_inasistencia(e) {
		e.preventDefault();
		var last_value = parseFloat($(this).attr("last_value"));
		var valor_ina = parseFloat($(this).val());
		if (last_value != valor_ina) {
			$(this).attr("last_value", valor_ina);
			self.save_inasistencia(this);
		}
	}

	add_nota (e) {
		if(e.which==13) {
			e.preventDefault();
			self.save_nota(this);
		}
	}

	save_nota(elem){
		var estudiante_id = $(elem).attr("estudiante");
		var periodo_id = $("#periodo_id").val();
		var asignacion_id = $(elem).attr("asignacion_id");
		if (asignacion_id == undefined){
			var asignacion_id = $("#asignacion_id").val();
		}

		var tipo = $(elem).attr("tipo");

		var nota_id = $(elem).attr("nota_id");
		var id = parseInt(nota_id.split("_")[1]);
		var valor_nota = parseFloat($(elem).val());
		if (valor_nota > self.MAX_NOTA) {
			if (valor_nota >= 10){
				$(elem).val(valor_nota / 10);
			}
			valor_nota = parseFloat($(elem).val());
			if (valor_nota > self.MAX_NOTA) {
				$(elem).val(self.MAX_NOTA);
				$(elem).val(self.MAX_NOTA);
			}
		}
		else if (valor_nota < self.MIN_NOTA) {
			$(elem).val(self.MIN_NOTA);
		}
		$(elem).val(parseFloat($(elem).val()).toFixed(1));
		valor_nota = parseFloat($(elem).val());

		if (!isNaN(valor_nota)){
			var data_send = {
				"valor_nota" : valor_nota,
				"estudiante_id" : estudiante_id,
				"periodo_id" : periodo_id,
				"asignacion_id" : asignacion_id,
				"ajaxRequest" : "Yes",
				"ajax_action" : "editNota",
				"tipo" : tipo
			}
			var url = "/estudiantes/EditarNotas/Default";
			self.sendData(self, data_send, [id, valor_nota], url);
		}
		else{
			alert("Este campo no soporta letras");
		}
	}

	add_inasistencia (e) {
		if(e.which==13) {
			e.preventDefault();
			self.save_inasistencia(this);
		}
	}

	save_inasistencia(elem) {
		var estudiante_id = $(elem).attr("estudiante");
		var periodo_id = $("#periodo_id").val();
		var asignacion_id = $(elem).attr("asignacion_id");
		if (asignacion_id == undefined){
			var asignacion_id = $("#asignacion_id").val();
		}

		var inas_id = $(elem).attr("inas_id");
		var id = parseInt(inas_id.split("_")[1]);
		var valor_inas = parseInt($(elem).val());
		if (valor_inas > 20) {
			$(elem).val(20);
		}
		else if (valor_inas < 0) {
			$(elem).val(0);
		}
		valor_inas = parseInt($(elem).val());
		$(elem).val(valor_inas);

		if (!isNaN(valor_inas)){
			var data_send = {
				"inas" : valor_inas,
				"estudiante_id" : estudiante_id,
				"periodo_id" : periodo_id,
				"asignacion_id" : asignacion_id,
				"ajaxRequest" : "Yes",
				"ajax_action" : "editNota",
				"tipo" : "inas"
			}
			var url = "/estudiantes/EditarNotas/Default";
			self.sendData(self, data_send, [id, valor_inas], url);
		}
		else {
			alert("Este campo no soporta letras");
		}
	}

	proccess_error (objects) {
		var nota_id = objects;
		//$("[nota_id='nota_" + String(nota_id) + "']").attr("title", "Hubo un error al guardar la nota").tooltip("update");
	}

	proccess_json (json, data) {
		var nota_id = data[0];
		var valor = data[1];
		if (json.concept == "success") {
			$("#fecha_" + (nota_id)).text(json.fecha);
			if (json.tipo == "nota" || json.tipo == "habilitacion"){
				$("[nota_id='nota_" + (nota_id + 1) + "']").focus();
				$("[nota_id='nota_" + (nota_id) + "']").addClass("success").removeClass("error");
			}
			else if (json.tipo == "inas"){
				$("[inas_id='inas_" + (nota_id + 1) + "']").focus();
				$("[inas_id='inas_" + (nota_id) + "']").addClass("success").removeClass("error");
			}
			//$("#justificacion_" + nota_id).text("Bien").addClass("success").removeClass("error");
		}
		else {
			if (json.tipo == "nota"){
				$("[nota_id='nota_" + (nota_id) + "']").addClass("error").removeClass("success").val(valor);
			}
			else if (json.tipo == "inas"){
				$("[inas_id='inas_" + (nota_id) + "']").addClass("error").removeClass("success").val(valor);
			}
			//$("#justificacion_" + nota_id).text(json.error).addClass("error").removeClass("success").val(valor);
		}
	}
}

const not = new notas_class();
