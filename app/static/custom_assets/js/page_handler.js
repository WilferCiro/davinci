$(document).ready(function(){
	$("#generalLoading").hide();
	//$(".multiselect").attr("multiple", "multiple").searchableOptionList();
	//$(".searchSelect").searchableOptionList();
	$("#formForm").submit(function(e){
		var button = $("#buttonSendForm");
		var text = button.text();
		button.text("Cargando...");
		setTimeout(function() {
			button.attr('disabled', "true");
		}, 150);
		setTimeout(function() {
			button.removeAttr('disabled').text(text);
		}, 10000);
	});
	$("button, a").tooltip();

	/*if (window.hasOwnProperty('webkitSpeechRecognition')) {
		shortcut.add("Ctrl+B",function() {
			var id_e = $(':focus');
			if(id_e){
				var recognition = new webkitSpeechRecognition();

				recognition.continuous = false;
				recognition.interimResults = false;

				recognition.lang = "es-CO";
				recognition.start();

				recognition.onresult = function(e) {
					results = e.results[0][0].transcript;
					var id = $(':focus');
					if (id.get(0).tagName == "INPUT"){
						id.val(id.val() + results);
					}
					else if(id.get(0).tagName == "TEXTAREA"){
						id.val(id.val() + " " + results);
					}
					recognition.stop();
				};
				recognition.onerror = function(e) {
					recognition.stop();
				}
			}
		});
	}*/


});

class PageHandler {
	constructor(){
		self = this;
		self.configureAjax(self);
		self.DEBUG = true;

		// Notas definition
		self.MAX_NOTA = 5.0;
		self.MIN_NOTA = 0.0;


		$(".formSendAjax").submit({self : this}, this.submitFormAjax);

		$("#mostrar_notificaciones").click(this.load_notificaciones);
	}

	load_notificaciones (event) {
		var data = {}
		$(".nofity-list").text("cargando notificaciones...");
		self.sendData(self, data, "save", "/json/notification");

	}

	submitFormAjax (event) {
		event.preventDefault();
		var form = $(this);
		var url = form.attr("action");
		var self = event.data.self;
		var dataSend = form.serializeArray();
		dataSend.push({name : "ajaxRequest", value : true});
		self.sendData(self, dataSend, form, url, true);
	}


	getCookie(name) {
		var cookieValue = null;
		if(document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for(var i = 0; i < cookies.length; i++) {
			    var cookie = jQuery.trim(cookies[i]);
			    if(cookie.substring(0, name.length + 1) == (name + '=')) {
			        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
			        break;
			    }
			}
		}
		return cookieValue;
	}

	showLoading (){
		$("#generalLoading").show().removeClass("alert-success").removeClass("alert-danger").removeClass("alert-info").addClass("alert-info").text("Loading, Please wait...");
	}
	showError (error){
		$("#generalLoading").show().removeClass("alert-success").removeClass("alert-info").addClass("alert-danger").text("Error, processing");
	}
	showSuccess (){
		$("#generalLoading").show().removeClass("alert-danger").removeClass("alert-success").removeClass("alert-info").addClass("alert-success").text("Success").hide(9000);
	}

	formLoading (form){
		form.find("#messageBody").text("Cargando...").removeClass("alert-danger").addClass("alert-primary");
	}

	formSuccess (json, form){
		if (json.error){
			self.formError(form);
		}
		else{
			form.find("#messageBody").text("Petición realizada con éxito");
			form.closest(".modal").modal("hide");
			form.trigger("reset");
			self.proccess_json(json, []);
		}
	}

	formError (form){
		form.find("#messageBody").text("Error al realizar la petición").removeClass("alert-primary").addClass("alert-danger");
	}

	configureAjax (self){
		$.ajaxSetup({
			global: true,
			beforeSend: function(xhr, settings) {
				if(!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
				    xhr.setRequestHeader("X-CSRFToken", self.getCookie('csrftoken'));
				    xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded; charset=UTF-8');
				}
			}
		});
	}

	sendData (self, data, array_objects, url, is_form = false){
		/*
			array_objects are the graphics elements to modify in proccess_json
			data is the dict that has the elements to send

		*/
		if (is_form){
			self.formLoading(array_objects);
		}
		else{
			self.showLoading();
		}

		$.ajax({
			url : url,
			data : data,
			type : "POST",
			dataType : 'json',
			success : function(json) {
				if (is_form){
					self.formSuccess(json, array_objects);
				}
				else{
					self.showSuccess();
					self.proccess_json(json, array_objects);
				}
			},
			error : function(xhr, status) {
				if (self.DEBUG){
					alert('Disculpe, ocurrió un problema' + xhr.responseText);
				}

				if (is_form){
					self.formError(array_objects);
				}
				else{
					self.showError(xhr.responseText);
				}
			}
		});

	}

	proccess_json (json_data, array_objects){
		if (json_data.notificacion){
			/**/
			var i = 0;
			var resultados = json_data.results;
			$(".nofity-list").empty();
			for (i = 0; i < resultados.length; i++) {
				console.log(resultados[i].evento);
				$(".nofity-list").append(" \
				<a href='"+ resultados[i].href +"' class='notify-item'> \
					<div class='notify-text'> \
						<span class='msg'>"+ resultados[i].evento +"</span> \
						<span>" + resultados[i].fecha_inicio + " - " + resultados[i].fecha_fin + "</span> \
					</div> \
				</a>");
			}
		}
		else{
			self.aditional_proccess_json(json_data, array_objects);
		}
	}
}
