
class calendario extends PageHandler {
	constructor() {
		super();

		this.last_start = "";
		this.last_end = "";
		this.last_revertFunc = null;
		//self.configureAjax(self);
		this.calendar =  $('#calendar').fullCalendar({
			header: {
				left: 'title',
				//center: 'agendaDay,agendaWeek,month',
				center: 'month',
				right: 'prev,next today'
			},
			editable: true,
			firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
			selectable: true,
			defaultView: 'month',

			axisFormat: 'h:mm',
			columnFormat: {
				month: 'dddd',    // Mon
				week: 'ddd d', // Mon 7
				day: 'dddd M/d',  // Monday 9/7
				agendaDay: 'dddd d'
			},
				titleFormat: {
				month: 'MMMM yyyy', // September 2009
				week: "MMMM yyyy", // September 2009
				day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
			},
			allDaySlot: false,
			selectHelper: true,
			select: function(start, end, allDay) {
				$("#myModal").modal("show");
				var fecha = new Date(start)
				$(".form_add_method").find("#id_fecha_inicio").val(fecha.getFullYear() + "-" + (fecha.getMonth()+1) + "-" + start.getDate());
				var fecha_end = new Date(end)
				$(".form_add_method").find("#id_fecha_fin").val(fecha_end.getFullYear() + "-" + (fecha.getMonth()+1) + "-" + fecha_end.getDate());
				$(".form_add_method").find("#id_descripcion").val("").focus();
				self.last_start = start;
				self.last_end = end;
				self.calendar.fullCalendar('unselect');
			},
			droppable: true, // this allows things to be dropped onto the calendar !!!
			editable: true,
			eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
				var fecha_start = new Date(event.start)
				var fecha_end = new Date(event.end)
				var fecha_inicio = fecha_start.getFullYear() + "-" + (fecha_start.getMonth()+1) + "-" + fecha_start.getDate();
				var fecha_fin = fecha_inicio;
				if (event.end != null){
					fecha_fin = fecha_end.getFullYear() + "-" + (fecha_end.getMonth()+1) + "-" + fecha_end.getDate();
				}
				var data = {ajaxRequest : "Yes", ajax_action : "editar_modal", object_id: event.id, tipoEdit: "only_date", fecha_inicio : fecha_inicio, "fecha_fin" : fecha_fin};
				self.sendData(self, data, [], "/adicional/calendario/");
			},

			events:
			{
				url: '/adicional/calendario/',
				type: 'POST',
				dataType: 'json',
				data: {
					ajaxRequest : 'yes',
					ajax_action : 'request_data'
				},
				error: function() {
					alert('Hubo un error al obtener los eventos');
				},
				color: '#007FC4',   // a non-ajax option
				textColor: 'black' // a non-ajax option
			},
			eventRender: function(event, element) {
				element.append( "<span class='closeon'><i class='fa fa-trash'></i></span> | <span class='edit'><i class='fa fa-pencil'></i></span>" );
				element.find(".closeon").click(function() {
					self.delete_event(event._id);
				});
				element.find(".edit").click(function() {
					self.edit_event(event);
				});
			}

		});


	}

	edit_event (event){
		var fecha_start = new Date(event.start)
		var fecha_end = new Date(event.end)
		$(".form_editar_method").find("#id_fecha_inicio").val(fecha_start.getFullYear() + "-" + (fecha_start.getMonth()+1) + "-" + fecha_start.getDate());
		if (event.end == null){
			$(".form_editar_method").find("#id_fecha_fin").val(fecha_start.getFullYear() + "-" + (fecha_start.getMonth()+1) + "-" + fecha_start.getDate());
		}
		else{
			$(".form_editar_method").find("#id_fecha_fin").val(fecha_end.getFullYear() + "-" + (fecha_end.getMonth()+1) + "-" + fecha_end.getDate());
		}
		$(".form_editar_method").find("#id_descripcion").val(event.title);
		$(".form_editar_method").find("#id_object_id").val(event.id);
		$("#modalEdit").modal("show");
	}

	delete_event (event_id){
		$("#delete_object_id").val(event_id);
		$("#modalDelete").modal("show");
	}

	aditional_proccess_json (json_data, array_objects){
		$('#calendar').fullCalendar('refetchEvents');
	}

}

const cal = new calendario();
