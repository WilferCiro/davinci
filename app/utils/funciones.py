from app.models import *

def get_tabla_notas(year):
	year = str(year)
	if year == "2016":
		return Nota2016
	if year == "2017":
		return Nota2017
	if year == "2018":
		return Nota2018
	if year == "2019":
		return Nota2019
	if year == "2020":
		return Nota2020
	if year == "2021":
		return Nota2021
	if year == "2022":
		return Nota2022
	if year == "2023":
		return Nota2023

def replace_informe_data(text, year):
	from django.utils import timezone
	now = timezone.now()
	text = text.replace("{{anio}}", str(year))
	text = text.replace("{{fecha_impresion}}", now.strftime("%B %d, %Y"))
	return text


def replace_data_students(text, student, replace_line = False):
	if student != None:
		salud = student.getSalud()
		acudiente = student.getAcudiente()
	else:
		student = Estudiante()
		salud = EstudianteSalud()
		acudiente = Acudiente()
	data = {
		"{{nombre_estudiante}}" : student.nombre,
		"{{apellido_estudiante}}" : student.apellido,
		"{{edad_estudiante}}" : student.getEdad(),
		"{{grupo_estudiante}}" : student.grupo,
		"{{documento_estudiante}}" : student.documento,
		"{{tipo_documento_estudiante}}" : student.getTipoDocumento(),
		"{{municipio_expedicion_estudiante}}" : student.municipio_expedicion,
		"{{sexo_estudiante}}" : student.getSexo(),
		"{{fecha_matricula_estudiante}}" : student.fecha_matricula,
		"{{fecha_nacimiento_estudiante}}" : student.fecha_nacimiento,
		"{{municipio_nacimiento_estudiante}}" : student.municipio_nacimiento,
		"{{puntaje_sisben_estudiante}}" : salud.puntaje_sisben,
		"{{municipio_residencia_estudiante}}" : student.municipio_residencia,

		"{{nombre_acudiente}}" : acudiente.nombre,
		"{{cedula_acudiente}}" : acudiente.identificacion,
		"{{municipio_residencia_acudiente}}" : acudiente.municipio_residencia,
		"{{direccion_acudiente}}" : acudiente.direccion_residencia
	}
	for key in data:
		new_text = str(data[key])
		if replace_line:
			new_text = new_text.replace("None", "________________________")
		else:
			new_text = new_text.replace("None", "")
		text = text.replace(key, new_text)
	return text

def excel_add_student(value, header):
	if header == "tipo_documento":
		return value.replace(".", "").replace(" ", "")
	elif header == "sexo":
		return value.lower().replace("masculino", Estudiante.MASC).replace("femenino", Estudiante.FEM).upper()
	elif header == "zona":
		return value.lower().replace("rural", Estudiante.Z_RURAL).replace("urbana", Estudiante.Z_URBANA).upper()
	elif header == "estado":
		value = value.lower().replace("inactivo", Estudiante.E_INACTIVO)
		value = value.replace("activo", Estudiante.E_ACTIVO)
		value = value.replace("retirado", Estudiante.E_RETIRADO)
		value = value.replace("desertor", Estudiante.E_DESERTOR)
		value = value.replace("cancelado", Estudiante.E_CANCELADO)
		return value.upper()
	elif header == "limitaciones":
		limitaciones = value.split(",")
		all_limitaciones = []
		for lim in limitaciones:
			try:
				retLim = Limitacion.objects.get(nombre = lim.strip())
				all_limitaciones.append(retLim)
			except:
				pass
		return all_limitaciones
	value = value.replace("Si", "True").replace("No", "False").replace("si", "True").replace("no", "False")
	return value
