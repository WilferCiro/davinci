import os
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from weasyprint import HTML, CSS
from django.http import HttpResponse
from django.template.loader import render_to_string
from app.models import *
from app.forms.various import *

def show_pdf(data, template_path, absolute_path="/", download = False):
	html_template = render_to_string(template_path, data)
	pdf_file = HTML(string=html_template, base_url=absolute_path).write_pdf()
	response = HttpResponse(pdf_file, content_type='application/pdf')
	#response['Content-Disposition'] = 'attachment; filename="home_page.pdf"'
	response['Content-Disposition'] = 'inline; filename="home_page.pdf"'
	return response

def getImpresionFormato(requestPG, data_return, year):
	cabecera_actual = requestPG.get("cabecera_actual")
	if cabecera_actual:
		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
	else:
		Datos = ConfiguracionInformes.objects.get(year = year)
		DatosIns = ConfiguracionInstitucion.objects.get(year = year)
	cabecera = Datos.cabecera_informe_comun
	escudo = settings.MEDIA_URL + str(DatosIns.escudo)

	data_return["cabecera"] = cabecera
	data_return["escudo"] = escudo

	data_return["size_file"] = requestPG.get("size", "legal")
	data_return["orientacion"] = requestPG.get("orientacion", "portrait")
	horizontal = str(requestPG.get("margen_horizontal", 1))
	vertical = str(requestPG.get("margen_vertical", 1))
	data_return["margins"] = vertical + "cm " + horizontal + "cm " + vertical + "cm " + horizontal + "cm"

	return data_return

def getImpresionOptions(request):
	try:
		formato_form = ImpresionFormato(request.POST)
	except:
		formato_form = ImpresionFormato(request.GET)
	if formato_form.is_valid():
		data_return = {}
		data_return["cabecera_actual"] = formato_form.cleaned_data["cabecera_actual"]
		data_return["size"] = formato_form.cleaned_data["size"]
		data_return["orientacion"] = formato_form.cleaned_data["orientacion"]
		data_return["horizontal"] = str(formato_form.cleaned_data["margen_horizontal"])
		data_return["vertical"] = str(formato_form.cleaned_data["margen_vertical"])
	return data_return
