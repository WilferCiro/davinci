# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.core.validators import FileExtensionValidator


class formCopy(forms.Form):

	docentes = forms.FileField(label="Docentes excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	docentes.widget.attrs['accept'] = ".xlsx"

	grupos = forms.FileField(label="Grupos excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	grupos.widget.attrs['accept'] = ".xlsx"

	asignaturas = forms.FileField(label="Asignaturas excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	asignaturas.widget.attrs['accept'] = ".xlsx"

	juicios = forms.FileField(label="Juicios excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	juicios.widget.attrs['accept'] = ".xlsx"

	asignaciones = forms.FileField(label="Asignaciones excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	asignaciones.widget.attrs['accept'] = ".xlsx"

	estudiantes = forms.FileField(label="Estudiantes excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	estudiantes.widget.attrs['accept'] = ".xlsx"

	acudientes = forms.FileField(label="Acudientes excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	acudientes.widget.attrs['accept'] = ".xlsx"

	notas = forms.FileField(label="Notas excel (.xlsx)", required = False, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	notas.widget.attrs['accept'] = ".xlsx"
