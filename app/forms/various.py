# Own libraries
from app.models import *

# Django libraries
from django import forms


class ImpresionFormato(forms.Form):
	ORIENTACION = [("portrait", "Vertical"), ("landscape", "Horizontal")]
	orientacion = forms.ChoiceField(label="Orientación", choices=ORIENTACION, required=True)
	TAMANO = [("legal", "Oficio"), ("A4", "Carta")]
	size = forms.ChoiceField(label="Tamaño", choices=TAMANO, required=True)
	margen_horizontal = forms.CharField(label="izq-dere (cm)", widget=forms.TextInput(attrs={'type':'number', 'min':'0', 'max':'7', 'value' : 1}), required=False)
	margen_vertical = forms.CharField(label="arr-aba (cm)", widget=forms.TextInput(attrs={'type':'number', 'min':'0', 'max':'7', 'value' : 1}), required=False)
	#descargar_archivo = forms.BooleanField(label="Descargar evitando visualización (Sólo PDF)", required = False)
	cabecera_actual = forms.BooleanField(label="Cabecera, escudo y cuerpo según configuración del año actual (si deselecciona la opción, se pondrá la configuración del año seleccionado)", required = False)
	cabecera_actual.initial = True
