# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput
from django.utils.translation import ugettext_lazy as _
from django_select2.forms import Select2Widget

class editNoticiasBlog(ModelForm):
	class Meta:
		model = NoticiasBlog
		fields = ["titulo", "imagen", "cuerpo"]
		labels = {
			"titulo": _("Título"),
			"imagen": _("Imagen"),
			"cuerpo" : _("Cuerpo")
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editNoticiasBlog, self).__init__(*args, **kwargs)


class editIcfes(ModelForm):
	class Meta:
		model = Icfes
		fields = ["cuerpo", "A", "B", "C", "D", "correcta", "area"]
		widgets = {
			'A': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),
			'B': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),
			'C': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),
			'D': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),
		}
		labels = {
			"A": _("Opción A"),
			"B": _("Opción B"),
			"C": _("Opción C"),
			"D": _("Opción D"),
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editIcfes, self).__init__(*args, **kwargs)

class formEstadisticas(forms.Form):
	PROMEDIOS_BAJOS_GRADOS = 0
	PROMEDIOS_BAJOS_GRUPOS = 1
	DESEMPLENO_SEDE_PERIODO = 2
	PROMEDIO_ASIGNATURAS_GRUPOS = 3
	DESEMPENO_ASIGNATURA = 4
	PROMEDIOS_GRUPO = 5
	TIPO_INFORME = [
		(PROMEDIOS_BAJOS_GRADOS, "Promedios bajos por grados en el periodo"),
		(PROMEDIOS_BAJOS_GRUPOS, "Promedios bajos por grupos en el periodo"),
		(DESEMPLENO_SEDE_PERIODO, "Desempeño de la sedes en el periodo"),
		(PROMEDIO_ASIGNATURAS_GRUPOS, "Promedio de las asignaturas en el grupo y periodo"),
		(DESEMPENO_ASIGNATURA, "Desempeño en asignaturas en el grupo y periodo"),
		(PROMEDIOS_GRUPO, "Promedio de los grupos en el periodo")
	]
	tipo = forms.ChoiceField(label = "Tipo gráfica", choices = TIPO_INFORME)
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none(), label="Periodo", required=True)
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.none(), label="Grupo", required=False)

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(formEstadisticas, self).__init__(*args, **kwargs)
		fil = Q(year = year)
		if user != None:
			if user.es_docente():
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				if ad.count() == 0:
					self.fields["asignatura"].queryset = Asignatura.objects.none()
					self.fields["grado"].queryset = Grado.objects.none()
				else:
					fil = Q()
					for a in ad:
						fil |= Q(pk = a.grupo.pk)
					self.fields["grupo"].queryset = Grupo.objects.filter(fil)
			elif user.es_estudiante():
				self.fields["grupo"].queryset = Grupo.objects.filter(Q(grupo = user.docente().grupo))
			elif user.es_admin():
				self.fields["grupo"].queryset = Grupo.objects.all().order_by("-titulo")
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year))

class editCalendar(ModelForm):

	class Meta:
		model = Calendario
		fields = ["fecha_inicio", "fecha_fin", "descripcion"]
		widgets = {
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),
			"fecha_fin" : DatePickerInput(format="YYYY-MM-DD").start_of('fecha days'),
			"fecha_inicio" : DatePickerInput(format="YYYY-MM-DD").end_of('fecha days')
		}
		labels = {
			"fecha_fin": _("Fecha final"),
			"fecha_inicio": _("Fecha inicial"),
			"descripcion" : _("Descripción")
		}

class editSalidaEscolar(ModelForm):
	class Meta:
		model = Salida
		fields = ["archivo", "year", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True})
		}
		labels = {
			"year": _("Año"),
			"archivo" : _("Archivo"),
			"observaciones" : _("Observaciones")
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editSalidaEscolar, self).__init__(*args, **kwargs)
