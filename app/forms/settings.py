# Own libraries
from app.models import *

# Django libraries
from django import forms
from ckeditor.widgets import CKEditorWidget
from django.forms import ModelForm, Textarea
from bootstrap_datepicker_plus import DateTimePickerInput,TimePickerInput
from django_select2.forms import Select2MultipleWidget, Select2Widget


class ComunInformes(ModelForm):
	class Meta:
		model = ConfiguracionInformes
		exclude = ["year"]

class ComunInstitucion(ModelForm):
	class Meta:
		model = ConfiguracionInstitucion
		exclude = ["year"]
		widgets = {
			'direccion': Textarea(attrs={'cols': 20, 'rows': 4}),
			'mision': Textarea(attrs={'cols': 20, 'rows': 4}),
			'vision': Textarea(attrs={'cols': 20, 'rows': 4}),
			'historia': Textarea(attrs={'cols': 20, 'rows': 4}),
			'descripcion_corta': Textarea(attrs={'cols': 20, 'rows': 4}),
		}

class editSede(ModelForm):
	class Meta:
		model = Sede
		fields = ['dane', 'hora_apertura', 'hora_cierre', 'observaciones']
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
			'hora_apertura' : TimePickerInput(options={"format":"HH:mm"}),
			'hora_cierre' : TimePickerInput(options={"format":"HH:mm"}),
		}

class editPeriodo(ModelForm):
	class Meta:
		model = Periodo
		fields = ['fecha_inicio', 'fecha_final', 'fecha_apertura', 'fecha_cierre']
		widgets = {
			'fecha_inicio' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm"}),
			'fecha_final' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm"}),
			'fecha_apertura' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm"}),
			'fecha_cierre' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm"}),
		}

class editAsignatura(ModelForm):
	class Meta:
		model = Asignatura
		fields = ['abreviatura', 'comportamiento', 'promedio']

	def __init__(self, *args, **kwargs):
		super(editAsignatura, self).__init__(*args, **kwargs)

class editAreaFundamental(ModelForm):
	class Meta:
		model = AreaFundamental
		fields = ['abreviatura']

class editGrado(ModelForm):
	class Meta:
		model = Grado
		fields = ['observaciones']
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class editGrupo(ModelForm):
	class Meta:
		model = GrupoDocente
		fields = ['docente']
		widgets = {
			'docente': Select2Widget
		}
