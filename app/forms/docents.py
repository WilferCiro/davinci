# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from django_select2.forms import Select2Widget
from bootstrap_datepicker_plus import DatePickerInput
from django.core.validators import FileExtensionValidator
from django.utils.translation import ugettext_lazy as _

class editInasistenciaDocente(ModelForm):
	class Meta:
		model = InasistenciaDocente
		fields = ["fecha", "horas", "docente", "motivo"]
		widgets = {
			'motivo': Textarea(attrs={'cols': 20, 'rows': 3}),
			'fecha' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
			'docente': Select2Widget,
		}

class editDocente(ModelForm):
	class Meta:
		model = Docente
		fields = ["nombre", "apellido", "email", "estado", "sede", "documento", "sexo", "celular", "estudios", "direccion"]
		widgets = {
			'estudios': Textarea(attrs={'cols': 20, 'rows': 3}),
			'direccion': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class editJuicioValorativo(ModelForm):
	class Meta:
		model = JuicioValorativo
		fields = ['asignatura', 'grado', 'periodo', 'estado', 'descripcion']
		widgets = {
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 3}),
			'grado': Select2Widget,
			'asignatura': Select2Widget,
			'periodo': Select2Widget,
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editJuicioValorativo, self).__init__(*args, **kwargs)
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year))
		if user != "" and user != None:
			if user.es_docente():
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				if ad.count() == 0:
					self.fields["asignatura"].queryset = Asignatura.objects.none()
					self.fields["grado"].queryset = Grado.objects.none()
				else:
					fil = Q()
					for a in ad:
						fil |= Q(pk = a.asignatura.pk)
					fil &= Q(year = year)
					self.fields["asignatura"].queryset = Asignatura.objects.filter(fil)

					fil = Q()
					for a in ad:
						fil |= Q(pk = a.grupo.grado.pk)
					self.fields["grado"].queryset = Grado.objects.filter(fil)
			else:
				self.fields["asignatura"].queryset = Asignatura.objects.filter(Q(year = year))


class editAsignacion(ModelForm):
	class Meta:
		model = AsignacionDocente
		fields = ['asignatura', 'grupo', 'docente', 'hs']
		widgets = {
			'grupo': Select2Widget,
			'asignatura': Select2Widget,
			'docente': Select2Widget,
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editAsignacion, self).__init__(*args, **kwargs)
		self.fields["asignatura"].queryset = Asignatura.objects.filter(Q(year = year))

class ImprimirJuicios(forms.Form):
	grado = forms.ModelChoiceField(widget=Select2Widget, queryset=Grado.objects.all().order_by("-titulo"), label="Grado", required=False)
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.all().order_by("-titulo"), label="Periodo", required=False)
	asignatura = forms.ModelChoiceField(widget=Select2Widget, queryset=Asignatura.objects.none().order_by("-titulo"), label="Asignatura", required=False)
	FORMATO = (("pdf", "PDF"), ("xlsx", "Excel"))
	formato = forms.ChoiceField(label="Formato", choices=FORMATO, required=True)
	solo_activos = forms.BooleanField(label="Imprimir sólo juicios activos", required = False)
	order = forms.ChoiceField(label="Ordenar por", choices=(('grado', 'Grado'), ('periodo', 'Periodo'), ('asignatura', 'Asignatura')), required=True)

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(ImprimirJuicios, self).__init__(*args, **kwargs)
		self.fields["asignatura"].queryset = Asignatura.objects.filter(Q(year = year))

class formPrintInasistencias(forms.Form):
	MESES = [
		[1, "Enero"],
		[2, "Febrero"],
		[3, "Marzo"],
		[4, "Abril"],
		[5, "Mayo"],
		[6, "Junio"],
		[7, "Julio"],
		[8, "Agosto"],
		[9, "Septiembre"],
		[10, "Octubre"],
		[11, "Noviembre"],
		[12, "Diciembre"]
	]
	mes = forms.ChoiceField(label="Mes", choices=MESES, required=True)
	anio = forms.ChoiceField(label="Año", choices=FECHA_CHOOSER, required=True)


class editDocenteAvatar(ModelForm):
	class Meta:
		model = Docente
		fields = ["foto"]


class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())


class formGuiada(forms.Form):
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.all().order_by("-titulo"), label="Grupo", required=True)

class asignacionGuiadaEspecifica(ModelForm):
	class Meta:
		model = AsignacionDocente
		fields = ["docente"]


class FormJuicioExcel(forms.Form):
	archivo = forms.FileField(label="Archivo excel (.xlsx)", required = True, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	archivo.widget.attrs['accept'] = ".xlsx"
