# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from bootstrap_datepicker_plus import DatePickerInput
from django_select2.forms import Select2MultipleWidget, Select2Widget, HeavySelect2Widget
from django.core.validators import FileExtensionValidator

class addEstudiante(ModelForm):
	class Meta:
		model = Estudiante
		fields = ["nombre", "apellido", "estado", "grupo", "documento", "sexo", "zona"]
		widgets = {
			'grupo': Select2Widget
		}


class FiltroNotasBasico(forms.Form):
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none(), label="Periodo", required=True)
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.none(), label="Grupo", required=False)
	promedio_min = forms.FloatField(label = "Promedio mín", initial=0.0)
	promedio_max = forms.FloatField(label = "Promedio máx", initial=5.0)
	maximo_perdidas = forms.IntegerField(label = "Máx áreas perdidas", initial=21)
	simular_notas = forms.BooleanField(required=False, label="Simular notas de periodos posteriores")

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(FiltroNotasBasico, self).__init__(*args, **kwargs)
		fil = Q(year = year)
		if user != None:
			if user.es_docente():
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				if ad.count() == 0:
					self.fields["asignatura"].queryset = Asignatura.objects.none()
					self.fields["grado"].queryset = Grado.objects.none()
				else:
					fil = Q()
					for a in ad:
						fil |= Q(pk = a.grupo.pk)
					self.fields["grupo"].queryset = Grupo.objects.filter(fil)
			elif user.es_admin():
				self.fields["grupo"].queryset = Grupo.objects.all().order_by("-titulo")
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year))


class formInformes(forms.Form):
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none(), label="Periodo", required=True)
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.none(), label="Grupo", required=False)
	BOLETIN = 0
	COLSOLIDADO = 1
	PROMEDIO_ASIGNATURAS = 2
	PROMEDIO_DOCENTES = 3
	AREAS_PENDIENTES = 4
	LISTADO_PROMEDIOS = 5
	MEJORES_PROMEDIOS_SEDES = 6
	MEJORES_PROMEDIOS_GRUPOS = 7
	ASISTENCIA = 8
	TIPO_INFORME = [
		(BOLETIN, "Boletines"),
		(COLSOLIDADO, "Consolidado del grupo"),
		#(PROMEDIO_ASIGNATURAS, "Promedios por asignaturas"),
		#(PROMEDIO_DOCENTES, "Promedios por docentes"),
		(AREAS_PENDIENTES, "Aprobados reprobados y sus áreas perdidas"),
		(LISTADO_PROMEDIOS, "Lista ordenada de promedios"),
		(MEJORES_PROMEDIOS_SEDES, "Mejores promedios de las sedes"),
		(MEJORES_PROMEDIOS_GRUPOS, "Mejores promedios por grupos"),
		(ASISTENCIA, "Asistencia de todos los grupos")
	]
	informes = forms.ChoiceField(label = "Tipo Informe", choices = TIPO_INFORME)

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(formInformes, self).__init__(*args, **kwargs)
		fil = Q(year = year)
		if user != None:
			if user.es_docente():
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				if ad.count() == 0:
					self.fields["asignatura"].queryset = Asignatura.objects.none()
					self.fields["grado"].queryset = Grado.objects.none()
				else:
					fil = Q()
					for a in ad:
						fil |= Q(pk = a.grupo.pk)
					self.fields["grupo"].queryset = Grupo.objects.filter(fil)
			elif user.es_admin():
				self.fields["grupo"].queryset = Grupo.objects.all().order_by("-titulo")
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year))


class notaEstudiante(forms.Form):
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none().order_by("-titulo"), label="Periodo", required=True)
	asignacion = forms.ModelChoiceField(widget=Select2Widget, queryset=AsignacionDocente.objects.none().order_by("-grupo", "asignatura"), label="Asignación", required=True)

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(notaEstudiante, self).__init__(*args, **kwargs)
		fil = Q(year = year)
		if user != None:
			if user.es_docente():
				fil = fil & Q(docente = user.docente())
			elif user.es_estudiante():
				fil = fil & Q(grupo = user.estudiante().grupo)
		self.fields["asignacion"].queryset = AsignacionDocente.objects.filter(fil)
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year) & Q(es_final = False))

class asistenciaSelect(forms.Form):
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.none().order_by("-grado"), label="Grupo", required=True)
	fecha = forms.CharField(label="Fecha ", widget=DatePickerInput(format="YYYY-MM-DD"), required=False)

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(asistenciaSelect, self).__init__(*args, **kwargs)
		fil = Q()
		if user != None:
			if user.es_docente():
				fil = user.docente().filtroGrupos(year)
		self.fields["grupo"].queryset = Grupo.objects.filter(fil)

class notaEstudianteExcel(forms.Form):
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none().order_by("-titulo"), label="Periodo", required=True)
	asignacion = forms.ModelChoiceField(widget=Select2Widget, queryset=AsignacionDocente.objects.none().order_by("-grupo", "asignatura"), label="Asignación", required=True)
	archivo = forms.FileField(label="Archivo excel (.xlsx)", required = True, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	archivo.widget.attrs['accept'] = ".xlsx"

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(notaEstudianteExcel, self).__init__(*args, **kwargs)
		fil = Q(year = year)
		if user != None:
			if user.es_docente():
				fil = fil & Q(docente = user.docente())
			elif user.es_estudiante():
				fil = fil & Q(grupo = user.estudiante().grupo)
		self.fields["asignacion"].queryset = AsignacionDocente.objects.filter(fil)
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year) & Q(es_final = False))

class notaEstudianteIndividual(forms.Form):
	periodo = forms.ModelChoiceField(widget=Select2Widget, queryset=Periodo.objects.none().order_by("-titulo"), label="Periodo", required=True)
	estudiante = forms.ModelMultipleChoiceField(queryset = Estudiante.objects.none(), widget=HeavySelect2Widget(data_url='/json/estudiante'), required = True)
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		year = kwargs.pop('year', None)
		super(notaEstudianteIndividual, self).__init__(*args, **kwargs)
		self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year) & Q(es_final = False))
		self.fields["estudiante"].widget = HeavySelect2Widget(data_url='/json/estudiante')

class EstudiantesListadoBasica(forms.Form):
	grupo = forms.ModelChoiceField(widget=Select2Widget, queryset=Grupo.objects.all().order_by("-titulo"), label="Grupo", required=False)
	grado = forms.ModelChoiceField(widget=Select2Widget, queryset=Grado.objects.all().order_by("-titulo"), label="Grado", required=False)
	sede = forms.ModelChoiceField(widget=Select2Widget, queryset=Sede.objects.all().order_by("-titulo"), label="Sede", required=False)

	ORDENAR = [("apellido", "Apellido"), ("nombre", "Nombres"), ("documento", "Documento"), ("sisben", "Sisben"), ("ars", "ARS"),  ("tipo_documento", "Tipo de documento")]
	ordenar_por = forms.ChoiceField(label="Ordenar por", choices=ORDENAR)

	columnas_blanco = forms.CharField(label="Columnas en blanco", widget=forms.TextInput(attrs={'type':'number', 'min':'0', 'max':'10'}), required=False)
	filas_blanco = forms.CharField(label="Filas en blanco", widget=forms.TextInput(attrs={'type':'number', 'min':'0', 'max':'10'}), required=False)

	incluir_retirados = forms.BooleanField(label="Incluir retirados, cancelados, inactivos, etc", required = False)
	mostrar_solo_retirados = forms.BooleanField(label="Mostrar sólo retirados, cancelados, inactivos etc", required = False)
	mostrar_motivos_no_activo = forms.BooleanField(label="Mostrar Fecha y motivo de retiro del estudiante no activo junto a su nombre", required = False)

	agregar_nro_estudiante = forms.BooleanField(label="Agregar Nro del estudiante", required = False)
	agregar_nro_estudiante.initial=True
	numerar_casillas_blanco = forms.BooleanField(label="Numerar casillas en blanco", required = False)
	numerar_casillas_blanco.initial=True
	mostrar_primero_nombre = forms.BooleanField(label="Mostrar primero nombre y después apellido del estudiante", required = False)
	mostrar_nombre_separado = forms.BooleanField(label="Mostrar Nombre(s) y apellido(s) en diferentes columnas", required = False)


class CamposExportar(forms.Form):
	CAMPOS_EXPORTAR = (
		("apellido_nombre__format", "Apellidos y Nombres"), ("tipo_documento", "Tipo de documento"), ("documento", "Identificación"), ("grupo", "Grupo"),
		("salud__tipo_sangre", "Tipo sangre"), ("estado__format", "Estado"), ("sexo__format", "Sexo"), ("zona__format", "Zona"), ("municipio_expedicion", "Municipio de espedición"),
		("departamento_expedicion", "Departamento de expedición"), ("fecha_nacimiento", "Fecha de nacimiento"),("municipio_nacimiento", "Municipio de nacimiento"),
		("departamento_nacimiento", "Departamento de nacimiento"), ("direccion_residencia", "Dirección de residencia"), ("municipio_residencia", "Municipio de residencia"),
		("telefono", "Teléfono"), ("celular", "Celular"), ("email", "E-mail (correo)"), ("aprobado", "Aprobado"), ("aprobado_pasado", "Aprobado el año pasado"),
		("limitaciones__format", "Limitaciones"), ("eps", "Eps"), ("puntaje_sisben", "Puntaje del sisbén"), ("ars", "Ars"),
		("enfermedades", "Enfermedades"), ("alergias", "Alergias"),	("medicamentos", "Medicamentos"),
		("adicional__desplazado", "Desplazado"), ("adicional__familias_accion", "Familias en acción"), ("adicional__victima_conflicto", "Victima del conflicto"),
		("adicional__alumna_madre_cabeza", "Alumna madre cabeza de familia"), ("adicional__beneficiario_fuerza_publica", "Beneficiario de la fuerza pública"), ("adicional__beneficiario_heroe_nacion", "Beneficiario héroe de la nación"),
		("adicional__sabe_nadar", "¿Sabe nadar?"), ("adicional__dificultades_aprendizaje", "¿Tiene dificultades de aprendizaje?"), ("adicional__lugar_desplazamiento", "Lugar de desplazamiento"),
		("adicional__etnia_indigena", "Étnia indígena"), ("adicional__tipo_vivienda", "Tipo de vivienda"), ("adicional__estrato_socioeconomico", "Estrato socioeconómico"),
		("adicional__ie_proviene", "IE proviene"), ("adicional__municipio_proviene", "Municipio proviene"), ("adicional__departamento_proviene", "Departamento proviene"),
		("adicional__tel_familiar_fuera", "Teléfono familiar por fuera"), ("acudiente__nombre", "Nombre acudiente"), ("acudiente__parentezco", "Parentezco acudiente"),
		("acudiente__edad", "Edad acudiente"), ("acudiente__identificacion", "Identificación acudiente"), ("acudiente__expedido_en", "Expedido en acudiente"),
		("acudiente__ocupacion", "Ocupación acudiente"), ("acudiente__municipio_residencia", "Municipio residencia acudiente"),
		("acudiente__direccion_residencia", "Dirección residencia acudiente"), ("acudiente__telefono", "Teléfono acudiente"), ("acudiente__celular", "Celular acudiente"),
		("acudiente__email", "Email acudiente"), ("acudiente__es_cabeza_familia", "es cabeza de familia acudiente")
	)
	campos = forms.MultipleChoiceField(label="Campos", choices=CAMPOS_EXPORTAR, widget=Select2MultipleWidget)
	campos.widget.attrs["name"] = "campos[]"
	texto_adicional = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, 'cols': 20}), label = "Texto adicional", required = False)

	FORMATO = (("pdf", "PDF"), ("xlsx", "Excel"), ("formato_notas", "Formato Notas"))
	formato = forms.ChoiceField(label="Formato", choices=FORMATO, required=True)


class EstudiantesListadoFitros(forms.Form):
	sexo = forms.ChoiceField(label="Sexo", choices=Estudiante.SEXO_CHOICES, required=False)
	#estado = forms.ChoiceField(label="Estado", choices=Estudiante.ESTADO_CHOICES)
	zona = forms.ChoiceField(label="Zona", choices=Estudiante.ZONA_CHOICES, required=False)
	tipo_sangre = forms.ChoiceField(label="Tipo de sangre", choices=EstudianteSalud.SANGRE, required=False)
	tipo_documento = forms.ChoiceField(label="Tipo de documento", choices=Estudiante.TIPO_DOC_CHOICES, required=False)

	desplazados = forms.ChoiceField(label="Desplazados", choices=(('0', 'Todos'), ('1', 'Sólo desplazados'), ('2', 'Sólo no desplazados')), required=False)
	familias_accion = forms.ChoiceField(label="Familias en acción", choices=(('0', 'Todos'), ('1', 'Sólo incluidos en familias en acción'), ('2', 'Sólo no incluidos en familias en acción')), required=False)

	nacidos_antes = forms.CharField(label="Nacidos antes de ", widget=DatePickerInput(format="YYYY-MM-DD"), required=False)
	nacidos_despues = forms.CharField(label="Nacidos después de ", widget=DatePickerInput(format="YYYY-MM-DD"), required=False)

	ingresados_desde = forms.CharField(label="Ingresados desde ", widget=DatePickerInput(format="YYYY-MM-DD"), required=False)
	ingresados_hasta = forms.CharField(label="Ingresados hasta ", widget=DatePickerInput(format="YYYY-MM-DD"), required=False)

	aprobado = forms.BooleanField(label="Sólo aprobados hasta la fecha", required = False)
	aprobado_pasado = forms.BooleanField(label="Sólo aprobados el año pasado", required = False)

	dividir_grupos = forms.BooleanField(label="Dividir por grupos", required = False)
	dividir_grupos.initial=True




####### Forms edit data student

# for table
class editEstudianteBasico(ModelForm):
	class Meta:
		model = Estudiante
		fields = ["nombre", "apellido", "grupo", "documento", "tipo_documento", "sexo", "estado", "celular1", "celular2"]

# for profile
class editEstudianteLimitaciones(forms.Form):
	limitaciones = forms.ModelMultipleChoiceField(queryset = Limitacion.objects.filter(estado = Limitacion.E_ACTIVO), widget=Select2MultipleWidget, required = False)
	limitaciones.widget.attrs["name"] = "limitaciones[]"
	def __init__(self, *args, **kwargs):
		limitaciones = kwargs.pop('limitaciones', None)
		super(editEstudianteLimitaciones, self).__init__(*args, **kwargs)
		if limitaciones != None:
			self.fields["limitaciones"].initial = limitaciones

class editEstudianteSalud(ModelForm):
	class Meta:
		model = EstudianteSalud
		fields = "__all__"
		exclude = ["estudiante"]

class editEstudianteAcudiente(ModelForm):
	class Meta:
		model = Acudiente
		fields = "__all__"
		exclude = ["year"]

class editEstudiantePersonal(ModelForm):
	class Meta:
		model = Estudiante
		fields = ["nombre", "apellido", "grupo", "documento", "tipo_documento", "municipio_expedicion", "departamento_expedicion", "fecha_nacimiento", "municipio_nacimiento", "departamento_nacimiento", "direccion_residencia", "municipio_residencia", "departamento_residencia", "celular1", "celular2", "email", "sexo", "zona", "estado", "fecha_retiro", "fecha_matricula", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
			'fecha_nacimiento' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
			'fecha_retiro' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

class editEstudianteAdicional(ModelForm):
	class Meta:
		model = EstudianteAdicional
		fields = "__all__"
		exclude = ["estudiante"]

class editEstudianteAvatar(ModelForm):
	class Meta:
		model = Estudiante
		fields = ["foto"]

class editEstudianteAnecdotario(ModelForm):
	class Meta:
		model = Anecdotario
		fields = ["anotaciones_negativas", "anotaciones_positivas"]

class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())


##### Upload excel for students
class formUploadExcel(forms.Form):
	archivo = forms.FileField(required = True, validators=[FileExtensionValidator(allowed_extensions=['xlsx'])])
	archivo.widget.attrs['accept'] = ".xlsx"
