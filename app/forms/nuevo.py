# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets

class editFallo(ModelForm):
	class Meta:
		model = Fallo
		fields = ["observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3})
		}

class editAdministradorAvatar(ModelForm):
	class Meta:
		model = Administrador
		fields = ["foto"]

class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())

class editAdministrador(ModelForm):
	class Meta:
		model = Administrador
		fields = ["nombre", "apellido", "email", "telefono", "sede", "sexo", "celular", "direccion", "estudios"]
		widgets = {
			'direccion': Textarea(attrs={'cols': 20, 'rows': 3}),
			'estudios': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
