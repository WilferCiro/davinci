# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DateTimePickerInput
from ckeditor.widgets import CKEditorWidget
from django_select2.forms import HeavySelect2MultipleWidget, Select2MultipleWidget, Select2Widget
from django.utils.translation import ugettext_lazy as _

class editAnuncioActividades(ModelForm):
	class Meta:
		model = AnuncioActividades
		fields = ["grupo", "docente", "asignatura", "titulo", "fecha_realizacion", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
        	'fecha_realizacion' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm"}),
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editAnuncioActividades, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				fil_gr = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
					fil_gr |= Q(pk = a.grupo.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)
				self.fields["grupo"].queryset = Grupo.objects.filter(fil_gr)

class editMaterialRecuperacion(ModelForm):
	class Meta:
		model = MaterialRecuperacion
		fields = ["grupo", "docente", "asignatura", "periodo", "titulo", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editMaterialRecuperacion, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				fil_gr = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
					fil_gr |= Q(pk = a.grupo.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)
				self.fields["grupo"].queryset = Grupo.objects.filter(fil_gr)

class editMaterialApoyo(ModelForm):
	class Meta:
		model = MaterialApoyo
		fields = ["grupo", "asignatura", "docente", "periodo", "titulo", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editMaterialApoyo, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				fil_gr = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
					fil_gr |= Q(pk = a.grupo.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)
				self.fields["grupo"].queryset = Grupo.objects.filter(fil_gr)


class editBibliotecaVirtual(ModelForm):
	class Meta:
		model = BiliotecaVirtual
		fields = ["titulo", "url", "asignatura", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editBibliotecaVirtual, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)

class ContactoAcudientesForm(forms.Form):
	asunto = forms.CharField(label='Asunto', max_length=100, required = True)
	mensaje = forms.CharField(widget=CKEditorWidget(), required = True)
	estudiantes = forms.ModelMultipleChoiceField(
		queryset = Estudiante.objects.none(),
		widget=HeavySelect2MultipleWidget(
			data_url='/json/estudiante'
		), required = False
	)
	estudiantes.widget.attrs["name"] = "estudiantes[]"
	grupo = forms.ModelChoiceField(label="Grupo", queryset=Grupo.objects.all(), widget=Select2Widget, required = False)


class editEscuelaPadres(ModelForm):
	class Meta:
		model = EscuelaPadres
		fields = ["titulo", "imagen", "cuerpo"]
		labels = {
			"titulo": _("Título"),
			"imagen": _("Imagen"),
			"cuerpo" : _("Cuerpo")
		}

	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editEscuelaPadres, self).__init__(*args, **kwargs)
