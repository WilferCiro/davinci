
# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DateTimePickerInput
from ckeditor.widgets import CKEditorWidget
from django_select2.forms import HeavySelect2MultipleWidget, Select2MultipleWidget, Select2Widget

class editPlanArea(ModelForm):
	class Meta:
		model = PlanArea
		fields = ["docente", "asignatura", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3})
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editPlanArea, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				fil_gr = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
					fil_gr |= Q(pk = a.grupo.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)


class editPlanActividades(ModelForm):
	class Meta:
		model = PlanActividad
		fields = ["docente", "asignatura", "periodo", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3})
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editPlanActividades, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
				ad = AsignacionDocente.objects.filter(Q(docente = user.docente()) & Q(year = year))
				fil_as = Q(pk = 0)
				fil_gr = Q(pk = 0)
				for a in ad:
					fil_as |= Q(pk = a.asignatura.pk)
					fil_gr |= Q(pk = a.grupo.pk)
				fil_as &= Q(year = year)
				self.fields["asignatura"].queryset = Asignatura.objects.filter(fil_as)
				self.fields["periodo"].queryset = Periodo.objects.filter(Q(year = year) & Q(es_final = False))



class editDocumentoInstitucional(ModelForm):
	class Meta:
		model = DocInstitucionales
		fields = ["tema", "titulo", "observaciones", "archivo"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editDocumentoInstitucional, self).__init__(*args, **kwargs)


class editFormato(ModelForm):
	class Meta:
		model = Formato
		fields = ["docente", "archivo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3})
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', '')
		year = kwargs.pop('year', '')
		super(editFormato, self).__init__(*args, **kwargs)
		if user != "" and user != None:
			if user.es_docente():
				self.fields["docente"].queryset = Docente.objects.filter(pk = user.docente().pk)
