# Own libraries
from app.models import *

# Django libraries
from django import forms
from django.forms import ModelForm
from bootstrap_datepicker_plus import DatePickerInput


class editTemas(ModelForm):
	class Meta:
		model = PlaneadorTemas
		fields = "__all__"

class editActividad(ModelForm):
	class Meta:
		model = PlaneadorActividades
		fields = "__all__"
		widgets = {
			'fecha' : DatePickerInput(options={"format":"YYYY-MM-DD"})
		}

class editEvaluacion(ModelForm):
	class Meta:
		model = PlaneadorEvaluacion
		fields = "__all__"
		widgets = {
			'fecha' : DatePickerInput(options={"format":"YYYY-MM-DD"})
		}

class editSuperacion(ModelForm):
	class Meta:
		model = PlaneadorSuperacion
		fields = "__all__"
		widgets = {
			'fecha' : DatePickerInput(options={"format":"YYYY-MM-DD"})
		}
