from django.urls import path
from app.views import main
from app.views import index
from app.views import students
from app.views import docents
from app.views import settings
from app.views import academic, adicional, ajax, nuevo, documentos
from app.views import blog
from app.views import pdf
from app.views import planeador
from app.views import dv_admin


urlpatterns = [
	path('logout/', index.logoutView, name='Logout'),
	path('login/', index.loginView, name='Login'),
	path('inicio/', index.IndexPageDV.as_view(), name='IndexPageDV'),
	path('estudiantes/list/', students.Estudiantes.as_view(), name='EstudiantesPage'),
	path('estudiantes/listado', students.GenerarListado.as_view(), name='EstudiantesGenerarListado'),
	path('estudiantes/EditarNotas/<str:tipo>', students.EditarNotasView.as_view(), name='EditarNotasView'),
	path('estudiantes/NotasIndividual', students.EditarNotasIndividualView.as_view(), name='EditarNotasIndividualView'),
	path('estudiantes/informes', students.GenerarInformes.as_view(), name='GenerarInformes'),
	path('estudiantes/filtro_nota', students.FiltroNotas.as_view(), name='FiltroNotas'),
	path("estudiantes/list/view/<pk>", students.StudentProfile.as_view(), name="StudentProfile"),
	path("estudiantes/list/view", students.StudentProfile.as_view(), name="StudentProfile2"),
	path("estudiantes/AgregarExcel", students.AgregarExcel.as_view(), name="AgregarExcel"),

	path('estudiantes/excel/Notas', students.NotasExcel.as_view(), name="NotasExcel"),
	path('estudiantes/asistencia', students.AsistenciaDiaria.as_view(), name="AsistenciaDiaria"),

	path('docentes/', docents.Docentes.as_view(), name='DocentesPage'),
	path('docentes/JuiciosValorativos/', docents.JuiciosValorativos.as_view(), name='JuiciosValorativos'),
	path('docentes/asignacion/', docents.AsignacionDocenteView.as_view(), name='AsignacionDocenteView'),
	path('docentes/print/JuiciosValorativos/', docents.PrintJuiciosValorativos.as_view(), name='PrintJuiciosValorativos'),
	path("docentes/view/<pk>", docents.DocenteProfile.as_view(), name="DocenteProfile"),
	path("docentes/view", docents.DocenteProfile.as_view(), name="DocenteProfile2"),
	path("docentes/asignacion_guiada", docents.AsignacionGuiada.as_view(), name="AsignacionGuiada"),
	path("docentes/JuiciosExcel", docents.JuiciosExcel.as_view(), name="JuiciosExcel"),
	path("docentes/inasistencias", docents.InasistenciasView.as_view(), name="InasistenciasView"),
	path("docentes/AsignacionesExcel", docents.AsignacionesExcel.as_view(), name="AsignacionesExcel"),

	path('configuracion/informes/', settings.ConfiguracionInformesView.as_view(), name='ConfiguracionInformes'),
	path('configuracion/datos_institucion/', settings.ConfiguracionInstitucionView.as_view(), name='ConfiguracionInstitucion'),
	path('configuracion/periodos/', settings.ConfiguracionPeriodos.as_view(), name='ConfiguracionPeriodos'),
	path('configuracion/sedes/', settings.ConfiguracionSedes.as_view(), name='ConfiguracionSedes'),
	path('configuracion/asignaturas/', settings.ConfiguracionAsignaturas.as_view(), name='ConfiguracionAsignaturas'),
	path('configuracion/areas_fundamentales/', settings.ConfiguracionAreasFundamentales.as_view(), name='ConfiguracionAreasFundamentales'),
	path('configuracion/grados/', settings.ConfiguracionGrados.as_view(), name='ConfiguracionGrado'),
	path('configuracion/ListadoGrupos/', settings.ListadoGrupos.as_view(), name='ListadoGrupos'),
	path('configuracion/ListadoGrupos/view/<pk>', settings.GrupoProfile.as_view(), name='GrupoProfile'),

	path('planeador/temas', planeador.TemasView.as_view(), name='TemasView'),
	path('planeador/actividades', planeador.ActividadesView.as_view(), name='ActividadesView'),
	path('planeador/superacion', planeador.SuperacionView.as_view(), name='SuperacionView'),
	path('planeador/evaluacion', planeador.EvaluacionView.as_view(), name='EvaluacionView'),

	path('academico/anuncio_actividades/', academic.AnuncioActividades.as_view(), name='AnuncioActividades'),
	path('academico/material_recuperacion/', academic.MaterialRecuperacionView.as_view(), name='MaterialRecuperacionView'),
	path('academico/material_apoyo/', academic.MaterialApoyoView.as_view(), name='MaterialApoyoView'),
	path('academico/biblioteca_virtual/', academic.BibliotecaVirtualView.as_view(), name='BibliotecaVirtualView'),
	path('academico/contacto_acudientes/', academic.ContactoAcudientesView.as_view(), name='ContactoAcudientesView'),
	path('academico/escuela_padres/', academic.EscuelaPadresView.as_view(), name='EscuelaPadresView'),

	path('pdf/matricula/<pk>/<year>', pdf.HojaMatricula.as_view(), name='HojaMatricula'),
	path('pdf/boletines/<pk>/<periodo>/<year>', pdf.Boletin.as_view(), name='Boletin'),
	path('pdf/consolidado_grupo/<pk>/<periodo>/<year>', pdf.Consolidado.as_view(), name='Consolidado'),
	path('pdf/constancia_estudio/<pk>/<year>', pdf.ConstanciaEstudio.as_view(), name='ConstanciaEstudio'),
	path('pdf/mejores_promedios_sedes/<pk>/<periodo>/<year>', pdf.MejoresPromediosSedes.as_view(), name='MejoresPromediosSedes'),
	path('pdf/mejores_promedios_grupos/<pk>/<periodo>/<year>', pdf.MejoresPromediosGrupos.as_view(), name='MejoresPromediosGrupos'),
	path('pdf/listado_promedios/<pk>/<periodo>/<year>', pdf.ListadoPromedios.as_view(), name='ListadoPromedios'),
	path('pdf/dane', pdf.DanePDF.as_view(), name='DanePDF'),
	path('pdf/anecdotario/<pk>/<year>', pdf.AnecdotarioView.as_view(), name='AnecdotarioView'),
	path('pdf/certificado_notas/<pk>/<year>', pdf.CertificadoNotas.as_view(), name='CertificadoNotas'),
	path('pdf/asistencia/<pk>/<year>', pdf.AsistenciaDiaria.as_view(), name='AsistenciaDiaria'),
	path('pdf/docentes/inasistencias', pdf.AsistenciaDocentes.as_view(), name='AsistenciaDocentes'),

	path('pdf/estudiantes/listado', pdf.ListadoEstudiantes.as_view(), name='ListadoEstudiantes'),

	path('documentos/institucionales/', documentos.DocumentosInstitucionalesView.as_view(), name='DocumentosInstitucionalesView'),
	path('documentos/plan_area/', documentos.PlanAreaView.as_view(), name='PlanArea'),
	path('documentos/plan_actividades/', documentos.PlanActividadesView.as_view(), name='PlanActividades'),
	path('documentos/formatos/', documentos.FormatosView.as_view(), name='FormatosView'),

	#path('pdf/promedio_asignaturas/<pk>/<periodo>/<year>', pdf.PromedioAsignaturas.as_view(), name='PromedioAsignaturas'),
	#path('pdf/promedio_docentes/<pk>/<periodo>/<year>', pdf.PromedioDocentes.as_view(), name='PromedioDocentes'),

	path('adicional/noticias/', adicional.NoticiasBlogView.as_view(), name='NoticiasBlogView'),
	path('adicional/dane/', adicional.Dane.as_view(), name='Dane'),
	path('adicional/calendario/', adicional.CalendarioView.as_view(), name='CalendarioView'),
	path('adicional/salidas_escolares/', adicional.SalidasEscolares.as_view(), name='SalidasEscolares'),
	path('adicional/estadisticas/', adicional.EstadisticasView.as_view(), name='EstadisticasView'),
	path('adicional/icfes/', adicional.IcfesView.as_view(), name='IcfesView'),

	path('app_blog/mensajes/', settings.MensajesBlog.as_view(), name='MensajesBlog'),
	path("search/", index.SearchPage.as_view(), name="SearchPagina"),

	path('json/estudiante/', ajax.EstudianteAjax.as_view(), name='EstudianteJson'),
	path('json/docentes/', ajax.DocentesAjax.as_view(), name='DocentesAjax'),

	path('api/icfes/', ajax.IcfesViewSet.as_view(), name='IcfesAPI'),

	path('changeyear/', index.ChangeYearView, name='ChangeYearView'),

	path('cambios/', nuevo.Cambios.as_view(), name='Cambios'),
	path('reporte_fallas/', nuevo.FallosView.as_view(), name = 'FallosView'),

	path('administradores/', nuevo.AdministradoresView.as_view(), name = 'AdministradoresView'),
	path('administradores/view/<pk>', nuevo.AdministradorProfile.as_view(), name = 'AdministradorProfile'),

	path('', blog.IndexPage.as_view(), name='IndexPage'),
	path('blog/adicional', blog.AdicionalPage.as_view(), name='AdicionalPage'),
	path('blog/fotos', blog.GalleryPage.as_view(), name='GalleryPage'),
	path('blog/contacto/', blog.ContactPage.as_view(), name='ContactPage'),
	path('blog/docentes/', blog.DocentesPageBlog.as_view(), name='DocentesPageBlog'),
	path('blog/calendario/', blog.CalendarioPage.as_view(), name='CalendarioPage'),
	path('blog/noticia/<pk>', blog.NoticiaPage.as_view(), name='NoticiaPage'),
	path('blog/escuela_padres/<pk>', blog.EscuelaPage.as_view(), name='EscuelaPage'),

	# ADMINS
	path('dv_admin/add_data', dv_admin.excelData.as_view(), name='AddDataAdmin'),
]
