from django.contrib import admin
from app.models import *
from django.contrib.auth.admin import UserAdmin


from django.contrib.auth.forms import UserChangeForm

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm

    fieldsets = (
            (None, {'fields': (
            	'last_name',
            	'first_name',
            	'username',
            	'password',
            	'email',
            	'perfil'
            )}),
    )


admin.site.register(User, MyUserAdmin)

# Register your models here.
admin.site.register(Sede)
admin.site.register(Grupo)
admin.site.register(GrupoDocente)
admin.site.register(Grado)
admin.site.register(Periodo)
admin.site.register(Docente)
admin.site.register(Estudiante)
admin.site.register(EstudianteSalud)
admin.site.register(EstudianteAdicional)
admin.site.register(EstudianteAcudiente)
admin.site.register(Acudiente)
admin.site.register(Anecdotario)
admin.site.register(Fallo)
admin.site.register(Icfes)
admin.site.register(Inasistencia)
admin.site.register(LimitacionEstudiante)
admin.site.register(Limitacion)

admin.site.register(Valoracion)

admin.site.register(Enfermedades)
admin.site.register(EnfermedadesEstudiante)

admin.site.register(Administrador)
admin.site.register(AreaFundamental)
admin.site.register(Asignatura)
admin.site.register(ConfiguracionInstitucion)
admin.site.register(ConfiguracionInformes)

admin.site.register(AnuncioActividades)
admin.site.register(JuicioValorativo)
admin.site.register(DocInstitucionales)
admin.site.register(TemasDocInstitucionales)
admin.site.register(NoticiasBlog)

admin.site.register(AsignacionDocente)

admin.site.register(Nota2016)
admin.site.register(Nota2017)
admin.site.register(Nota2018)
admin.site.register(Nota2019)
admin.site.register(Nota2020)
admin.site.register(Nota2021)
admin.site.register(Nota2022)
admin.site.register(Nota2023)
