# Own libraries
from app.views.main import Main, MainTable
from app.models import *
from app.forms.nuevo import *

# Django libraries
from django.views import View
from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils.translation import ugettext_lazy as _


# System libraries
import json

class Cambios(Main, View):
	"""
		view of cambios page
	"""
	template = 'plus/cambios.html'
	page_title = "Cambios en la plataforma"

	def get_data(self, request, kwargs):
		with open('version_control.json') as json_file:
			data = json.load(json_file)
		data_return = {"all_cambios" : data}
		return data_return

class FallosView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Reporte de fallos")
	register_name = _("reporte de fallo")
	form_action = "/reporte_fallas/"
	model_object = Fallo
	table_columns =	dict(pk = "ID", estado__format = "Estado", user__format = "Usuario", observaciones = "Observaciones", fecha_modificacion__date_format = "Fecha de Modificación")
	return_edit_columns = ["observaciones"]
	form_edit = editFallo
	form_add = editFallo
	delete_register = True
	docente_add_delete = True
	mostrar_control_activos = True

	def _getFormatRow(self, column, obj):
		if column == "user":
			if obj.user != None:
				if obj.user.es_docente():
					return "Doc. " + obj.user.docente().get_full_name()
				elif obj.user.es_admin():
					return "Admon. " + obj.user.administrador().get_full_name()
			return "N/A"
		elif column == "estado":
			return obj.getEstado()
		return ""

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Reporte de fallas"), "")
		if self.user.es_estudiante():
			self.can_view_page = False

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(pk__icontains=value))
		return fil

	def _addData(self, request):
		if request.user.es_admin() or request.user.es_docente():
			form_edit = self.form_add(request.POST)
			if form_edit.is_valid():
				fallo = Fallo(observaciones = form_edit.cleaned_data["observaciones"], user = self.user)
				fallo.save()
				return True
			else:
				self._add_error(form_edit.errors)
		return False

	def _preProccess(self, request):
		user = request.user
		if user.es_admin() or user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True

	def can_delete_row(self, e):
		if self.user == e.user:
			return True
		return False

	def can_edit_row(self, e):
		if self.user == e.user:
			return True
		return False


class AdministradoresView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Administradores")
	register_name = _("administrador")
	form_action = "/administradores/"
	model_object = Administrador
	table_columns =	dict(apellido = "Apellido", nombre = "Nombre", email = "E-mail", sede = "Sede")
	return_edit_columns = []
	form_edit = None
	form_add = None
	delete_register = False
	docente_add_delete = False
	mostrar_control_activos = False
	view_aparte = True
	can_view = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Administradores"), "")
		if not self.user.es_admin():
			self.can_view_page = False

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(apellido__icontains = value) | Q(nombre__icontains=value) | Q(email__icontains = value))
		return fil

class AdministradorProfile(Main, View):
	template = "administrador/profile.html"
	page_title = "Perfil de administrador"

	def get_data(self, request, kwargs):
		try:
			user_id = int(kwargs['pk'])
			administrador = Administrador.objects.get(Q(pk = user_id))
		except:
			self.can_view_page = False
			return {}

		if not self.user.es_admin():
			self.can_view_page = False
			return {}

		propio_perfil = False
		if self.user.administrador() == administrador:
			propio_perfil = True

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Perfil de administrador", "")

		data_return = {}

		administrador_data = {
			"pk" : administrador.pk,
			"nombre" : administrador.nombre,
			"apellido" : administrador.apellido,
			"email" : administrador.email,
			"telefono" : administrador.telefono,
			"celular" : administrador.celular,
			"direccion" : administrador.direccion,
			"sede" : administrador.sede,
			"genero" : administrador.getSexo(),
			"creation_date" : administrador.user.date_joined,
			"last_login" : administrador.user.last_login,
			"foto" : administrador.foto,
			"username" : administrador.user.username,
			"estudios" : administrador.estudios
		}

		if propio_perfil:
			data_return["form_action"] = "/administradores/view/" + str(user_id)
			data_return["form_edit_avatar"] = editAdministradorAvatar(instance = administrador)
			data_return["form_edit_pass"] = changePass()
			data_return["form_edit_data"] = editAdministrador(instance = administrador)

		data_return["administrador"] = administrador_data
		data_return["propio_perfil"] = propio_perfil
		return data_return

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)

		if tipo_edit == "datos_completos" and object_id != None:
			try:
				administrador = Administrador.objects.get(pk = object_id)
				if self.user.es_admin() and self.user == administrador.user:
					form = editAdministrador(data = request.POST, files = request.FILES, instance = administrador)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				self._add_error("Error al editar datos")
				pass

		elif tipo_edit == "user_avatar" and object_id != None:
			try:
				administrador = Administrador.objects.get(pk = object_id)
				if self.user.es_admin() and self.user == administrador.user:
					form = editAdministradorAvatar(data = request.POST, files = request.FILES, instance = administrador)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		elif tipo_edit == "password" and object_id != None:
			try:
				administrador = Administrador.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and self.user.es_admin() and self.user == administrador.user:
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						administrador.user.set_password(pass1)
						administrador.user.save()
						return True
					else:
						self._add_error("Las contraseñas no coinciden")
				else:
					self._add_error("No se puede editar la contraseña")
			except Exception as ex:
				print(ex)

		return False
