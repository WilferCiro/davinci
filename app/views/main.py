from django.shortcuts import render
from django.views import View
from app.models import *
from django.http import JsonResponse
from django.db.models import Q
from django.core.files.storage import default_storage
from django.shortcuts import HttpResponseRedirect, HttpResponse
import os.path
from django.conf import settings
from django.utils.formats import localize, date_format

from django.utils import translation
from django.utils.translation import ugettext_lazy as _

from app.utils.macros import *

class Main(object):
	"""
		Main Class
	"""
	template = "structure.html"
	menu = []
	js_files = []
	css_files = []
	breadcrumb = []
	errors = []
	page_title = "Da Vinci"
	aditional_templates = []
	can_view_page = True
	nro_notificaciones = 0

	def __init__(self):
		"""
			Constructor of the main class
		"""
		self.__empty_all()

	def __get_menu(self, path):
		"""
			Set the menu of application
			@param user with the user data
		"""
		menu = []
		submenu = []
		submenu.append(dict(label = "Estudiantes", url = "/estudiantes/list", icon = "fa fa-graduation-cap", item_class = "", content = "Boletines, hoja de matrícula, constancia estudio, anecdotario, certificado notas, estado disciplinario"))
		if self.user.es_admin():
			submenu.append(dict(label = "Agregar por excel", url = "/estudiantes/AgregarExcel", icon = "fa fa-file-excel-o", item_class = "", content = "agregar estudiantes, importar"))
		if not self.user.es_estudiante():
			submenu.append(dict(label = "Notas", url = "/estudiantes/EditarNotas/Normal", icon = "fa fa-edit", item_class = "", content = ""))
			submenu.append(dict(label = "Notas Individual", url = "/estudiantes/NotasIndividual", icon = "fa fa-pencil-square", item_class = "", content = ""))
			submenu.append(dict(label = "Recuperaciones", url = "/estudiantes/EditarNotas/Recuperacion", icon = "fa fa-pencil", item_class = "", content = ""))

			submenu.append(dict(label = "Notas a través de excel", url = "/estudiantes/excel/Notas", icon = "fa fa-table", item_class = "", content = ""))
			submenu.append(dict(label = "Listado", url = "/estudiantes/listado", icon = "fa fa-list", item_class = "", content = ""))
			submenu.append(dict(label = "Informes", url = "/estudiantes/informes", icon = "fa fa-tasks", item_class = "", content = "Boletines, promedios, lista ordenada, Excel, PDF, exportar"))
			submenu.append(dict(label = "Filtro notas", url = "/estudiantes/filtro_nota", icon = "fa fa-filter", item_class = "", content = "Excel, PDF, exportar"))
		#submenu.append(dict(label = "Restaurante", url = "/estudiantes/Restaurante", icon = "fa fa-cutlery", item_class = "", content = ""))
		submenu.append(dict(label = "Asistencia diaria", url = "/estudiantes/asistencia	", icon = "fa fa-calendar", item_class = "", content = "Inasistencia, retardos, faltas"))
		menu.append(dict(label = "Estudiantes", url = "", icon = "fa fa-graduation-cap", item_class = "", submenu = submenu))

		submenu = []
		submenu.append(dict(label = "Docentes", url = "/docentes", icon = "fa fa-user", item_class = "", content = "Lista de los docentes, perfil de los docentes"))
		submenu.append(dict(label = "Ver juicios valorativos", url = "/docentes/JuiciosValorativos", icon = "fa fa-check-square", item_class = "", content = ""))
		submenu.append(dict(label = "Juicios excel", url = "/docentes/JuiciosExcel", icon = "fa fa-check-square", item_class = "", content = ""))
		submenu.append(dict(label = "Imprimir juicios V.", url = "/docentes/print/JuiciosValorativos", icon = "fa fa-print", item_class = "", content = ""))
		submenu.append(dict(label = "Asignación", url = "/docentes/asignacion", icon = "fa fa-check-circle-o", item_class = "", content = ""))
		if self.user.es_admin():
			submenu.append(dict(label = "Asignación guíada", url = "/docentes/asignacion_guiada", icon = "fa fa-arrow-right", item_class = "", content = "Asignación, guía"))
			submenu.append(dict(label = "Inasistencias", url = "/docentes/inasistencias", icon = "fa fa-user", item_class = "", content = "Inasistencias"))
			submenu.append(dict(label = "Administradores", url = "/administradores", icon = "fa fa-user", item_class = "", content = "Perfil administradores"))
		menu.append(dict(label = "Docentes", url = "", icon = "fa fa-users", item_class = "", submenu = submenu))

		submenu = []
		if self.user.es_admin():
			submenu.append(dict(label = "Institución", url = "/configuracion/datos_institucion", icon = "fa fa-flag", item_class = "", content = "configuración"))
			submenu.append(dict(label = "Informes", url = "/configuracion/informes", icon = "fa fa-gear", item_class = "", content = "configuración"))
		submenu.append(dict(label = "Periodos académicos", url = "/configuracion/periodos", icon = "fa fa-calendar-times-o", item_class = "", content = "configuración"))
		submenu.append(dict(label = "Asignaturas", url = "/configuracion/asignaturas", icon = "fa fa-book", item_class = "", content = ""))
		submenu.append(dict(label = "Áreas fundamentales", url = "/configuracion/areas_fundamentales", icon = "fa fa-bookmark", item_class = "", content = ""))
		submenu.append(dict(label = "Sedes", url = "/configuracion/sedes", icon = "fa fa-institution", item_class = "", content = ""))
		if not self.user.es_estudiante():
			submenu.append(dict(label = "Grados", url = "/configuracion/grados", icon = "fa fa-list-alt", item_class = "", content = ""))
			submenu.append(dict(label = "Grupos", url = "/configuracion/ListadoGrupos", icon = "fa fa-group", item_class = "", content = ""))
		menu.append(dict(label = "Ajustes", url = "", icon = "fa fa-gears", item_class = "", submenu = submenu))

		submenu = []
		submenu.append(dict(label = "Institucionales", url = "/documentos/institucionales", icon = "fa fa-file-text-o", item_class = "", content = ""))
		submenu.append(dict(label = "Plan de área", url = "/documentos/plan_area", icon = "fa fa-file-text-o", item_class = "", content = ""))
		submenu.append(dict(label = "Plan de actividades", url = "/documentos/plan_actividades", icon = "fa fa-file-text-o", item_class = "", content = ""))
		if not self.user.es_estudiante():
			submenu.append(dict(label = "Formatos", url = "/documentos/formatos", icon = "fa fa-file-pdf-o", item_class = "", content = ""))
		menu.append(dict(label = "Documentos", url = "", icon = "fa fa-plus", item_class = "", submenu = submenu))

		submenu = []
		if self.user.es_admin():
			submenu.append(dict(label = "DANE", url = "/adicional/dane", icon = "fa fa-table", item_class = "", content = ""))
			#submenu.append(dict(label = "Acta meci", url = "/adicional/acta_meci", icon = "fa fa-file-text-o", item_class = "", content = ""))
		submenu.append(dict(label = "Estadísticas", url = "/adicional/estadisticas", icon = "fa fa-pie-chart", item_class = "", content = "graficas"))
		submenu.append(dict(label = "Icfes", url = "/adicional/icfes", icon = "fa fa-check-circle-o", item_class = "", content = "preguntas, selección múltiple"))
		submenu.append(dict(label = "Calendario", url = "/adicional/calendario", icon = "fa fa-calendar-plus-o", item_class = "", content = "fechas"))
		#submenu.append(dict(label = "Registro de actividades", url = "/adicional/registro_actividades", icon = "fa fa-pencil-square", item_class = "", content = ""))
		submenu.append(dict(label = "Salidas escolares", url = "/adicional/salidas_escolares", icon = "fa fa-road", item_class = "", content = "mapa"))
		if self.user.es_admin():
			submenu.append(dict(label = "Arins", url = "/adicional/arins", icon = "fa fa-money", item_class = "", content = "inventario"))
		menu.append(dict(label = "Más funciones", url = "", icon = "fa fa-plus", item_class = "", submenu = submenu))

		submenu = []
		if self.user.es_admin():
			submenu.append(dict(label = "Contacto acudientes", url = "/academico/contacto_acudientes", icon = "fa fa-twitch", item_class = "", content = "correo, email"))
		submenu.append(dict(label = "Material de apoyo", url = "/academico/material_apoyo", icon = "fa fa-object-group", item_class = "", content = ""))
		submenu.append(dict(label = "Material de recuperación", url = "/academico/material_recuperacion", icon = "fa fa-paper-plane", item_class = "", content = ""))
		submenu.append(dict(label = "Anuncio de actividades", url = "/academico/anuncio_actividades", icon = "fa fa-commenting", item_class = "", content = ""))
		submenu.append(dict(label = "Biblioteca virtual", url = "/academico/biblioteca_virtual", icon = "fa fa-book", item_class = "", content = ""))
		submenu.append(dict(label = "Escuela de padres", url = "/academico/escuela_padres", icon = "fa fa-search", item_class = "", content = ""))
		menu.append(dict(label = "Académico", url = "", icon = "fa fa-sticky-note", item_class = "", submenu = submenu))

		submenu = []
		if self.user.es_admin():
			submenu.append(dict(label = "Noticias blog", url = "/adicional/noticias", icon = "fa fa-newspaper-o", item_class = "", content = ""))
			submenu.append(dict(label = "Mensajes del blog", url = "/app_blog/mensajes", icon = "fa fa-twitch", item_class = "", content = "peticiones"))
		submenu.append(dict(label = "Ver el blog", url = "/", icon = "fa fa-eye", item_class = "", content = ""))
		menu.append(dict(label = "Blog", url = "", icon = "fa fa-html5", item_class = "", submenu = submenu))

		submenu = []
		submenu.append(dict(label = "Temas", url = "/planeador/temas", icon = "fa fa-newspaper-o", item_class = "", content = "temas"))
		submenu.append(dict(label = "Actividades", url = "/planeador/actividades", icon = "fa fa-newspaper-o", item_class = "", content = "plan de actividades, planeador"))
		submenu.append(dict(label = "Plan de superación", url = "/planeador/superacion", icon = "fa fa-twitch", item_class = "", content = "plan de superación, planeador"))
		submenu.append(dict(label = "Plan de evaluación", url = "/planeador/evaluacion", icon = "fa fa-eye", item_class = "", content = "plan de evaluación"))
		submenu.append(dict(label = "Organizador", url = "/planeador/organizador", icon = "fa fa-eye", item_class = "", content = "organizador de planeador"))
		menu.append(dict(label = "Planeador", url = "", icon = "fa fa-list-alt", item_class = "", submenu = submenu))

		submenu = []
		submenu.append(dict(label = "Cambios", url = "/cambios", icon = "fa fa-newspaper-o", item_class = "", content = "Actualizaciones del sistema, cambios, adiciones"))
		submenu.append(dict(label = "Reporte de fallas", url = "/reporte_fallas", icon = "fa fa-twitch", item_class = "", content = "Errores, reporte de fallas"))
		menu.append(dict(label = "Extra", url = "", icon = "fa fa-plus", item_class = "", submenu = submenu))

		submenu = []
		for year in FECHA_CHOOSER:
			submenu.append(dict(label = str(year[0]), url = "/changeyear?year=" + str(year[0]), icon = "fa fa-newspaper-o", item_class = "", content = "Cambio Año " + str(year[0])))
		menu.append(dict(label = "Años (" + str(self.year) + ")", url = "", icon = "fa fa-plus", item_class = "", submenu = submenu))

		for men in menu:
			item = next((item for item in men["submenu"] if (str(path)[0:-1]).lower() in str(item["url"]).lower()), False)
			if item != False:
				item["item_class"] = "active"
				men["item_class"] = "active"
				break

		self.menu = menu

	def __empty_all(self):
		"""
			Prevent data repeat
		"""
		self.js_files = []
		self.css_files = []
		self.aditional_templates = []
		self.breadcrumb = []
		self.menu = []
		self.errors = []

	def _add_error(self, error):
		"""
			Adds a error for show
			@param error as str
		"""
		error_2 = error
		try:
			error_2 = error.as_text()
		except:
			pass
		self.errors.append(error_2)

	def _add_css(self, css_file):
		"""
			Adds a css file to document
			@param css_file as css file path inside static/
		"""
		if css_file not in self.css_files:
			self.css_files.append(css_file)

	def _add_js(self, js_file):
		"""
			Adds a js file to document
			@param js_file as js file path inside static/
		"""
		if js_file not in self.js_files:
			self.js_files.append(js_file)


	def _add_aditional_template(self, template):
		"""
			Adds a template for include to document
			@param template as html file path inside static/
		"""
		if template not in self.aditional_templates:
			self.aditional_templates.append(template)

	def _add_breadcrumb(self, label, url):
		"""
			Adds a item of the breadcrumb
			@param label as text to show, string
			@param url as url to redirect
		"""
		self.breadcrumb.append(dict(label = label, url = url))

	def _count_notificaciones(self, request):
		self.nro_notificaciones = 0
		import datetime
		x = datetime.datetime.now()
		fecha_desde = x.strftime('%Y-%m-%d')
		fecha_hasta = x.strftime('%Y-%m-%d')
		self.nro_notificaciones = Calendario.objects.filter(Q(fecha_inicio__range = (fecha_desde, fecha_hasta)) | Q(fecha_fin__range = (fecha_desde, fecha_hasta))).count()

	def initialProccess(self, request, kwargs):
		"""
			Organices the initial data
		"""
		self.user = request.user
		#self._count_notificaciones(request)

		data_page = dict()
		data_page["normal_response"] = True
		data_page["return_response"] = None


		if self.user.is_anonymous or not self.user.is_authenticated:
			success = request.GET.get("success", None)
			error = request.GET.get("error", None)
			logged = request.GET.get("logged", None)
			self.template = "login.html"
			data_page["success"] = success
			data_page["error"] = error
			data_page["logged"] = logged
			return data_page
		else:
			data_page["year"] = str(self.year)
			data_page["min_lost"] = self.min_lost
			data_page["year_list"] = FECHA_CHOOSER
			self.__get_menu(str(request.path))
			data_page.update(self.get_data(request, kwargs))
			data_page["menu"] = self.menu
			data_page["user"] = self.user

			user_profile = None
			if self.user.es_admin():
				user_profile = self.user.administrador()
			elif self.user.es_docente():
				user_profile = self.user.docente()
			elif self.user.es_estudiante():
				user_profile = self.user.estudiante()

			if user_profile != None:
				data_page["datos_user"] = {
					"nombre" : user_profile.nombre,
					"foto" : user_profile.foto,
					"pk" : user_profile.pk
				}
			else:
				data_page["datos_user"] = {
					"nombre" : "NA",
					"foto" : "",
					"pk" : self.user.pk
				}


			data_page["can_view_page"] = self.can_view_page
			if self.can_view_page:
				data_page["breadcrumb"] = self.breadcrumb
				data_page["page_title"] = self.page_title
				data_page["aditional_templates"] = self.aditional_templates
				data_page["js_files"] = self.js_files
				data_page["css_files"] = self.css_files
				try:
					data_page["last_error"] = ", ".join(request.session['last_error'])
				except:
					data_page["last_error"] = ""
			else:
				data_page["page_title"] = _("Prohibido")
		try:
			translation.activate(request.session[translation.LANGUAGE_SESSION_KEY])
			data_page["lang"] = request.session[translation.LANGUAGE_SESSION_KEY]
		except Exception as e:
			data_page["lang"] = "es"
		#data_page["nro_notificaciones"] = self.nro_notificaciones
		request.session['last_error'] = ""
		return data_page

	def _essentialData(self, request):
		"""
			Organices the pre data in view like yeae and user
		"""
		self.max_nota = MAX_NOTA
		self.min_nota = MIN_NOTA
		self.min_lost = 3.0
		self.perdidas_pei = 3
		if request.user.is_authenticated:
			try:
				self.year = int(request.session['year'])
			except:
				now = datetime.datetime.now()
				request.session['year'] = now.year
				self.year = int(now.year)

			self.user = request.user
			self._preProccess(request)

	def get(self, request, *args, **kwargs):
		"""
			Execute when the page makes a request
			@param request as request data
			@return render to response data
		"""
		self._essentialData(request)
		data_page = self.initialProccess(request, kwargs)
		if self.can_view_page:
			if not data_page["normal_response"]:
				if type(data_page["return_response"]) == HttpResponseRedirect or type(data_page["return_response"]) == HttpResponse:
					return data_page["return_response"]
				else:
					return data_page["return_response"](data_page, self.template, request.build_absolute_uri())
			else:
				return render(request, self.template, data_page)
		else:
			return render(request, "index.html", data_page)


	def _preProccess(self, request):
		pass

	def post(self, request, *args, **kwargs):
		"""
			Proccess the post data
			@param request as request data
		"""
		self._essentialData(request)
		success = False
		if request.POST.get("draw", None) != None:
			dataRet = self._dataTable(request)
			return dataRet

		elif request.POST.get("ajaxRequest", None) != None:
			ajaxR = self._ajaxRequest(request)
			return ajaxR

		elif request.POST.get("getObjectData", None) != None:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				ajaxR = self._getObjectData(object_id)
				return ajaxR
			else:
				success = False

		elif request.POST.get("tipoForm", None) != None:
			tipo = request.POST.get("tipoForm", None)

			if tipo == "editData":
				success = self._editData(request)
				if type(success) != HttpResponseRedirect:
					prev_dir = request.POST.get("prev_dir", None)
					request.session['last_error'] = self.errors
					if prev_dir != None:
						if "?" in prev_dir:
							return HttpResponseRedirect(str(prev_dir) + "&success="+str(success))
						else:
							return HttpResponseRedirect(str(prev_dir) + "?success="+str(success))
					else:
						return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				else:
					return success

			elif tipo == "addData":
				success = self._addData(request)
				if type(success) != HttpResponseRedirect:
					request.session['last_error'] = self.errors
					return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				else:
					return success

			elif tipo == "removeData":
				success = self._removeData(request)
				if type(success) != HttpResponseRedirect:
					request.session['last_error'] = self.errors
					return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				else:
					return success

			elif tipo == "informeForm":
				return self._proccessInforme(request)

			elif tipo == "sendData":
				success = self._sendData(request)
				request.session['last_error'] = self.errors
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))

			else:
				self._nothingPost()

		else:
			self._nothingPost()

		data_page = self.initialProccess(request, kwargs)
		data_page["success"] = success
		data_page["last_error"] = self.errors
		return render(request, self.template, data_page)

	def _nothingPost(self):
		print("Nothing")

	def get_data(self, request, *args):
		"""
			Returns the data to show
			@param request as request data
			is abstract
		"""
		pass


class MainTable(Main):
	"""
		Main table class
	"""
	#### Tables definition elements
	template = "tablas.html"
	# Dict with Column user and Column back
	table_columns = dict(titulo = _("Título"))
	# Columns to return in _GetData
	return_edit_columns = []

	model_object = None
	register_name = ""
	form_action = "/inicio"
	mostrar_control_activos = False
	form_filter = False

	can_add = False
	can_edit = False
	can_delete = False
	form_edit = None
	form_add = None
	delete_register = False

	can_view = False
	view_aparte = False

	order_table = 0

	# Muestra el formulario en una página aparte, úsese sólamente cuando el registro tenga archivos
	edit_aparte = False

	aditional_buttons = ""

	def _checkPermission(self, object_id, user):
		return True

	def get_data(self, request, kwargs):
		"""
			Obtiene los datos predeterminados de la página
		"""
		editarGET = request.GET.get("EditRegister", None)
		addGET = request.GET.get("AddRegister", None)

		if (((editarGET != None and self.can_edit) or (addGET != None and self.can_add)) and self.edit_aparte):
			return_val = self._get_data_tables(request)
			self.template = "form.html"
			object_id = ""
			forms = []
			object_inst = None
			if editarGET != None and self.can_edit:
				object_id = request.GET.get("object_id", None)
				#if self._checkPermission(objeto_id, user):
				#	continue
				try:
					object_inst = self.model_object.objects.get(pk = object_id)
					if not self.can_edit_row(object_inst):
						self.can_view_page = False
						return {}
					tipo_form = "editData"
					forms.append(dict(href="first", completa="True", tab=_("Editar registro"), form = self.form_edit(prefix = "edit", instance = object_inst, user = self.user, year = self.year)))
				except Exception as e:
					print(e)
					tipo_form = "addData"
					forms.append(dict(href="first", completa="True", tab=_("Añadir registro"), form = self.form_add(user = self.user, year = self.year)))
			elif addGET != None and self.can_add:
				tipo_form = "addData"
				forms.append(dict(href="first", completa="True", tab=_("Añadir registro"), form = self.form_add(user = self.user, year = self.year)))

			if return_val == None:
				return_val = dict()
			return_val["cancelar_link"] = self.form_action
			return_val["register_name"] = self.register_name
			return_val["form_action"] = self.form_action
			return_val["forms"] = forms
			return_val["target"] = ""
			return_val["tipo_form"] = tipo_form
			return_val["object_id"] = object_id
			return_val["title_form"] = self._get_title_form(object_inst)
			return return_val

		# Table Views
		if self.form_edit != None and self.edit_aparte == False and self.can_edit:
			self._add_aditional_template("varios/dialogo_editar.html")

		if self.form_add != None and self.edit_aparte == False and self.can_add:
			self._add_aditional_template("varios/dialogo_agregar.html")

		if self.delete_register and self.can_delete:
			self._add_aditional_template("varios/dialogo_eliminar.html")

		# Add css and javascript files
		self._add_js("jquery.dataTables.min.js")
		self._add_js("dataTables.bootstrap4.min.js")
		self._add_js("tablas.js")
		self._add_css("dataTables.bootstrap4.min.css")
		self._add_css("dropdown_extension.css")

		return_val = self._get_data_tables(request)

		self.columns = [""]
		for key in self.table_columns.keys():
			self.columns.append(self.table_columns[key])
		self.columns.append(_("Ops."))

		# Valores
		if return_val == None:
			return_val = dict()
		return_val["register_name"] = self.register_name
		return_val["form_action"] = self.form_action
		return_val["columns"] = self.columns
		if self.form_edit != None:
			if type(self.form_edit) == list:
				return_val["form_edit"] = self.form_edit
			else:
				if self.form_filter:
					return_val["form_edit"] = self.form_edit(prefix = "edit", user = self.user, year = self.year)
				else:
					return_val["form_edit"] = self.form_edit(prefix = "edit")
		if self.form_add != None:
			if type(self.form_add) == list:
				return_val["form_add"] = self.form_add
			else:
				if self.form_filter:
					return_val["form_add"] = self.form_add(user = self.user, year = self.year)
				else:
					return_val["form_add"] = self.form_add()
		return_val["mostrar_control_activos"] = self.mostrar_control_activos
		return_val["delete_register"] = self.delete_register
		#return_val["docente_add_delete"] = self.docente_add_delete
		return_val["can_add"] = self.can_add
		return_val["edit_aparte"] = self.edit_aparte
		return_val["success"] = request.GET.get("success", None)

		# return data
		return return_val

	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_edit:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				form_edit = self.form_edit(prefix = "edit", data = request.POST, files = request.FILES, instance=sed)
				if form_edit.is_valid():
					form_edit.save()
					return True
			self._add_error(form_edit.errors)
		else:
			self._add_error(_("Usted no tiene permisos para editar este objeto"))
		return False

	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			form_add = self.form_add(data = request.POST, files = request.FILES)
			if form_add.is_valid():
				form_add.save()
				return True
			self._add_error(form_add.errors)
		else:
			self._add_error(_("Usted no tiene permisos para añadir este objeto"))
		return False

	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			#if self._checkPermission(object_id, user):
			sed = self.model_object.objects.get(pk = object_id)
			sed.delete()
			return True
		else:
			self._add_error(_("Usted no tiene permisos para eliminar este objeto"))
		return False


	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		obj_id = 0
		if action == "editar_modal":
			data = self._editData(request)
		if action == "agregar_modal":
			data = self._addData(request)
		if action == "eliminar_modal":
			data = self._deleteData(request)

		if type(data) is list or type(data) is tuple:
			error = not(data[0])
			obj_id = data[1]
		else:
			error = not(data)
		data = {
			"error" : error,
			"errores" : ", ".join(self.errors),
			"obj_id" : obj_id,
		}
		return JsonResponse(data)

	def _getObjectData(self, object_id):
		"""
			Returns the object data for complete the edit fields
			@param object_id as int
		"""
		object_data = self.model_object.objects.get(pk = object_id)
		data = {
			'object_id' : object_data.pk,
			'get_json' : True,
			'json_register_name' : str(object_data)
		}
		for key in self.return_edit_columns:
			if len(key) == 2 and type(key) in (tuple, list):
				if key[1] == "date_format":
					data[key[0]] = localize(str(getattr(object_data, key[0])).replace("None", ""))
				else:
					value = str(getattr(getattr(object_data, key[0]), key[1]))
					key_item = key[0]
					data[key_item] = value
			else:
				data[key] = str(getattr(object_data, key))
		return JsonResponse(data)

	def _dataTable(self, request):
		"""
			Return data of dataTable
			@param request as request data
		"""
		value = request.POST.get("search[value]", None)
		start = int(request.POST.get("start", self.order_table))
		length = int(request.POST.get("length", 15))

		filters = ["pk"]
		for key in self.table_columns.keys():
			if not "__format" in key:
				filters.append(key.replace("__date_format", ""))
			else:
				filters.append(self.order_rows(key.replace("__format", "")))
		filters.append("pk")

		order = int(request.POST.get("order[0][column]", len(filters)))
		add = ""
		direction = request.POST.get("order[0][dir]", 'desc')

		if direction == "desc":
			add = "-"

		fil = self._getFilerTable(value)

		solo_activos = request.POST.get("solo_activos", "true")
		if solo_activos == "true":
			fil = fil & Q(estado = self.model_object.E_ACTIVO)

		if fil == Q():
			data = self.model_object.objects.all().order_by(add + filters[order])[start:start+length]
		else:
			data = self.model_object.objects.filter(fil).order_by(add + filters[order])[start:start+length]

		data_ret = []
		for e in data:
			datos = [
				#"<input type='radio' class='form-control input_check' name='col' value='" + str(e.pk) + "'>",
				""
			]

			for key in self.table_columns.keys():
				try:
					first, second = key.split("__")
					if second == "format":
						datos.append(self._getFormatRow(first, e))
					elif second == "date_format":
						value = str(date_format(getattr(e, first), format='SHORT_DATETIME_FORMAT', use_l10n=False)).replace("None", "")
						datos.append(value)
					else:
						value = str(getattr(getattr(e, first), second))
						datos.append(value)
				except:
					value = str(getattr(e, key))
					if os.path.isfile(settings.MEDIA_ROOT + "/" + value):
						direc = settings.MEDIA_URL + value
						datos.append("<a href='"+direc+"' download class='btn btn-xs btn-success'><i class='fa fa-download'></i> " + str(_("Descargar")) + "</a>")
					else:
						datos.append(value)

			buttons = ""
			if self.form_edit != None and self.can_edit and self.can_edit_row(e):
				buttons = buttons + "<a title='"+ str( _("Editar datos completos")) + "' class='btn btn-xs btn-warning clean_noti'"
				if self.edit_aparte == True:
					buttons = buttons + "href='" +self.form_action+ "?EditRegister&object_id="+str(e.pk)+"'"
				else:
					buttons = buttons + "onClick='editar_fila("+str(e.pk)+")'"
				buttons = buttons + "><i class='fa fa-edit'></i></a>"
			if self.can_view:
				if self.view_aparte:
					buttons = buttons + "<a title='"+ str(_("Ver registro")) + "' class='btn btn-xs btn-primary' href='"+str(self.form_action)+"view/"+str(e.pk)+"'><i class='fa fa-eye'></i></a>"
				else:
					buttons = buttons + "<a title='"+ str(_("Ver registro")) + "' class='btn btn-xs btn-primary' onClick='ver_fila("+str(e.pk)+")'><i class='fa fa-eye'></i></a>"
			if self.delete_register and self.can_delete and self.can_delete_row(e):
				buttons = buttons + "<button title='"+ str(_("Eliminar registro")) + "' class='btn btn-xs btn-danger clean_noti' onClick='eliminar_fila("+str(e.pk)+")'><i class='fa fa-trash'></i></button>"
			buttons += self.aditional_buttons.replace("__PK", str(e.pk))

			datos.append(buttons)

			data_ret.append(datos)
		return JsonResponse({'recordsTotal' : self.model_object.objects.filter(fil).count(), 'recordsFiltered' : self.model_object.objects.filter(fil).count(), 'data' : data_ret})

	def _get_title_form(self, obj):
		return ""

	def can_edit_row(self, obj):
		return True

	def can_delete_row(self, obj):
		return True

	def order_rows(self, key):
		return key



class MainBlog(Main):
	"""
		Main blog class
	"""
	template = "structure_blog.html"
	menu = []
	js_files = []
	css_files = []
	page_title = "blog"
	can_view_page = True

	def _essentialData(self, request):
		pass

	def __get_menu(self, path):
		menu = []
		all_pages = PaginaBlog.objects.all()
		for page in all_pages:
			menu.append(dict(label = _(page.nombre_enlace), url = "/blog/page/" + str(page.pk), item_class = ""))
		self.menu = menu

	def initialProccess(self, request, kwargs):
		"""
			Organices the initial data
		"""
		self.__get_menu(str(request.path))
		data_page = {}
		data_page = self.get_data(request, kwargs)
		data_page["menu"] = self.menu
		data_page["can_view_page"] = self.can_view_page
		data_page["page_title"] = self.page_title
		data_page["js_files"] = self.js_files
		data_page["css_files"] = self.css_files
		return data_page

def save_file(file, path_new):
	save_path = os.path.join(settings.MEDIA_ROOT, path_new, str(file))
	path = default_storage.save(save_path, file)
	return os.path.basename(path)
