# Own libraries
from app.views.main import Main, MainTable
from app.forms.documentos import *
from app.models import *

# Django libraries
from django.views import View
from django.db.models import Q


class DocumentosInstitucionalesView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Documentos institucionales"
	register_name = "documento institucional"
	form_action = "/documentos/institucionales/"
	model_object = DocInstitucionales
	table_columns =	dict(tema = "tema", titulo = "Título", archivo = "Archivo", observaciones="Observaciones")
	return_edit_columns = [["tema", "pk"], "titulo", "archivo", "observaciones"]
	form_edit = editDocumentoInstitucional
	form_add = editDocumentoInstitucional
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Documentos institucionales", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(titulo__icontains = value) | Q(tema__titulo__icontains=value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False



class PlanAreaView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	page_title = "Plan de área"
	register_name = "documento"
	form_action = "/documentos/plan_area/"
	model_object = PlanArea
	table_columns =	dict(docente = "docente", asignatura = "asignatura", observaciones = "Observaciones", archivo="Archivo", fecha_modificacion__date_format = "Fecha modificación")
	return_edit_columns = [["asignatura", "pk"], "archivo", "observaciones"]
	form_edit = editPlanArea
	form_add = editPlanArea
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planes de área", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(docente__nombre__icontains=value) | Q(docente__apellido__icontains=value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_estudiante():
			self.can_edit = False
			self.can_delete = False
			self.can_add = False
		self.can_edit = True
		self.can_delete = True
		self.can_add = True

	def can_delete_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False

	def can_edit_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False

class PlanActividadesView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Plan de actividades"
	register_name = "documento"
	form_action = "/documentos/plan_actividades/"
	model_object = PlanActividad
	table_columns =	dict(docente = "docente", asignatura = "Asignatura", periodo = "Periodo", archivo = "Archivo", fecha_modificacion__date_format = "Fecha mod.", observaciones="Observaciones")
	return_edit_columns = []
	form_edit = editPlanActividades
	form_add = editPlanActividades
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planes de actividades", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(titulo__icontains = value) | Q(tema__titulo__icontains=value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_estudiante():
			self.can_edit = False
			self.can_delete = False
			self.can_add = False
		self.can_edit = True
		self.can_delete = True
		self.can_add = True

	def can_delete_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False

	def can_edit_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False

class FormatosView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Formatos"
	register_name = "formato"
	form_action = "/documentos/formatos/"
	model_object = Formato
	table_columns =	dict(docente = "Docente sube", archivo = "Archivo", fecha_modificacion__date_format = "Fecha mod.", observaciones="Observaciones")
	return_edit_columns = []
	form_edit = editFormato
	form_add = editFormato
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Formatos", "")

		informacion_adicional = "<a href='/documentos/formatos/generar'> Click aquí para usar los formatos cargados previamente.</a>"

		return {"informacion_adicional" : informacion_adicional}

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_estudiante():
			self.can_edit = False
			self.can_delete = False
			self.can_add = False
		self.can_edit = True
		self.can_delete = True
		self.can_add = True

	def can_delete_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False

	def can_edit_row(self, e):
		if self.user.es_docente():
			if e.docente == self.user.docente():
				return True
		elif self.user.es_admin():
			return True
		return False
