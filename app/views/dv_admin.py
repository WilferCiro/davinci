# Own libraries
from app.views.main import Main, MainTable, save_file
from app.forms.dv_admin import *
from app.utils.funciones import *

# Django libraries
from django.views import View
from django.db.models import Q
from django.conf import settings
from django.shortcuts import HttpResponseRedirect, HttpResponse
from django.core.files.storage import default_storage
from openpyxl import Workbook, load_workbook

# System libraries
import xlwt
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import os
import itertools
import string

class excelData(Main, View):
	template = "dv_admin/excel.html"
	page_title = "Agregar copia de seguridad por excel"

	def _preProccess(self, request):
		self.can_view_page = False
		if self.user.es_admin():
			self.can_view_page = True

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Datos de excel", "")
		data_return = dict()

		data_return["form_upload"] = formCopy()

		save = request.GET.get("save", None)
		preview = request.GET.get("preview", None)
		file = request.GET.get("file", None)
		data_return["file"] = str(file)

		file_path = os.path.join(settings.MEDIA_ROOT, "excel_copy", str(file))
		if preview != None and default_storage.exists(file_path):
			wb = load_workbook(file_path)
			worksheet = wb.active
			row_index = 1
			if preview == "docentes":
				docentes_lista = []
				for row in worksheet.iter_rows():
					try:
						sede = Sede.objects.get(pk = worksheet["J" + str(row_index)].value)
					except:
						sede = None
					docente = Docente.objects.filter(copy_id = worksheet["A" + str(row_index)].value)
					if len(docente) == 0:
						docentes_lista.append({
							"id" : str(worksheet["A" + str(row_index)].value),
							"nombre" : str(worksheet["B" + str(row_index)].value),
							"apellido" : str(worksheet["C" + str(row_index)].value),
							"correo" : str(worksheet["D" + str(row_index)].value),
							"celular" : str(worksheet["E" + str(row_index)].value),
							"direccion" : str(worksheet["F" + str(row_index)].value),
							"profesion" : str(worksheet["G" + str(row_index)].value),
							"documento" : str(worksheet["H" + str(row_index)].value),
							"estado" : str(worksheet["I" + str(row_index)].value),
							"sede" : sede
						})
					row_index += 1
				data_return["docentes_lista"] = docentes_lista
				if(save != None):
					self.saveDocentes(docentes_lista)

			elif preview == "grupos":
				grupos_lista = []
				for row in worksheet.iter_rows():
					try:
						sede = Sede.objects.get(pk = worksheet["C" + str(row_index)].value)
					except:
						sede = None
					try:
						grado = Grado.objects.get(pk = worksheet["D" + str(row_index)].value)
					except:
						grado = None
					grupo = Grupo.objects.filter(copy_id = worksheet["A" + str(row_index)].value)
					if len(grupo) == 0:
						grupos_lista.append({
							"id" : str(worksheet["A" + str(row_index)].value),
							"nombre" : str(worksheet["B" + str(row_index)].value),
							"sede" : sede,
							"grado" : grado,
							"id_sede" : worksheet["C" + str(row_index)].value
						})
					row_index += 1
				data_return["grupos_lista"] = grupos_lista
				if(save != None):
					self.saveGrupos(grupos_lista)
					pass


			elif preview == "asignaturas":
				asignaturas_lista = []
				for row in worksheet.iter_rows():
					try:
						fundamental = AreaFundamental.objects.get(copy_id = worksheet["D" + str(row_index)].value)
					except:
						fundamental = None
					asignatura = Asignatura.objects.filter(Q(copy_id = worksheet["A" + str(row_index)].value) & Q(year = worksheet["H" + str(row_index)].value))
					if len(asignatura) == 0:
						if worksheet["A" + str(row_index)].value != None:
							asignaturas_lista.append({
								"id" : str(worksheet["A" + str(row_index)].value),
								"nombre" : str(worksheet["B" + str(row_index)].value),
								"abreviatura" : str(worksheet["C" + str(row_index)].value),
								"fundamental" : fundamental,
								"promedio" : self.replaceTrueFalse(str(worksheet["E" + str(row_index)].value)),
								"convivencia" : self.replaceTrueFalse(str(worksheet["F" + str(row_index)].value)),
								"porcentaje" : str(worksheet["G" + str(row_index)].value),
								"year" : worksheet["H" + str(row_index)].value,
							})
					row_index += 1
				data_return["asignaturas_lista"] = asignaturas_lista
				if(save != None):
					self.saveAsignaturas(asignaturas_lista)
					pass

			elif preview == "juicios":
				juicios_lista = []
				for row in worksheet.iter_rows():
					year = str(worksheet["F" + str(row_index)].value)
					try:
						asignatura = Asignatura.objects.get(Q(copy_id = worksheet["B" + str(row_index)].value) & Q(year = year))
					except:
						asignatura = None
					try:
						grado = Grado.objects.get(valor_numerico = int(worksheet["C" + str(row_index)].value) - 1)
					except:
						grado = None
					try:
						periodo = Periodo.objects.get(Q(year = year) & Q(orden = worksheet["D" + str(row_index)].value ) )
					except:
						periodo = None
					if asignatura != None:
						juicios = JuicioValorativo.objects.filter(Q(periodo = periodo) & Q(asignatura = asignatura) & Q(grado = grado))
						if len(juicios) <= 3:
							juicios_lista.append({
								"descripcion" : str(worksheet["A" + str(row_index)].value),
								"asignatura" : asignatura,
								"grado" : grado,
								"periodo" : periodo,
								"year" : year,
								"es_nee" : True if str(worksheet["E" + str(row_index)].value) == "especial" else False
							})
					row_index += 1
				data_return["juicios_lista"] = juicios_lista
				if(save != None):
					self.saveJuicios(juicios_lista)
					pass

			elif preview == "asignaciones":
				asignaciones_lista = []
				for row in worksheet.iter_rows():
					year = str(worksheet["F" + str(row_index)].value)
					try:
						docente = Docente.objects.get(copy_id = worksheet["B" + str(row_index)].value)
					except:
						docente = None
					try:
						asignatura = Asignatura.objects.get(Q(copy_id = worksheet["C" + str(row_index)].value) & Q(year = year))
					except:
						asignatura = None
					try:
						grupo = Grupo.objects.get(copy_id = worksheet["D" + str(row_index)].value)
					except:
						grupo = None
					if asignatura != None and docente != None and grupo != None:
						asignacion_existe = AsignacionDocente.objects.filter(Q(docente = docente) & Q(asignatura = asignatura) & Q(grupo = grupo))
						if len(asignacion_existe) == 0:
							asignaciones_lista.append({
								"docente" : docente,
								"asignatura" : asignatura,
								"grupo" : grupo,
								"year" : year,
								"h_s" : str(worksheet["E" + str(row_index)].value)
							})
					row_index += 1
				data_return["asignaciones_lista"] = asignaciones_lista
				if(save != None):
					self.saveAsignaciones(asignaciones_lista)
					pass

			elif preview == "estudiantes":
				estudiantes_lista = []
				headers = []
				for i in list(itertools.islice(self.excel_cols(), 380)):
					headers.append(str(worksheet[str(i) + str(row_index)].value))

				for row in worksheet.iter_rows():
					currentYear = "2016"
					if row_index > 1:
						data_estudiante = {
							"2016" : {},
							"2017" : {},
							"2018" : {},
							"2019" : {},
							"2020" : {}
						}

						col_index = 0
						for i in list(itertools.islice(self.excel_cols(), 380)):
							value = str(worksheet[str(i) + str(row_index)].value)
							if value in ["2016", "2017", "2018", "2019", "2020"]:
								currentYear = value
							else:
								data_estudiante[currentYear][headers[col_index]] = value
							col_index += 1

						estudiantes_lista.append(data_estudiante)
					row_index += 1

				data_return["estudiantes_lista"] = estudiantes_lista
				if save != None:
					self.saveEstudiantes(estudiantes_lista, headers)
					pass

			elif preview == "notas":
				notas_lista = []
				nro = 0
				min = int(request.GET.get("min", 0))
				max = int(request.GET.get("max", 10000))
				max_rows = int(worksheet.max_row)
				for row in worksheet.iter_rows():
					nro += 1
					if nro > min:
						if nro == max:
							break
						year = str(worksheet["T" + str(row_index)].value)
						try:
							asignatura = Asignatura.objects.get(Q(copy_id = int(worksheet["B" + str(row_index)].value)) & Q(year = year))
						except:
							asignatura = None
						try:
							estudiante = Estudiante.objects.get(Q(copy_id = int(worksheet["C" + str(row_index)].value)) & Q(year=year))
						except:
							estudiante = None
						try:
							periodo = Periodo.objects.get(Q(orden = worksheet["D" + str(row_index)].value) & Q(year = worksheet["T" + str(row_index)].value))
						except:
							periodo = None
						valor_nota =  worksheet["E" + str(row_index)].value.replace("*", "").replace(",", ".")
						inasistencias = worksheet["F" + str(row_index)].value
						inasistencias = int(str(inasistencias)) if inasistencias != None and str(inasistencias).isnumeric() else 0
						habilitacion = worksheet["O" + str(row_index)].value
						habilitacion = int(str(habilitacion.replace("*", ""))) if habilitacion != None and str(habilitacion).isnumeric() else 0.0
						if asignatura != None and periodo != None and estudiante != None:
							tabla_notas = get_tabla_notas(worksheet["T" + str(row_index)].value)
							notas_lista.append({
								"asignatura" : asignatura,
								"habilitacion" : habilitacion,
								"inasistencias" : inasistencias,
								"valor_nota" : valor_nota,
								"tabla_notas" : tabla_notas,
								"periodo" : periodo,
								"estudiante" : estudiante
							})
							#nota = estudiante.updateNota(asignatura, periodo, tabla_notas, valor_nota, inasistencias, habilitacion)

					row_index += 1
				data_return["notas_lista"] = notas_lista
				if(save != None):
					self.saveNotas(notas_lista)
					data_return["normal_response"] = False
					if max + 10000 > max_rows:
						data_return["return_response"] = HttpResponseRedirect('/dv_admin/add_data?successNotas')
					else:
						data_return["return_response"] = HttpResponseRedirect('/dv_admin/add_data?save=notas&preview=notas&file=' + str(file) + "&min=" + str(min + 10000) + "&max=" + str(max + 10000))

			elif preview == "acudientes":
				acudientes_lista = []
				for row in worksheet.iter_rows():
					year = str(worksheet["R" + str(row_index)].value)
					if worksheet["A" + str(row_index)].value != None:
						estudiante = Estudiante.objects.filter(acudiente_id = int(worksheet["A" + str(row_index)].value))
						if len(estudiante) > 0:
							acudientes_lista.append({
								"nombre" : worksheet["B" + str(row_index)].value,
								"identificacion" : worksheet["E" + str(row_index)].value,
								"expedido_acudiente" : worksheet["F" + str(row_index)].value,
								"direccion" : worksheet["H" + str(row_index)].value,
								"municipio_residencia" : worksheet["Q" + str(row_index)].value,
								"celular1" : worksheet["I" + str(row_index)].value,
								"celular2" : worksheet["J" + str(row_index)].value,
								"cabeza_familia" : worksheet["L" + str(row_index)].value,
								"parentesco" : worksheet["M" + str(row_index)].value,
								"estudiante" : estudiante[0],
								"year" : year
							})

					row_index += 1
				data_return["acudientes_lista"] = acudientes_lista
				if(save != None):
					self.saveAcudientes(acudientes_lista)
					pass

		return data_return

	def excel_cols(self):
		n = 1
		while True:
			yield from (''.join(group) for group in itertools.product(string.ascii_uppercase, repeat=n))
			n += 1

	def replaceTrueFalse(self, text):
		if text == "Yes" or text == "Si" or text == "si" or text == "yes":
			return True
		return False


	def saveAcudientes(self, data):
		for dat in data:
			existe = Acudiente.objects.filter(identificacion = dat["identificacion"])
			if len(existe) == 0:
				acudiente = Acudiente()
				acudiente.nombre = dat["nombre"]
				acudiente.parentesco = dat["parentesco"]
				acudiente.identificacion = dat["identificacion"]
				acudiente.expedido_en = dat["expedido_acudiente"]
				acudiente.direccion_residencia = dat["direccion"]
				acudiente.celular1 = dat["celular1"][0:25] if dat["celular1"] != None else None
				acudiente.celular2 = dat["celular2"][0:25] if dat["celular2"] != None else None
				acudiente.cabeza_familia = self.replaceTrueFalse(dat["cabeza_familia"])
				acudiente.year = dat["year"]
				acudiente.save()
			else:
				acudiente = existe[0]
			acudiente_estudiante = EstudianteAcudiente.objects.filter(Q(estudiante = dat["estudiante"]) & Q(acudiente=acudiente))
			if len(acudiente_estudiante) == 0:
				acudiente_estudiante = EstudianteAcudiente()
				acudiente_estudiante.estudiante = dat["estudiante"]
				acudiente_estudiante.acudiente = acudiente
				acudiente_estudiante.save()

	def saveDocentes(self, data):
		for dat in data:
			try:
				user_repeat = User.objects.get(username = dat["documento"])
			except:
				user = User.objects.create_user(username=dat["documento"], password=dat["documento"])
				new_docente = Docente(
					nombre = dat["nombre"],
					apellido = dat["apellido"],
					email = dat["correo"],
					celular = dat["celular"],
					documento = dat["documento"],
					direccion = dat["direccion"],
					estudios = dat["profesion"],
					sede = dat["sede"],
					estado = dat["estado"][0],
					copy_id = dat["id"]
				)
				new_docente.user = user
				new_docente.save()


	def saveGrupos(self, data):
		for dat in data:
			try:
				group_repeat = Grupo.objects.get(copy_id = dat["id"])
			except:
				new_grupo = Grupo(
					copy_id = dat["id"],
					titulo = dat["nombre"],
					sede = dat["sede"],
					grado = dat["grado"]
				)
				new_grupo.save()


	def saveAsignaturas(self, data):
		for dat in data:
			try:
				asignatura_repeat = Asignatura.objects.get(Q(copy_id = dat["id"]) & Q(year = dat["year"]))
			except:
				new_asignatura = Asignatura(
					copy_id = dat["id"],
					titulo = dat["nombre"],
					abreviatura = dat["abreviatura"],
					area_fundamental = dat["fundamental"],
					promedio = dat["promedio"],
					comportamiento = dat["convivencia"],
					porcentaje = dat["porcentaje"],
					year = dat["year"]
				)
				new_asignatura.save()


	def saveAsignaciones(self, data):
		for dat in data:
			new_asignacion = AsignacionDocente(
				asignatura = dat["asignatura"],
				docente = dat["docente"],
				grupo = dat["grupo"],
				hs = dat["h_s"],
				year = dat["year"]
			)
			new_asignacion.save()


	def saveJuicios(self, data):
		for dat in data:
			new_juicio = JuicioValorativo(
				asignatura = dat["asignatura"],
				grado = dat["grado"],
				periodo = dat["periodo"],
				descripcion = dat["descripcion"],
				es_nee = dat["es_nee"],
				year = dat["year"],
			)
			new_juicio.save()


	def getDate(self, date):
		if date != "0000-00-00" and str(date).count("-") == 2 and len(date) == 10:
			parts = date.split("-")
			if int(parts[0]) == 0 or int(parts[1]) == 0 or int(parts[2]) == 0:
				return None
			if int(parts[0]) > 1990 and int(parts[1]) > 12 and int(parts[2]) > 12:
				return None
			if int(parts[2]) > 1990:
				if int(parts[1]) > 12:
					return parts[2] + "-" + parts[0] + "-" + parts[1]
				else:
					return parts[2] + "-" + parts[1] + "-" + parts[0]
			elif int(parts[1]) > 12:
				return parts[0] + "-" + parts[2] + "-" + parts[1]
			else:
				return parts[0] + "-" + parts[1] + "-" + parts[2]
		return None


	def saveEstudiantes(self, dataEstudiantes, headers):
		index = 0
		for user in User.objects.all():
			if user.es_estudiante():
				user.delete()
		for data in dataEstudiantes:
			user_estudiante = None
			for year in ["2016", "2017", "2018", "2019", "2020"]:
				documento = None
				if "documento_estudiante" in data[year]:
					documento = data[year]["documento_estudiante"]

				if documento != None and len(documento) > 0:
					if user_estudiante == None:
						try:
							user_estudiante = User.objects.create_user(username=documento, password=documento)
						except:
							user_estudiante = None

					if data[year] != {} and "id_acudiente" in data[year] and documento != None and documento != "None" and data[year]["grupo"] != None and len(data[year]["grupo"]) > 0 and user_estudiante != None and "id_estudiante" in data[year]:

						new_object = Estudiante(documento = documento)
						new_object.user = user_estudiante
						new_object.year = year
						new_object.save()
						salud = EstudianteSalud(estudiante = new_object)
						adicional = EstudianteAdicional(estudiante = new_object)
						salud.save()
						adicional.save()


						new_object.copy_id = data[year]["id_estudiante"]
						if data[year]["id_acudiente"] != None and data[year]["id_acudiente"] != "None" and data[year]["id_acudiente"] != "":
							new_object.acudiente_id = data[year]["id_acudiente"]
						new_object.estado = data[year]["estado"][0].upper()
						new_object.fecha_retiro = self.getDate(data[year]["fecha_retiro"])
						new_object.observaciones = data[year]["correo"].replace("None", "")
						new_object.nombre = data[year]["nombre_estudiante"]
						new_object.apellido = data[year]["apellido_estudiante"]
						id_grupo = int(data[year]["grupo"])
						grupo_obj = Grupo.objects.filter(copy_id = id_grupo)
						if len(grupo_obj) > 0:
							new_object.grupo = grupo_obj[0]
						new_object.tipo_documento = data[year]["tipo_documento_estudiante"].upper().replace("TARJETA IDENTIDAD", "TI").replace("CEDULA CIUDADANIA", "CC").replace("PASAPORTE", "PA").replace("REGISTRO CIVIL", "RC").replace("NUIP", "NU").replace("NES", "NE").replace("CEDULA EXTRANGERIA", "CE").replace("CONTRASEÑA", "CO")
						new_object.documento = data[year]["documento_estudiante"]
						new_object.municipio_expedicion = data[year]["municipio_expedicion"]
						new_object.departamento_expedicion = data[year]["departamento_expedicion"]
						new_object.sexo = data[year]["sexo"][0]
						new_object.observaciones = data[year]["correo"]
						new_object.fecha_nacimiento = self.getDate(data[year]["nacimiento"])
						new_object.municipio_nacimiento = data[year]["municipio_nacimiento"]
						new_object.departamento_nacimiento = data[year]["departamento_nacimiento"]
						new_object.direccion_residencia = data[year]["direccion_residencia"]
						new_object.municipio_residencia = data[year]["municipio_residencia"]
						new_object.departamento_residencia = data[year]["departamento_residencia"]
						new_object.zona = data[year]["zona_vivienda"][0]
						new_object.celular1 = data[year]["celular"][0:25]
						new_object.celular2 = data[year]["celular"][0:25]
						new_object.email = data[year]["correo"].replace("None", "")
						salud.eps = data[year]["eps"].replace("None", "")
						salud.tipo_sangre = data[year]["tipo_sangre"][0:1].replace("P", "+").replace("N", "-")
						salud.puntaje_sisben = data[year]["sisben"].replace("None", "")
						salud.ars = data[year]["ars"].replace("None", "")
						salud.enfermedades = data[year]["enfermedades"].replace("None", "")
						salud.alergias = data[year]["alergias"].replace("None", "")
						salud.medicamentos = data[year]["medicamentos"].replace("None", "")
						limitaciones = data[year]["limitaciones"].split(",")
						for lim in limitaciones:
							lim_obj = Limitacion.objects.filter(nombre = lim)
							if len(lim_obj) > 0:
								created = LimitacionEstudiante(estudiante = new_object, limitacion = lim_obj[0])
								created.save()
						salud.discapacidades = data[year]["discapacidad"]

						adicional.desplazado = True if "desplazado" in data[year]["informacion_adicional"] else False
						adicional.familias_accion = True if "familias_accion" in data[year]["informacion_adicional"] else False
						adicional.victima_conflicto = True if "desplazado" in data[year]["informacion_adicional"] else False
						adicional.alumna_madre_cabeza = True if "alumna_madre_cabeza" in data[year]["informacion_adicional"] else False
						adicional.beneficiario_fuerza_publica = True if "beneficiario_fuerza_publica" in data[year]["informacion_adicional"] else False
						adicional.beneficiario_heroe_nacion = True if "beneficiario_heroe_nacion" in data[year]["informacion_adicional"] else False
						adicional.lugar_desplazamiento = data[year]["lugar_desplazamiento"].replace("None", "")
						adicional.etnia_indigena = data[year]["etnia_indigena"].replace("None", "")
						adicional.tipo_vivienda = data[year]["tipo_vivienda"].replace("None", "")
						adicional.estrato_socioeconomico = data[year]["estrato"][0:4].replace("None", "")
						adicional.vive_con = data[year]["vive_con"].replace("None", "")
						adicional.personas_hogar = data[year]["personas_hogar"] if data[year]["personas_hogar"] != "" and data[year]["personas_hogar"].isnumeric() else 0
						adicional.hermanos_ie = data[year]["hermanos_ie"] if data[year]["hermanos_ie"] != "" and data[year]["hermanos_ie"].isnumeric() else 0
						adicional.ie_proviene = data[year]["ie_proviene"].replace("None", "")
						adicional.tel_familiar_fuera = data[year]["tel_familiar_fuera"].replace("None", "")
						adicional.lugar_proviene = data[year]["municipio_proviene"].replace("None", "")
						adicional.dificultades_aprendizaje = True if data[year]["difAprendizaje"] == "1" else False
						adicional.sabe_nadar = True if data[year]["sabeNadar"] == "1" else False

						new_object.save()
						salud.save()
						adicional.save()

	def saveNotas(self, data):
		for da in data:
			nota = da["estudiante"].updateNota(da["asignatura"], da["periodo"], da["tabla_notas"], da["valor_nota"], da["inasistencias"], da["habilitacion"])


	def _addData(self, request):
		estudiantes = request.FILES['estudiantes'] if 'estudiantes' in request.FILES else None
		docentes = request.FILES['docentes'] if 'docentes' in request.FILES else None
		asignaturas = request.FILES['asignaturas'] if 'asignaturas' in request.FILES else None
		juicios = request.FILES['juicios'] if 'juicios' in request.FILES else None
		asignaciones = request.FILES['asignaciones'] if 'asignaciones' in request.FILES else None
		notas = request.FILES['notas'] if 'notas' in request.FILES else None
		grupos = request.FILES['grupos'] if 'grupos' in request.FILES else None
		acudientes = request.FILES['acudientes'] if 'acudientes' in request.FILES else None
		file=None
		if estudiantes != None:
			file = save_file(estudiantes, "excel_copy")
			tipo = "estudiantes"
		elif docentes != None:
			file = save_file(docentes, "excel_copy")
			tipo = "docentes"
		elif asignaturas != None:
			file = save_file(asignaturas, "excel_copy")
			tipo = "asignaturas"
		elif juicios != None:
			file = save_file(juicios, "excel_copy")
			tipo = "juicios"
		elif asignaciones != None:
			file = save_file(asignaciones, "excel_copy")
			tipo = "asignaciones"
		elif notas != None:
			file = save_file(notas, "excel_copy")
			tipo = "notas"
		elif grupos != None:
			file = save_file(grupos, "excel_copy")
			tipo = "grupos"
		elif acudientes != None:
			file = save_file(acudientes, "excel_copy")
			tipo = "acudientes"
		if file != None:
			if tipo == "notas":
				return HttpResponseRedirect('/dv_admin/add_data?save=notas&preview=' + str(tipo) + '&file=' + str(file))
			else:
				return HttpResponseRedirect('/dv_admin/add_data?preview=' + str(tipo) + '&file=' + str(file))
		else:
			return HttpResponseRedirect('/dv_admin/add_data')
