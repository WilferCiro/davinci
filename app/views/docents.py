# Own libraries
from app.views.main import Main, MainTable, save_file
from app.models import *
from app.forms.docents import *
from app.forms.various import *
from app.utils.pdf import show_pdf, getImpresionFormato

# Django libraries
from django.views import View
from django.db.models import Q
from django.conf import settings
from django.shortcuts import HttpResponseRedirect, HttpResponse
from django.core.files.storage import default_storage
from openpyxl import Workbook, load_workbook
from django.utils.translation import ugettext_lazy as _

# Anothers libraries
import xlwt
import os

class Docentes(MainTable, View):
	"""
		view of Docentes page
	"""
	page_title = "Docentes"
	register_name = "docente"
	form_action = "/docentes/"
	model_object = Docente
	table_columns = dict(nombre = "Nombres", apellido = "Apellidos", sede = "Sede", documento ="Documento", sexo__format = "Sexo", celular = "Celular", estado__format = "Estado")
	return_edit_columns = ["nombre", "apellido", ["sede", "pk"], "documento", "sexo", "celular", "estado", "estudios", "direccion"]
	form_edit = editDocente
	form_add = editDocente
	mostrar_control_activos = True
	delete_register = False
	view_aparte = True
	can_view = True

	def _getFormatRow(self, column, obj):
		if column == "estado":
			return obj.getEstado()
		elif column == "sexo":
			return obj.getSexo()
		return ""

	def _addData(self, request):
		if request.user.es_admin():
			form_edit = self.form_edit(request.POST)
			if form_edit.is_valid():
				documento = form_edit.cleaned_data['documento']
				try:
					user_repeat = User.objects.get(username = documento)
				except:
					user = User.objects.create_user(username=documento, password=documento)
					new_docente = form_edit.save(commit=False)
					new_docente.user = user
					new_docente.save()
					form_edit.save_m2m()
					return True
		return False

	def _deleteData(self, request):
		object_id = request.POST.get("object_id", None)
		if object_id != None and request.user.es_admin():
			sed = self.model_object.objects.get(pk = object_id)
			sed.user.delete()
			sed.delete()
			return True

		return False

	def _preProccess(self, request):
		if not request.user.es_admin():
			self.table_columns = dict(nombre = "Nombres", apellido = "Apellidos", sede = "Sede", estudios = "Estudios", estado__format = "Estado")
			self.can_add = False
			self.can_delete = False
			self.can_edit = False
			self.can_view = False
		elif self.user.es_admin():
			self.can_add = True
			self.can_delete = True
			self.can_edit = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Docentes", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(nombre__icontains = value) | Q(apellido__icontains = value) | Q(sede__titulo__icontains=value) | Q(sexo__icontains = value) | Q(email__icontains = value) | Q(documento__icontains = value))
			return fil
		return Q()


class DocenteProfile(Main, View):
	template = "docente/profile.html"
	page_title = "Perfil de docente"

	def get_data(self, request, kwargs):
		try:
			user_id = int(kwargs['pk'])
			docente = Docente.objects.get(Q(pk = user_id))
		except:
			if self.user.es_docente():
				docente = self.user.docente()
			else:
				self.can_view_page = False
				return {}

		propio_perfil = False
		if self.user.es_estudiante() or (self.user.es_docente() and self.user != docente.user):
			self.can_view_page = False
			return {}
		else:
			propio_perfil = True

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Perfil de docente", "")

		data_return = {}

		docente_data = {
			"pk" : docente.pk,
			"nombre" : docente.nombre,
			"apellido" : docente.apellido,
			"username" : docente.user.username,
			"documento" : docente.documento,
			"email" : docente.email,
			"telefono" : docente.telefono,
			"celular" : docente.celular,
			"direccion" : docente.direccion,
			"estudios" : docente.estudios,
			"sede" : docente.sede,
			"genero" : docente.getSexo(),
			"creation_date" : docente.user.date_joined,
			"last_login" : docente.user.last_login,
			"foto" : docente.foto
		}

		date_min = datetime.datetime(self.year, 1, 1)
		date_max = datetime.datetime(self.year, 12, 31)
		all_inasistencias = InasistenciaDocente.objects.filter(Q(docente = docente) & Q(fecha__lte = date_max) & Q(fecha__gte = date_min)).order_by("fecha")
		inas = []
		nro = 1
		for ina in all_inasistencias:
			inas.append({
				"nro" : nro,
				"fecha" : ina.fecha,
				"docente" : ina.docente.get_full_name(),
				"horas" : ina.horas,
				"motivo" : ina.motivo
			})
			nro += 1

		data_return["all_inasistencias"] = inas
		if len(inas) == 0:
			data_return["empty_inasistencias"] = True

		asignaciones = AsignacionDocente.objects.filter(Q(docente = docente) & Q(year = self.year))
		all_asignaciones = []
		for asi in asignaciones:
			all_asignaciones.append({
				"grupo" : asi.grupo.getGrupo(),
				"asignatura" : asi.asignatura.titulo,
				"horas" : asi.hs
			})

		data_return["asignaciones"] = all_asignaciones

		if self.user.es_admin() or self.user.docente() == docente:
			data_return["form_edit_avatar"] = editDocenteAvatar(instance = docente)
			data_return["form_action"] = "/docentes/view/" + str(user_id)
			data_return["form_edit_pass"] = changePass()
			data_return["form_edit_data"] = editDocente(instance = docente)

		data_return["docente"] = docente_data
		return data_return

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)

		if tipo_edit == "datos_completos" and object_id != None:
			try:
				docente = Docente.objects.get(pk = object_id)
				if self.user.es_admin:
					form = editDocente(data = request.POST, files = request.FILES, instance = docente)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				self._add_error("Error al editar datos")
				pass

		elif tipo_edit == "user_avatar" and object_id != None:
			try:
				docente = Docente.objects.get(pk = object_id)
				if self.user.es_admin:
					form = editDocenteAvatar(data = request.POST, files = request.FILES, instance = docente)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		elif tipo_edit == "password" and object_id != None:
			try:
				docente = Docente.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and (self.user.es_admin or self.user == docente):
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						docente.user.set_password(pass1)
						docente.user.save()
						return True
					else:
						self._add_error("Las contraseñas no coinciden")
				else:
					self._add_error("No se puede editar la contraseña")
			except Exception as ex:
				print(ex)

		return False



class JuiciosValorativos(MainTable, View):
	"""
		view of Grados page
	"""
	#TODO: agregar posibilidad a docentes para agregar datos
	page_title = "Juicios valorativos"
	register_name = "juicio valorativo"
	form_action = "/docentes/JuiciosValorativos/"
	model_object = JuicioValorativo
	table_columns =	dict(descripcion = "Descripción", periodo = "Periodo", asignatura = "Asignatura", grado = "Grado", estado__format = "Estado")
	return_edit_columns = ["descripcion", ["periodo", "pk"], ["asignatura", "pk"], ["grado", "pk"], "estado"]
	form_edit = editJuicioValorativo
	form_add = editJuicioValorativo
	mostrar_control_activos = True
	delete_register = True
	docente_add_delete = True

	form_filter = True

	def _getFormatRow(self, column, obj):
		if column == "estado":
			return obj.getEstado()
		return ""

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Juicios Valorativos", "")

	def _getFilerTable(self, value):
		fil = Q(year = self.year)
		if self.user.es_docente():
			fil = self.user.docente().filtroGradoAsignatura(self.year)
		if self.user.es_estudiante():
			fil = Q(grado = self.user.estudiante().grupo.grado)
		if value != "":
			fil = fil & (Q(descripcion__icontains = value) | Q(periodo__titulo__icontains = value) | Q(asignatura__titulo__icontains=value) | Q(grado__titulo__icontains = value) | Q(estado__icontains = value))
		return fil

	def _preProccess(self, request):
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class AsignacionDocenteView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Asignación Docentes"
	register_name = "asignación docente"
	form_action = "/docentes/asignacion/"
	model_object = AsignacionDocente
	table_columns =	dict(asignatura = "Asignatura", docente = "Docente", grupo = "Grupo", hs = "H E")
	return_edit_columns = [["asignatura", "pk"], ["docente", "pk"], ["grupo", "pk"], "hs"]
	form_edit = editAsignacion
	form_add = editAsignacion
	delete_register = True
	docente_add_delete = True
	form_filter = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Asignación Docente", "")

	def _getFilerTable(self, value):
		fil = Q(year = self.year)
		if self.user.es_docente():
			fil = Q(docente = self.user.docente())
		elif self.user.es_estudiante():
			fil = Q(grupo = self.user.estudiante().grupo)

		if value != "":
			fil = fil & (Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(hs__icontains = value))
		return fil

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class PrintJuiciosValorativos(Main, View):
	template = "form.html"
	page_title = "Imprimir juicios valorativos"

	def get_data(self, request, kwargs):

		forms = []
		forms.append(dict(href="first", tab="Filtros y más", form = ImprimirJuicios(year = self.year, user = self.user)))
		forms.append(dict(href="three", tab="Impresión", form=ImpresionFormato()))
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/index")
		self._add_breadcrumb("Imprimir juicios valorativos", "")

		ayuda = "Si no selecciona ningúna opción, se imprimirán todos los juicios completos"

		# Data to return
		data_return = dict()
		data_return["ayuda"] = ayuda
		data_return["forms"] = forms
		data_return["target"] = "target='_blank'"
		data_return["tipo_form"] = "informeForm"
		data_return["form_action"] = "/docentes/print/JuiciosValorativos/"

		return data_return

	def _proccessInforme(self, request):
		imprimir_form = ImprimirJuicios(request.POST)
		if imprimir_form.is_valid():
			data_return = {}
			# Cabecera y demás
			Datos = ConfiguracionInformes.objects.get(pk=1)
			DatosIns = ConfiguracionInstitucion.objects.get(pk = 1)
			cabecera = Datos.cabecera_informe_comun
			escudo = settings.MEDIA_URL + str(DatosIns.escudo)

			asignatura = imprimir_form.cleaned_data["asignatura"]
			grado = imprimir_form.cleaned_data["grado"]
			periodo = imprimir_form.cleaned_data["periodo"]
			formato = imprimir_form.cleaned_data["formato"]
			solo_activos = imprimir_form.cleaned_data["solo_activos"]
			order = imprimir_form.cleaned_data["order"]

			filtro = Q()
			if asignatura != None:
				filtro = filtro & Q(asignatura = asignatura)
			if grado != None:
				filtro = filtro & Q(grado = grado)
			if periodo != None:
				filtro = filtro & Q(periodo = periodo)

			if solo_activos:
				filtro = filtro & Q(estado = JuicioValorativo.E_ACTIVO)

			juicios = JuicioValorativo.objects.filter(filtro).order_by(order)

			juicios_all = []
			for jui in juicios:
				juicios_all.append({
					"pk" : jui.pk,
					"descripcion" : jui.descripcion,
					"periodo" : jui.periodo.titulo,
					"asignatura" : jui.asignatura.titulo,
					"grado" : jui.grado.titulo
				})
			template_path = 'pdf/juicios.html'

			data_return["juicios_all"] = juicios_all
			data_return = getImpresionFormato(request.POST, data_return, self.year)

			return show_pdf(data_return, template_path, request.build_absolute_uri())


class AsignacionGuiada(Main, View):
	template = "docente/asignacion_guiada.html"
	page_title = "Asignación Guiada"

	def get_data(self, request, kwargs):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/index")
		self._add_breadcrumb("Asignación guiada", "")

		self._add_js("selects.js")

		# Data to return
		data_return = dict()

		data_return["form"] = formGuiada()

		grupo_pk = request.GET.get("grupo", None)
		if grupo_pk != None:
			try:
				grupo = Grupo.objects.get(pk = grupo_pk)
				asignaciones_all = []
				data_return["form"]["grupo"].initial = grupo
				nro = 1
				for asignacion in AsignacionDocente.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
					asignaciones_all.append({
						"nro" : nro,
						"docente" : asignacion.docente.get_full_name(),#asignacionGuiadaEspecifica(instance = asignacion),
						"grupo" : asignacion.grupo,
						"asignatura" : asignacion.asignatura,
						"hs" : asignacion.hs,
						"pk" : asignacion.pk
					})
					nro += 1

				data_return["view_data"] = True
				data_return["grupo"] = {
					"pk" : grupo.pk,
					"titulo" : grupo.getGrupo()
				}
				data_return["asignaciones_all"] = asignaciones_all
			except Exception as e:
				print(e)
		return data_return

	def _editData(self, request):
		grupo_pk = request.POST.get("grupo", None)
		success = False
		if grupo_pk != None:
			grupo = Grupo.objects.filter(pk = grupo_pk)
			if len(grupo) == 1:
				for asignacion in AsignacionDocente.objects.filter(Q(grupo = grupo[0]) & Q(year = self.year)):
					docente_pk = request.POST.get("docente" + str(asignacion.pk), None)
					hs = int(request.POST.get("hs" + str(asignacion.pk), 0))
					forSave = False
					if hs != 0:
						asignacion.hs = hs
						forSave = True
					if docente_pk != None and docente_pk != "":
						docente = Docente.objects.filter(pk = docente_pk)
						if len(docente) == 1:
							asignacion.docente = docente[0]
							forSave = True
						else:
							self._add_error("Docente de " + str(asignacion) + " no válido")
					if forSave:
						asignacion.save()
				success = True
			else:
				self._add_error("Grupo no válido")
		return HttpResponseRedirect("/docentes/asignacion_guiada?grupo=" + str(grupo_pk) + "&view=&success=" + str(success))


class JuiciosExcel(Main, View):
	template = "docente/juicios_excel.html"
	page_title = "Agregar juicios por excel"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Agregar juicios por excel", "")
		data_return = dict()
		preview = request.GET.get("preview", None)
		juicios_lista = []
		file_exists = False
		save = request.GET.get("save", None)
		mensaje = request.GET.get("mensaje", None)

		if self.user.es_estudiante():
			self.can_view_page = False
			return {}
		else:
			form_file = FormJuicioExcel()
			data_return["form_file"] = form_file

			file = request.GET.get("file", None)
			file_path = os.path.join(settings.MEDIA_ROOT, "excel_juicio", str(file))
			if preview != None and default_storage.exists(file_path):
				preview = True
				file_exists = True
				errores = []

				wb = load_workbook(file_path)
				worksheet = wb.active
				row_index = 1
				for row in worksheet.iter_rows():
					if row_index > 1:
						row_data = {}
						row_data["descripcion"] = worksheet["A" + str(row_index)].value
						periodo_nro = worksheet["B" + str(row_index)].value
						asignatura_title = worksheet["C" + str(row_index)].value
						grado_nro = worksheet["D" + str(row_index)].value
						periodo = Periodo.objects.filter(Q(orden = periodo_nro) & Q(year = self.year))
						asignatura = Asignatura.objects.filter(Q(titulo = asignatura_title)  & Q(year = self.year))
						grado = Grado.objects.filter(Q(valor_numerico = grado_nro))
						error = True
						if len(periodo) > 0 and len(asignatura) > 0 and len(grado) > 0:
							row_data["periodo"] = periodo[0]
							row_data["asignatura"] = asignatura[0]
							row_data["grado"] = grado[0]
							error = False
						if not error:
							juicios_lista.append(row_data)
						else:
							juicios_lista.append("error")
					row_index += 1

		if save != None:
			for row in juicios_lista:
				if row != "error":
					nuevo_juicio = JuicioValorativo()
					nuevo_juicio.periodo = row["periodo"]
					nuevo_juicio.grado = row["grado"]
					nuevo_juicio.asignatura = row["asignatura"]
					nuevo_juicio.descripcion = row["descripcion"]
					nuevo_juicio.year = self.year
					nuevo_juicio.save()

			data_return["normal_response"] = False
			data_return["return_response"] = HttpResponseRedirect("/docentes/JuiciosExcel?mensaje=Datos agregados con éxito")

		data_return["juicios_lista"] = juicios_lista
		data_return["file_exists"] = file_exists
		data_return["file_name"] = file
		data_return["preview"] = preview
		data_return["mensaje"] = mensaje
		return data_return


	def _addData(self, request):
		archivo = request.FILES['archivo']
		file = save_file(archivo, "excel_juicio")
		return HttpResponseRedirect('/docentes/JuiciosExcel?preview&file=' + str(file))


class InasistenciasView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Inasistencia docentes")
	register_name = _("inasistencia")
	form_action = "/docentes/inasistencias"
	model_object = InasistenciaDocente
	table_columns =	dict(horas = "Horas", docente = "Docente", motivo = "Motivo", fecha = "Fecha")
	return_edit_columns = ["fecha", "horas", ["docente", "pk"], "motivo"]
	form_edit = editInasistenciaDocente
	form_add = editInasistenciaDocente
	delete_register = True
	docente_add_delete = True

	edit_aparte = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Inasistencias docentes"), "")
		self._add_aditional_template("docente/imprimir_inasistencias.html")
		return_data = {
			"form_print" : formPrintInasistencias()
		}
		return return_data

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(motivo__icontains = value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha__icontains=value) | Q(horas__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self._can_view_page = False


class AsignacionesExcel(Main, View):
	template = "docente/asignaciones_excel.html"
	page_title = "Agregar asignaciones por excel"

	def _preProccess(self, request):
		self.can_view_page = False
		if self.user.es_admin():
			self.can_view_page = True

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Agregar asignaciones por excel", "")
		data_return = dict()
		preview = request.GET.get("preview", None)
		asignaciones_lista = []
		file_exists = False
		save = request.GET.get("save", None)
		mensaje = request.GET.get("mensaje", None)

		if self.user.es_estudiante():
			self.can_view_page = False
			return {}
		else:
			form_file = FormJuicioExcel()
			data_return["form_file"] = form_file

			file = request.GET.get("file", None)
			file_path = os.path.join(settings.MEDIA_ROOT, "excel_asignaciones", str(file))
			if preview != None and default_storage.exists(file_path):
				preview = True
				file_exists = True
				errores = []

				wb = load_workbook(file_path)
				worksheet = wb.active
				row_index = 1
				for row in worksheet.iter_rows():
					if row_index > 1:
						row_data = {}
						cedula_docente = worksheet["A" + str(row_index)].value
						asignatura_title = worksheet["B" + str(row_index)].value
						grupo_title = worksheet["C" + str(row_index)].value.split(" - ")
						if len(grupo_title) > 0:
							sede_title = grupo_title[-1]
							grupo_title = grupo_title[:-1][0]

						docente = Docente.objects.filter(Q(documento = cedula_docente))
						asignatura = Asignatura.objects.filter(Q(titulo = asignatura_title) & Q(year = self.year))
						grupo = Grupo.objects.filter(Q(titulo = grupo_title) & Q(sede__titulo = sede_title))

						row_data["hs"] = worksheet["D" + str(row_index)].value

						error = True
						if len(docente) > 0 and len(asignatura) > 0 and len(grupo) > 0:
							row_data["docente"] = docente[0]
							row_data["asignatura"] = asignatura[0]
							row_data["grupo"] = grupo[0]
							error = False
						if not error:
							asignaciones_lista.append(row_data)
						else:
							asignaciones_lista.append("error")
							print(docente, asignatura, grupo, grupo_title, sede_title)
					row_index += 1

		if save != None:
			for row in asignaciones_lista:
				if row != "error":
					asignacion = AsignacionDocente()
					asignacion.grupo = row["grupo"]
					asignacion.docente = row["docente"]
					asignacion.asignatura = row["asignatura"]
					asignacion.hs = row["hs"]
					asignacion.year = self.year
					asignacion.save()

			data_return["normal_response"] = False
			data_return["return_response"] = HttpResponseRedirect("/docentes/AsignacionesExcel?mensaje=Datos agregados con éxito")

		data_return["asignaciones_lista"] = asignaciones_lista
		data_return["file_exists"] = file_exists
		data_return["file_name"] = file
		data_return["preview"] = preview
		data_return["mensaje"] = mensaje
		return data_return


	def _addData(self, request):
		archivo = request.FILES['archivo']
		file = save_file(archivo, "excel_asignaciones")
		return HttpResponseRedirect('/docentes/AsignacionesExcel?preview&file=' + str(file))
