# Own libraries
from app.views.main import Main, MainTable
from app.forms.academic import *
from app.models import *

# Django libraries
from django.views import View
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class AnuncioActividades(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Anuncio de actividades"
	register_name = "anuncio de actividad"
	form_action = "/academico/anuncio_actividades/"
	model_object = AnuncioActividades
	table_columns =	dict(grupo = "grupo", docente = "Docente", asignatura = "Asignatura", titulo = "Título", fecha_realizacion__date_format = "Fecha de realización", archivo = "Archivo", observaciones = "Observaciones")
	return_edit_columns = [["asignatura", "pk"], ["docente", "pk"], ["grupo", "pk"], "titulo", "fecha_realizacion", "observaciones"]
	form_edit = editAnuncioActividades
	form_add = editAnuncioActividades
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	form_filter = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Anuncio de actividades", "")

	def _getFilerTable(self, value):
		fil = Q()
		if self.user.es_docente():
			fil = Q(docente = self.user.docente())
		elif self.user.es_estudiante():
			fil = Q(grupo = self.user.estudiante().grupo)

		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha_realizacion__icontains = value) | Q(titulo__icontains = value))
		return fil

	def _preProccess(self, request):
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True



class MaterialRecuperacionView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Material de recuperación"
	register_name = "material de recuperación"
	form_action = "/academico/material_recuperacion/"
	model_object = MaterialRecuperacion
	table_columns =	dict(grupo = "grupo", docente = "Docente", asignatura = "Asignatura", periodo="Periodo", titulo = "Título", fecha_ingreso__date_format = "Fecha de ingreso", archivo = "Archivo", observaciones = "Observaciones")
	return_edit_columns = [["asignatura", "pk"], ["docente", "pk"], ["grupo", "pk"], ["periodo", ["pk"]], "titulo", "fecha_realizacion", "observaciones"]
	form_edit = editMaterialRecuperacion
	form_add = editMaterialRecuperacion
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	form_filter = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Material de recuperación", "")

	def _getFilerTable(self, value):
		fil = Q()
		if self.user.es_docente():
			fil = Q(docente = self.user.docente())
		elif self.user.es_estudiante():
			fil = Q(grupo = self.user.estudiante().grupo)

		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha_realizacion__icontains = value) | Q(titulo__icontains = value))
		return fil

	def _preProccess(self, request):
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class MaterialApoyoView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Material de apoyo"
	register_name = "material de apoyo"
	form_action = "/academico/material_apoyo/"
	model_object = MaterialApoyo
	table_columns =	dict(grupo = "grupo", docente = "Docente", asignatura = "Asignatura", periodo="Periodo", titulo = "Título", fecha_ingreso__date_format = "Fecha de ingreso", archivo = "Archivo", observaciones = "Observaciones")
	return_edit_columns = [["asignatura", "pk"], ["docente", "pk"], ["grupo", "pk"], ["periodo", ["pk"]], "titulo", "fecha_realizacion", "observaciones"]
	form_edit = editMaterialApoyo
	form_add = editMaterialApoyo
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	form_filter = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Material de apoyo", "")

	def _getFilerTable(self, value):
		fil = Q()
		if self.user.es_docente():
			fil = Q(docente = self.user.docente())
		elif self.user.es_estudiante():
			fil = Q(grupo = self.user.estudiante().grupo)

		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha_realizacion__icontains = value) | Q(titulo__icontains = value))
		return fil
	def _preProccess(self, request):
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True

class BibliotecaVirtualView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Modificar Formulario para mostrar sólo los filtros de docentes
	page_title = "Biblioteca Virtual"
	register_name = "biblioteca virtual"
	form_action = "/academico/biblioteca_virtual/"
	model_object = BiliotecaVirtual
	table_columns =	dict(titulo = "Título", url = "Url", asignatura="asignatura", observaciones="Observaciones")
	return_edit_columns = [["asignatura", "pk"], "titulo", "url", "observaciones"]
	form_edit = editBibliotecaVirtual
	form_add = editBibliotecaVirtual
	delete_register = True
	docente_add_delete = True

	form_filter = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Biblioteca virtual", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(titulo__icontains = value) | Q(asignatura__titulo__icontains=value) | Q(url__icontains = value))
		return fil

	def _preProccess(self, request):
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True

class ContactoAcudientesView(Main, View):
	template = "form.html"
	page_title = "Contacto acudientes - estudiantes"

	def get_data(self, request, kwargs):

		forms = []
		form_comun = ContactoAcudientesForm()

		forms.append(dict(completa="si", href="first", tab="Formulario de correo", form = form_comun))

		ayuda = "Agrega los estudiantes que desees, dependiendo de la cantidad, este proceso puede demorar, <br /> se le enviará el mensaje a los estudiantes del grupo seleccionado y a los estudiantes seleccionados."

		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Cont. acudientes", "")
		return dict(forms = forms, ayuda=ayuda, tipo_form = "sendData", action_form="/academico/contacto_acudientes/")

	def _sendData(self, request):
		if request.user.es_admin():
			form = ContactoAcudientesForm(request.POST)
			#if form.is_valid():
			from django.core.mail import send_mail
			asunto = request.POST.get("asunto")
			mensaje = request.POST.get("mensaje")
			estudiantes = request.POST.getlist("estudiantes")
			grupo = request.POST.get("grupo", None)
			correos = []
			if grupo != None and grupo != "":
				for est in Estudiante.objects.filter(grupo = grupo):
					if est.email != "":
						correos.append(est.email)
			for e in estudiantes:
				new_e = Estudiante.objects.get(pk = e)
				if new_e.email != "":
					if new_e.email not in correos:
						correos.append(new_e.email)
			send_mail(
				asunto,
				'',
				'wilcirom@gmail.com',
				correos,
				fail_silently=False,
				html_message = mensaje
			)
			return True
		return False

	def _preProccess(self, request):
		user = request.user
		if user.es_estudiante():
			self._can_view_page = False
		else:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class EscuelaPadresView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Escuela padres")
	register_name = _("Escuela padres")
	form_action = "/academico/escuela_padres/"
	model_object = EscuelaPadres
	table_columns =	dict(titulo = "Título", imagen = "Imagen", fecha_publicacion__date_format = "Fecha de Publicación")
	return_edit_columns = ["cuerpo", "titulo", "imagen"]
	form_edit = editEscuelaPadres
	form_add = editEscuelaPadres
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Escuela padres"), "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha_realizacion__icontains = value) | Q(titulo__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self._can_view_page = False
