# Own libraries
from app.models import *

# Django libraries
from django.views import View
from django.db.models import Q
from django.http import JsonResponse

from app.serializer.adicional import *
from rest_framework.views import APIView
from rest_framework.response import Response

from random import randint


class EstudianteAjax(View):
	def get(self, request, *args, **kargs):
		term = request.GET.get("term", "")
		estudiantes = Estudiante.objects.filter((Q(nombre__icontains = term) | Q(apellido__icontains = term) | Q(grupo__titulo__icontains = term) | Q(grupo__sede__titulo__icontains = term)))[0:10]
		in_data = []
		for ci in estudiantes:
			in_data.append({"text" : ci.nombre + " " + ci.apellido + " - " + str(ci.grupo), "id" : ci.pk})
		data = {
			'results': in_data
		}
		return JsonResponse(data)

class DocentesAjax(View):
	def get(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			term = request.GET.get("term", "")
			docentes = Docente.objects.filter((Q(nombre__icontains = term) | Q(apellido__icontains = term)) & Q(estado = Docente.E_ACTIVO))[0:10]
			in_data = []
			for ci in docentes:
				in_data.append({"text" : ci.nombre + " " + ci.apellido, "id" : ci.pk, "selected": "true"})
			data = {
				'results': in_data,
			}
		return JsonResponse(data)


class IcfesViewSet(APIView):
	"""
	API endpoint that allows users to be viewed or edited.
	"""
	def get(self, request, format=None):
		questions = request.GET.get("questions","").split(",")
		areas = request.GET.get("areas","").split(",")
		fil_area = Q()
		for ar in areas:
			if ar != "":
				fil_area = fil_area | Q(area = ar)
		fil_question = Q()
		for qu in questions:
			if qu != "":
				fil_question = fil_question & ~Q(pk = int(qu))
		fil = fil_area & fil_question
		queryset = Icfes.objects.filter(fil)
		if len(queryset) > 0:
			nro = randint(1, len(queryset))
		else:
			nro = 1
		serializer = UserSerializer(queryset[nro - 1 : nro], many=True)
		return Response(serializer.data)

	def post(self, request, format=None):
		questions = ['']
		responses = ['']
		if "questions" in request.data:
			questions = request.data["questions"].split(",")
		if "responses" in request.data:
			responses = request.data["responses"].split(",")

		validas = 0
		erroneas = 0
		if len(responses) == len(questions):
			for i in range(len(responses)):
				if questions[i] != "" and responses[i] != "":
					icfes = Icfes.objects.filter(pk = int(questions[i]))
					if len(icfes) == 1:
						if responses[i] == icfes[0].correcta:
							validas += 1
						else:
							erroneas += 1

		content = {'correctas': validas, 'incorrectas' : erroneas, 'total' : validas + erroneas}
		return Response(content)
