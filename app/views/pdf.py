# Own libraries
from app.views.main import Main, MainTable
from app.views.adicional import Dane
from app.models import *
from app.utils.pdf import show_pdf, getImpresionFormato
from app.utils.funciones import replace_data_students
from app.utils.funciones import *
from app.utils.macros import *

# Django libraries
from django.http import JsonResponse
from django.views import View
from django.db.models import Q
from django.shortcuts import HttpResponseRedirect
from django.conf import settings

# System libraries
import datetime

class HojaMatricula(Main, View):
	template = "pdf/default.html"
	page_title = "Generar informes"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		try:
			empty = request.GET.get("vacia", None)
			if empty == None:
				complete_group = request.GET.get("grupo_completo", None)
				if complete_group != None:
					group_id = int(kwargs['pk'])
					year = int(kwargs["year"])
					student = Estudiante.objects.filter(Q(grupo__pk = group_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
				else:
					user_id = int(kwargs['pk'])
					year = int(kwargs["year"])
					student = Estudiante.objects.filter(Q(pk = user_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
		except:
			self.can_view_page = False
			return {}

		# Data to return
		data_return = dict()

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return["all_pages"] = []

		if empty != None:
			page1 = replace_data_students(Datos.hoja_matricula_1, None)
			page2 = replace_data_students(Datos.hoja_matricula_2, None, replace_line = True)
			data_return["all_pages"].extend([page1, page2])
		else:
			for st in student:
				page1 = replace_data_students(Datos.hoja_matricula_1, st)
				page2 = replace_data_students(Datos.hoja_matricula_2, st)
				data_return["all_pages"].extend([page1, page2])

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "legal"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"


		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class Boletin(Main, View):
	template = "pdf/boletin.html"
	page_title = "Generar boletin"

	def get_data(self, request, kwargs):
		try:
			individual = request.GET.get("individual", None)
			year = int(kwargs["year"])
			periodo_pk = int(kwargs["periodo"])
			periodo = Periodo.objects.get(pk = periodo_pk)
			if individual == None:
				group_id = int(kwargs['pk'])
				student = Estudiante.objects.filter(Q(grupo__pk = group_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
			else:
				user_id = int(kwargs['pk'])
				student = Estudiante.objects.filter(Q(pk = user_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		# Data to return
		data_return = dict()

		data_return["all_pages"] = []

		periodos_all = []
		for per in Periodo.objects.filter(Q(year = year) & (Q(orden__lte = periodo.orden) | Q(es_final = True))).order_by("es_final", "orden"):
			if per.isActive():
				if per.es_final:
					periodos_all.append({
						"pk" : per.pk,
						"titulo" : "Acu"
					})
				else:
					periodos_all.append({
						"pk" : per.pk,
						"titulo" : per.titulo
					})

		data_return["periodos"] = periodos_all

		grupo = student[0].grupo
		indicadores_return = []
		for asignacion in AsignacionDocente.objects.filter(Q(year = year) & Q(grupo = grupo)):
			indicadores_internal = []
			for indica in JuicioValorativo.objects.filter(Q(year = year) & Q(asignatura = asignacion.asignatura) & Q(grado = grupo.grado) & Q(periodo = periodo))[0:3]:
				indicadores_internal.append(indica.descripcion)
			indicadores_return.append({
				"pk" : asignacion.asignatura.pk,
				"titulo" : asignacion.asignatura.titulo,
				"indicadores" : indicadores_internal
			})

		for st in student:
			puesto = 0
			notas = st.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
			promedio = next((comp for comp in notas["promedios"] if comp["pk"] == periodo.pk), False)
			promedio_show = 0
			if promedio != False:
				promedio_show = promedio["valor"]

			perdidas = [comp for comp in notas["areas"] if comp["perdida"] == True]
			perdidas2 = [comp for comp in notas["asignaturas"] if comp["perdida"] == True]
			perdidas = len(perdidas) + len(perdidas2)

			datos_boletin = {
				"pk" : str(st.pk),
				"estudiante" : str(st.apellido) + " " + str(st.nombre),
				"identificacion" : str(st.documento),
				"grupo" : str(st.grupo),
				"director_grupo" : str(st.grupo.getDirector(year)),
				"promedio" : promedio_show,
				"puesto" : puesto,
				"perdidas" : perdidas,
				"pierde_anio" : True if periodo.es_final and perdidas >= self.perdidas_pei else False,
				"es_final" : periodo.es_final,
				"foto_estudiante" : st.foto
			}
			page = {
				"datos_boletin" : datos_boletin,
				"notas" : notas
			}

			data_return["all_pages"].append(page)

		if individual != None:
			promedios_estudiantes = []
			for new_student in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = year)):
				new_notas = new_student.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
				promedio = next((comp for comp in new_notas["promedios"] if comp["pk"] == periodo.pk), False)
				promedio_show = 0
				if promedio != False:
					promedio_show = promedio["valor"]
				promedios_estudiantes.append({
					"pk" : new_student.pk,
					"promedio" : promedio_show
				})
			promedios_estudiantes = sorted(promedios_estudiantes, key = lambda i: i['promedio'], reverse = True)
			nro_puesto = 0
			counter = 0
			for prom in promedios_estudiantes:
				if counter < 1 or (counter >= 1 and prom["promedio"] != promedios_estudiantes[counter - 1]["promedio"]):
					nro_puesto += 1
				if str(prom["pk"]) == str(data_return["all_pages"][0]["datos_boletin"]["pk"]):
					data_return["all_pages"][0]["datos_boletin"]["puesto"] = nro_puesto
				counter += 1
		else:
			promedios_estudiantes = sorted(data_return["all_pages"], key = lambda i: i["datos_boletin"]['promedio'], reverse = True)
			nro_puesto = 0
			counter = 0
			for prom in promedios_estudiantes:
				print(nro_puesto, counter<1, counter, str(prom["datos_boletin"]["promedio"]) != str(promedios_estudiantes[counter - 1]["datos_boletin"]["promedio"]), str(prom["datos_boletin"]["promedio"]), str(promedios_estudiantes[counter - 1]["datos_boletin"]["promedio"]))
				if counter < 1 or (counter >= 1 and str(prom["datos_boletin"]["promedio"]) != str(promedios_estudiantes[counter - 1]["datos_boletin"]["promedio"])):
					nro_puesto += 1
				new_student = next((comp for comp in data_return["all_pages"] if comp["datos_boletin"]["pk"] == prom["datos_boletin"]["pk"]), False)
				if new_student != False:
					new_student["datos_boletin"]["puesto"] = nro_puesto

				counter += 1

		data_return["indicadores"] = indicadores_return

		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["initial_periodo"] = {
			"pk" : periodo.pk,
			"titulo" : periodo.titulo
		}
		DatosIns = ConfiguracionInstitucion.objects.filter(year = year)[0]
		data_return["nombre_rector"] = DatosIns.nombre_rector
		data_return["identificacion_rector"] = DatosIns.identificacion_rector

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#show_pdf(data_return, self.template, request.build_absolute_uri())

		return data_return


class Consolidado(Main, View):
	template = "pdf/consolidado.html"
	page_title = "Generar boletin"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False


	def getNotasData(self, year, periodo, grupo, all_estudiantes, filters = None):
		if filters == None:
			filters = {
				"promedio_min" : MIN_NOTA,
				"promedio_max" : MAX_NOTA,
				"maximo_perdidas" : 100,
				"simular_notas" : False
			}
		areas_objects = [a for a in AreaFundamental.objects.all()]
		areas_objects.append(None)
		asignaturas_all = []
		areas_all = []
		for area in areas_objects:
			asignaciones = AsignacionDocente.objects.filter(Q(year = year) & Q(grupo = grupo) & Q(asignatura__area_fundamental = area)).order_by("asignatura__pk")
			asignaturas_internal = []
			if len(asignaciones) > 0:
				for asignacion in asignaciones:
					if area != None:
						asignaturas_internal.append({
							"pk" : asignacion.asignatura.pk,
							"abreviatura" : asignacion.asignatura.abreviatura,
							"total_perdidas" : 0
						})
					else:
						asignaturas_all.append({
							"pk" : asignacion.asignatura.pk,
							"abreviatura" : asignacion.asignatura.abreviatura,
							"total_perdidas" : 0
						})
				if area != None:
					areas_all.append({
						"pk" : area.pk,
						"abreviatura" : area.abreviatura,
						"asignaturas" : asignaturas_internal,
						"total_perdidas" : 0
					})
		estudiantes_all = []
		total_estudiantes = len(all_estudiantes)
		for est in all_estudiantes:
			notas_est = est.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost, sim_nota = filters["simular_notas"])
			promedio_return = 0
			perdidas_return = 0
			promedio_periodo = next((comp for comp in notas_est["promedios"] if comp["pk"] == periodo.pk), False)
			if promedio_periodo != False:
				promedio_return = promedio_periodo["valor"]
			perdidas_periodo = next((comp for comp in notas_est["perdidas"] if comp["pk"] == periodo.pk), False)
			if perdidas_periodo != False:
				perdidas_return = perdidas_periodo["valor"]
			if filters["promedio_min"] <= promedio_return and filters["promedio_max"] >= promedio_return and perdidas_return <= filters["maximo_perdidas"]:
				estudiantes_all.append({
					"pk" : est.pk,
					"nombre" : str(est.apellido) + " " + str(est.nombre),
					"notas" : notas_est,
					"perdidas" : perdidas_return,
					"promedio" : promedio_return
				})
				for area in areas_all:
					new_area = next((comp for comp in notas_est["areas"] if comp["pk"] == area["pk"]), False)
					if new_area != False:
						new_periodo = next((comp for comp in new_area["periodos"] if comp["pk"] == periodo.pk), False)
						if new_periodo != False:
							if new_periodo["valor"] < self.min_lost:
								area["total_perdidas"] += 1
						for asignatura in area["asignaturas"]:
							new_asignatura = next((comp for comp in new_area["asignaturas"] if comp["pk"] == asignatura["pk"]), False)
							if new_asignatura != False:
								new_periodo = next((comp for comp in new_asignatura["periodos"] if comp["pk"] == periodo.pk), False)
								if new_periodo != False:
									if new_periodo["valor"] < self.min_lost and new_periodo["habilitacion"] < self.min_lost:
										asignatura["total_perdidas"] += 1
				for asignatura in asignaturas_all:
					new_asignatura = next((comp for comp in notas_est["asignaturas"] if comp["pk"] == asignatura["pk"]), False)
					if new_asignatura != False:
						new_periodo = next((comp for comp in new_asignatura["periodos"] if comp["pk"] == periodo.pk), False)
						if new_periodo != False:
							if new_periodo["valor"] < self.min_lost and new_periodo["habilitacion"] < self.min_lost:
								asignatura["total_perdidas"] += 1

		for asigna in asignaturas_all:
			asigna["porcentaje_perdidas"] = int((100 * asigna["total_perdidas"]) / total_estudiantes)
		for area in areas_all:
			area["porcentaje_perdidas"] = int((100 * area["total_perdidas"]) / total_estudiantes)
			for asigna in area["asignaturas"]:
				asigna["porcentaje_perdidas"] = int((100 * asigna["total_perdidas"]) / total_estudiantes)

		return {
			"estudiantes" : estudiantes_all,
			"areas" : areas_all,
			"asignaturas" : asignaturas_all
		}

	def get_data(self, request, kwargs):
		try:
			year = int(kwargs["year"])
			periodo_pk = int(kwargs["periodo"])
			periodo = Periodo.objects.get(pk = periodo_pk)
			areas_pendientes = request.GET.get("areas_pendientes", None)
			grupo_id = int(kwargs['pk'])
			grupo = Grupo.objects.get(pk = grupo_id)
			all_estudiantes = Estudiante.objects.filter(Q(grupo = grupo) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return = {
			"periodo_pk" : periodo.pk,
			"periodo_titulo" : periodo.titulo
		}
		data_return.update(self.getNotasData(year, periodo, grupo, all_estudiantes))
		if areas_pendientes != None:
			data_return["areas_pendientes"] = True

		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#show_pdf(data_return, self.template, request.build_absolute_uri())

		return data_return


class ConstanciaEstudio(Main, View):
	template = "pdf/default.html"
	page_title = "Constancia de estudio"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		try:
			year = int(kwargs["year"])
			estudiante_id = int(kwargs["pk"])
			motivo = request.GET.get("motivo", "")
			familias_accion = request.GET.get("familias_accion", None)
			estudiante = Estudiante.objects.get(Q(year = year) & Q(pk = estudiante_id))
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		if familias_accion != None:
			page1 = replace_data_students(Datos.constancia_familias_accion, estudiante)
		else:
			page1 = replace_data_students(Datos.constancia_estudio, estudiante)
		page1 = replace_informe_data(page1, str(year))
		if familias_accion == None:
			page1 = page1.replace("{{motivo_constancia}}", str(motivo))

		data_return = {}
		data_return["all_pages"] = [page1]

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "A4"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class MejoresPromediosSedes(Main, View):
	template = "pdf/tablas_promedios.html"
	page_title = "Mejores promedios por grupos"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		try:
			year = int(kwargs["year"])
			periodo_id = int(kwargs["periodo"])
			periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = year))
			all_sedes = Sede.objects.all()
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		data_return = {}
		data_return["tablas"] = []

		for sede in all_sedes:
			all_estudiantes = Estudiante.objects.filter(Q(year = year) & Q(grupo__sede = sede) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
			estudiantes_return = []
			for est in all_estudiantes:
				notas_est = est.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
				promedio_return = 0
				promedio_periodo = next((comp for comp in notas_est["promedios"] if comp["pk"] == periodo.pk), False)
				if promedio_periodo != False:
					promedio_return = promedio_periodo["valor"]
				estudiantes_return.append({
					"pk" : est.pk,
					"grupo" : str(est.grupo.getGrupo()),
					"nombre" : str(est.apellido) + " " + str(est.nombre),
					"promedio" : promedio_return
				})
			estudiantes_return = sorted(estudiantes_return, key = lambda i: i['promedio'], reverse = True)
			counter = 0
			nro_puesto = 0
			for prom in estudiantes_return:
				if counter < 1 or (counter >= 1 and prom["promedio"] != estudiantes_return[counter - 1]["promedio"]):
					nro_puesto += 1
				prom["puesto"] = nro_puesto
				if nro_puesto == 20:
					break
				counter += 1
			data_return["tablas"].append({
				"estudiantes" : estudiantes_return,
				"add_grupo" : True,
				"leyenda" : "Mejores promedios de la sede " + str(sede.titulo) + " en el periodo " + str(periodo.titulo)
			})

		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return

class MejoresPromediosGrupos(Main, View):
	template = "pdf/tablas_promedios.html"
	page_title = "Mejores promedios por grupos"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		try:
			year = int(kwargs["year"])
			periodo_id = int(kwargs["periodo"])
			periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = year))
			sede_id = int(kwargs["pk"])
			sede = Sede.objects.get(Q(pk = sede_id))
			all_grupos = Grupo.objects.filter(Q(sede = sede))
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		data_return = {}
		data_return["tablas"] = []

		for grupo in all_grupos:
			all_estudiantes = Estudiante.objects.filter(Q(year = year) & Q(grupo = grupo) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
			estudiantes_return = []
			for est in all_estudiantes:
				notas_est = est.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
				promedio_return = 0
				promedio_periodo = next((comp for comp in notas_est["promedios"] if comp["pk"] == periodo.pk), False)
				if promedio_periodo != False:
					promedio_return = promedio_periodo["valor"]
				estudiantes_return.append({
					"pk" : est.pk,
					"nombre" : str(est.apellido) + " " + str(est.nombre),
					"promedio" : promedio_return
				})
			estudiantes_return = sorted(estudiantes_return, key = lambda i: i['promedio'], reverse = True)
			counter = 0
			nro_puesto = 0
			for prom in estudiantes_return:
				if counter < 1 or (counter >= 1 and prom["promedio"] != estudiantes_return[counter - 1]["promedio"]):
					nro_puesto += 1
				prom["puesto"] = nro_puesto
				if nro_puesto == 5:
					break
				counter += 1
			data_return["tablas"].append({
				"estudiantes" : estudiantes_return,
				"leyenda" : "Mejores promedios del grupo " + str(grupo.getGrupo()) + " en el periodo " + str(periodo.titulo)
			})


		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return

class ListadoPromedios(Main, View):
	template = "pdf/tablas_promedios.html"
	page_title = "Listado ordenado por promedios"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		try:
			year = int(kwargs["year"])
			periodo_id = int(kwargs["periodo"])
			periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = year))
			grupo_id = int(kwargs["pk"])
			grupo = Grupo.objects.get(Q(pk = grupo_id))
			all_estudiantes = Estudiante.objects.filter(Q(year = year) & Q(grupo = grupo) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
		except Exception as e:
			print(e)
			self.can_view_page = False
			return {}

		data_return = {}

		estudiantes_return = []
		for est in all_estudiantes:
			notas_est = est.getNotasActivas(year, get_tabla_notas(year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
			promedio_return = 0
			promedio_periodo = next((comp for comp in notas_est["promedios"] if comp["pk"] == periodo.pk), False)
			if promedio_periodo != False:
				promedio_return = promedio_periodo["valor"]
			estudiantes_return.append({
				"pk" : est.pk,
				"nombre" : str(est.apellido) + " " + str(est.nombre),
				"promedio" : promedio_return
			})
		estudiantes_return = sorted(estudiantes_return, key = lambda i: i['promedio'], reverse = True)
		counter = 0
		nro_puesto = 0
		for prom in estudiantes_return:
			if counter < 1 or (counter >= 1 and prom["promedio"] != estudiantes_return[counter - 1]["promedio"]):
				nro_puesto += 1
			prom["puesto"] = nro_puesto
			counter += 1

		data_return["tablas"] = [{
			"estudiantes" : estudiantes_return,
			"leyenda" : "Lista ordenada por promedios en el periodo " +str(periodo.titulo)+ " del grupo " + str(grupo.getGrupo()),
		}]

		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class DanePDF(Main, View):
	template = "pdf/dane.html"
	page_title = "Registro Dane"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return = {}

		data_return["sedes_all"] = Dane.getSedes(self)

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "A4"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 0.5cm 1cm 0.5cm"

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class AnecdotarioView(Main, View):
	template = "pdf/anecdotario.html"
	page_title = "Anecdotario del estudiante"

	def get_data(self, request, kwargs):
		try:
			complete_group = request.GET.get("grupo_completo", None)
			if complete_group != None:
				group_id = int(kwargs['pk'])
				year = int(kwargs["year"])
				student = Estudiante.objects.filter(Q(grupo__pk = group_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
			else:
				user_id = int(kwargs['pk'])
				year = int(kwargs["year"])
				student = Estudiante.objects.filter(Q(pk = user_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
		except:
			self.can_view_page = False
			return {}

		# Data to return
		data_return = dict()

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return["all_pages"] = []

		for st in student:
			periodos = []
			notas = st.getNotasActivas(year, get_tabla_notas(year), min_lost = self.min_lost)
			for periodo in Periodo.objects.filter(Q(year = year)).order_by("orden"):
				if periodo.isActive():
					perdidas = []
					excelentes = []
					other_asignaturas = []
					[other_asignaturas.extend(area["asignaturas"]) for area in notas["areas"]]
					for asignatura in notas["asignaturas"] + other_asignaturas:
						periodo_asignatura = next((comp for comp in asignatura["periodos"] if comp["pk"] == periodo.pk), False)
						if periodo_asignatura != None:
							if periodo_asignatura["perdida"]:
								perdidas.append(asignatura["titulo"] + " (" + str(periodo_asignatura["valor"]) + ")")
							else:
								if periodo_asignatura["valor"] >= 4.5:
									excelentes.append(asignatura["titulo"] + " (" + str(periodo_asignatura["valor"]) + ")")
					periodos.append({
						"titulo" : periodo.titulo,
						"perdidas" : "<br />".join(perdidas),
						"excelentes" : "<br />".join(excelentes)
					})
			anecdotario = Anecdotario.objects.filter(Q(estudiante = st))
			anotaciones_positivas = ""
			anotaciones_negativas = ""
			if len(anecdotario) > 0:
				anotaciones_positivas = anecdotario[0].anotaciones_positivas
				anotaciones_negativas = anecdotario[0].anotaciones_negativas


			all_datos = [st for st in Estudiante.objects.filter(Q(user = st.user) & ~Q(year = year))]
			areas_pendientes = []
			for st_otro in all_datos:
				periodo_final = Periodo.objects.filter(Q(es_final = True) & Q(year = st_otro.year))
				if len(periodo_final) > 0:
					periodo_final = periodo_final[0]
					notas = st_otro.getNotasActivas(st_otro.year, get_tabla_notas(st_otro.year), periodo_hasta = periodo_final.orden, min_lost = self.min_lost)
					other_asignaturas = []
					[other_asignaturas.extend(area["asignaturas"]) for area in notas["areas"]]
					for asignatura in notas["asignaturas"] + other_asignaturas:
						periodo_asignatura = next((comp for comp in asignatura["periodos"] if comp["pk"] == periodo_final.pk), False)
						if periodo_asignatura != None:
							if periodo_asignatura["perdida"]:
								areas_pendientes.append(str(st_otro.year) + " - " + asignatura["titulo"] + " (" + str(periodo_asignatura["valor"]) + ")")


			areas_pendientes = "<br />".join(areas_pendientes)

			datos_estudiante = {
				"pk" : str(st.pk),
				"estudiante" : str(st.apellido) + " " + str(st.nombre),
				"identificacion" : str(st.documento),
				"tipo_identificacion" : str(st.getTipoDocumento()),
				"sexo" : str(st.getSexo()),
				"grupo" : str(st.grupo),
				"periodos" : periodos,
				"anio" : year,
				"foto_estudiante" : st.foto,
				"anotaciones_negativas" : anotaciones_negativas,
				"anotaciones_positivas" : anotaciones_positivas,
				"areas_pendientes" : areas_pendientes
			}
			data_return["all_pages"].append(datos_estudiante)

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "legal"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"


		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class CertificadoNotas(Main, View):
	template = "pdf/certificado_notas.html"
	page_title = "Certificado de notas"

	def get_data(self, request, kwargs):
		try:
			user_id = int(kwargs['pk'])
			year = int(kwargs["year"])
			student = Estudiante.objects.filter(Q(pk = user_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
			student = student[0]
		except:
			self.can_view_page = False
			return {}

		# Data to return
		data_return = dict()

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		certificado = Datos.certificado_notas.split("{{tablas_notas_estudiante}}")
		primera_parte = replace_data_students(certificado[0], student)
		primera_parte = replace_informe_data(primera_parte, str(year))
		segunda_parte = ""
		if type(certificado) == list and len(certificado) > 1:
			segunda_parte = replace_data_students(certificado[1], student)
			segunda_parte = replace_informe_data(segunda_parte, str(year))

		all_datos = [st for st in Estudiante.objects.filter(Q(user = student.user))]
		all_years = []
		for st_otro in all_datos:
			periodos_return = []
			periodos_all = Periodo.objects.filter(Q(year = st_otro.year)).order_by("orden")
			notas = st_otro.getNotasActivas(st_otro.year, get_tabla_notas(st_otro.year), min_lost = self.min_lost)
			for periodo in periodos_all:
				periodos_return.append({
					"pk" : periodo.pk,
					"titulo" : periodo.titulo
				})
			all_years.append({
				"year" : st_otro.year,
				"grupo" : str(st_otro.grupo),
				"notas" : notas,
				"periodos" : periodos_return
			})

		data_return["all_years"] = all_years

		data_return["primera_parte"] = primera_parte
		data_return["segunda_parte"] = segunda_parte

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "legal"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return

class AsistenciaDiaria(Main, View):
	template = "pdf/asistencia.html"
	page_title = "Registro de asistencia"

	def get_data(self, request, kwargs):
		try:
			complete_group = request.GET.get("grupo_completo", None)
			all_grupos = request.GET.get("all_grupos", None)
			if complete_group != None:
				group_id = int(kwargs['pk'])
				year = int(kwargs["year"])
				all_estudiantes = Estudiante.objects.filter(Q(grupo__pk = group_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
			else:
				user_id = int(kwargs['pk'])
				year = int(kwargs["year"])
				all_estudiantes = Estudiante.objects.filter(Q(pk = user_id) & Q(year = year) & Q(estado = Estudiante.E_ACTIVO))
		except:
			self.can_view_page = False
			return {}


		# Data to return
		data_return = dict()
		individual = True

		pages = []
		if all_grupos != None:
			individual = False
			for periodo in Periodo.objects.filter(Q(year = year) & Q(es_final = False)):
				data = []
				for grupo in Grupo.objects.all():
					nro_retardos = Inasistencia.objects.filter(Q(estudiante__grupo = grupo) & Q(horas = 1) & Q(fecha__range = [periodo.fecha_inicio, periodo.fecha_final])).count()
					nro_inas = Inasistencia.objects.filter(Q(estudiante__grupo = grupo) & Q(horas__lte = 2) & Q(fecha__range = [periodo.fecha_inicio, periodo.fecha_final])).count()
					data.append({
						"grupo" : grupo.getGrupo(),
						"inasistencia" : nro_inas,
						"retardos" : nro_retardos
					})
				pages.append({
					"titulo" : "Registros en " + str(periodo.titulo),
					"data" : data
				})
		else:
			for estudiante in all_estudiantes:
				data = []
				nro_retardos = 0
				nro_inas = 0
				nro = 1
				for periodo in Periodo.objects.filter(Q(year = year) & Q(es_final = False)):
					print(periodo.fecha_inicio, periodo.fecha_final)
					for ina in Inasistencia.objects.filter(Q(estudiante = estudiante) & Q(fecha__range = [periodo.fecha_inicio, periodo.fecha_final])):
						excusa = "Sin Excusa"
						if ina.excusa:
							excusa = "Con excusa"
						concepto = "Inasistencia"
						if ina.horas == 1:
							concepto = "Retardo"
							nro_retardos += 1
						else:
							nro_inas += 1
						data.append({
							"nro" : nro,
							"fecha" : ina.fecha,
							"excusa" : excusa,
							"horas" : ina.horas,
							"concepto" : concepto
						})
						nro += 1
				if len(data) > 0 or complete_group == None:
					pages.append({
						"titulo" : "Registros del estudiante: " + str(estudiante),
						"data" : data,
						"total_inasistencia" : nro_inas,
						"total_retardos" : nro_retardos
					})
		data_return["all_pages"] = pages
		data_return["individual"] = individual

		data_return = getImpresionFormato(request.GET, data_return, year)

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class AsistenciaDocentes(Main, View):
	template = "pdf/asistencia_docentes.html"
	page_title = "Registro de asistencia docentes"

	def get_data(self, request, kwargs):
		try:
			individual = request.GET.get("individual", None)

			if individual != None:
				docente_pk = request.GET.get("docente", None)
				year = int(request.GET.get("anio", 1))
				date_min = datetime.datetime(year, 1, 1)
				date_max = datetime.datetime(year, 12, 31)
				docente = Docente.objects.get(pk = docente_pk)
				all_inasistencias = InasistenciaDocente.objects.filter(Q(docente = docente) & Q(fecha__lte = date_max) & Q(fecha__gte = date_min)).order_by("fecha")
				titulo = "Inasistencias del docente " + str(docente.get_full_name()) + " en las fechas " + str(date_min.strftime("%b %d %Y")) + " hasta " + str(date_max.strftime("%b %d %Y"))
			else:
				mes = int(request.GET.get("mes", 1))
				year = int(request.GET.get("anio", 1))
				date_min = datetime.datetime(year, mes, 1)
				date_max = datetime.datetime(year, mes, 31)

				all_inasistencias = InasistenciaDocente.objects.filter(Q(fecha__lte = date_max) & Q(fecha__gte = date_min)).order_by("fecha")
				titulo = "Inasistencias de los docentes en las fechas " + str(date_min.strftime("%b %d %Y")) + " hasta " + str(date_max.strftime("%b %d %Y"))
		except Exception as e:
			self.can_view_page = False
			return {}

		data_return = {}
		inas = []
		nro = 1
		for ina in all_inasistencias:
			inas.append({
				"nro" : nro,
				"fecha" : ina.fecha,
				"docente" : ina.docente.get_full_name(),
				"horas" : ina.horas,
				"motivo" : ina.motivo
			})
			nro += 1

		if len(all_inasistencias) == 0:
			data_return["vacio"] = True
		data_return["all_inasistencias"] = inas
		data_return["titulo"] = titulo

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "A4"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return


class ListadoEstudiantes(Main, View):
	template = "pdf/asistencia_docentes.html"
	page_title = "Registro de asistencia docentes"

	def get_data(self, request, kwargs):
		try:
			individual = request.GET.get("individual", None)

			if individual != None:
				docente_pk = request.GET.get("docente", None)
				year = int(request.GET.get("anio", 1))
				date_min = datetime.datetime(year, 1, 1)
				date_max = datetime.datetime(year, 12, 31)
				docente = Docente.objects.get(pk = docente_pk)
				all_inasistencias = InasistenciaDocente.objects.filter(Q(docente = docente) & Q(fecha__lte = date_max) & Q(fecha__gte = date_min)).order_by("fecha")
				titulo = "Inasistencias del docente " + str(docente.get_full_name()) + " en las fechas " + str(date_min.strftime("%b %d %Y")) + " hasta " + str(date_max.strftime("%b %d %Y"))
			else:
				mes = int(request.GET.get("mes", 1))
				year = int(request.GET.get("anio", 1))
				date_min = datetime.datetime(year, mes, 1)
				date_max = datetime.datetime(year, mes, 31)

				all_inasistencias = InasistenciaDocente.objects.filter(Q(fecha__lte = date_max) & Q(fecha__gte = date_min)).order_by("fecha")
				titulo = "Inasistencias de los docentes en las fechas " + str(date_min.strftime("%b %d %Y")) + " hasta " + str(date_max.strftime("%b %d %Y"))
		except Exception as e:
			self.can_view_page = False
			return {}

		data_return = {}
		inas = []
		nro = 1
		for ina in all_inasistencias:
			inas.append({
				"nro" : nro,
				"fecha" : ina.fecha,
				"docente" : ina.docente.get_full_name(),
				"horas" : ina.horas,
				"motivo" : ina.motivo
			})
			nro += 1

		if len(all_inasistencias) == 0:
			data_return["vacio"] = True
		data_return["all_inasistencias"] = inas
		data_return["titulo"] = titulo

		Datos = ConfiguracionInformes.objects.last()
		DatosIns = ConfiguracionInstitucion.objects.last()
		cabecera = Datos.cabecera_informe_comun
		escudo = settings.MEDIA_URL + str(DatosIns.escudo)

		data_return["cabecera"] = cabecera
		data_return["escudo"] = escudo

		data_return["size_file"] = "A4"
		data_return["orientacion"] = "portrait"
		data_return["margins"] = "1cm 2cm 1cm 2cm"

		data_return["normal_response"] = False
		data_return["return_response"] = show_pdf#(data_return, self.template, request.build_absolute_uri())

		return data_return
