# Own libraries
from app.views.main import Main
from app.models import *

# Django libraries
from django.views import View
from django.contrib.auth import logout
from django.shortcuts import HttpResponseRedirect

# System libraries
import datetime

class IndexPageDV(Main, View):
	"""
		view of index page
	"""
	template = 'index.html'
	page_title = "Inicio"

	def get_data(self, request, kwargs):

		import datetime
		x = datetime.datetime.now()
		fecha_desde = x.strftime('%Y-%m-%d')
		fechas_cal = Calendario.objects.filter(Q(fecha_inicio__gte = fecha_desde))

		nro_estudiantes = Estudiante.objects.filter(estado = Estudiante.E_ACTIVO).count()
		nro_docentes = Docente.objects.all().count()
		self._add_breadcrumb("Inicio", "")
		return dict(nro_docentes = nro_docentes, nro_estudiantes = nro_estudiantes)

class SearchPage(Main, View):
	"""
		view of index page
	"""
	template = 'search.html'

	def get_data(self, request, kwargs):
		busqueda = request.GET.get("q", "")
		if busqueda != "":
			full_items = []

			estudiantes = Estudiante.objects.filter((Q(nombre__icontains = busqueda) | Q(apellido__icontains = busqueda)) & Q(year = self.year) )
			for est in estudiantes:
				full_items.append({
					"url" : est.getProfileURL(),
					"image" : None,
					"label" : est.get_full_name(),
					"content" : "perfil del estudiante " + est.get_full_name() + " - " + str(est.grupo)
				})

			docentes = Docente.objects.filter(Q(nombre__icontains = busqueda) | Q(apellido__icontains = busqueda))
			for doc in docentes:
				full_items.append({
					"url" : doc.getProfileURL(),
					"image" : None,
					"label" : doc.get_full_name(),
					"content" : "perfil del docente " + doc.get_full_name() + " - " + str(doc.sede)
				})

			for men in self.menu:
				for item in men["submenu"]:
					#items = next((item for item in men["submenu"] if busqueda.lower() in item["label"].lower()), False)
					if busqueda.lower() in item["label"].lower() or busqueda.lower() in item["content"].lower():
						#item["label"] = item["label"].lower().replace(busqueda.lower(), "<mark>" + busqueda + "</mark>")
						#print(items)
						full_items.append(item)
			self._add_breadcrumb("Inicio", "/")
			self._add_breadcrumb("Búsqueda de \""+busqueda+"\"", "")
			return dict(query = busqueda, full_items = full_items)
		else:
			return {}

def logoutView (request):
	"""
		Logout from application, and returns to blog
	"""
	logout(request)
	return HttpResponseRedirect('/inicio/?success')

def loginView (request):
	"""
		Log in the page
	"""
	from django.contrib.auth import authenticate, login
	username = request.POST.get("usuario", None)
	password = request.POST.get("contrasena", None)
	user = authenticate(username=username, password=password)
	try:
		user = authenticate(username=username, password=password)
		if (user.es_docente() and user.docente().estado == Docente.E_ACTIVO) or (user.es_admin() and user.administrador().estado == Administrador.E_ACTIVO)  or (user.es_estudiante() and user.estudiante().estado == Estudiante.E_ACTIVO):
			if request.POST.get('remember_me'):
				# Expira en 2 semanas
				request.session.set_expiry(1209600)
				request.session.modified = True
			login(request, user)
			now = datetime.datetime.now()
			request.session['year'] = now.year
			return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
		else:
			return HttpResponseRedirect('/inicio/?inactive')

	except Exception as e:
		print(e)
		return HttpResponseRedirect('/inicio/?error')


def ChangeYearView (request):
	if request.user.is_authenticated:
		year = request.GET.get("year", None)
		for ye in FECHA_CHOOSER:
			if year in ye:
				request.session['year'] = year
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
