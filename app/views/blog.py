from django.shortcuts import render
from django.views import View
from app.models import *
# Create your views here.
from django.shortcuts import HttpResponseRedirect
from datetime import datetime

class Main(object):
	"""
		Main Class
	"""
	template = "index.html"
	js_files = []
	css_files = []
	aditional_templates = []
	page_title = ""
	is_main = False

	def __init__(self):
		"""
			Constructor of the main class
		"""
		self.__empty_all()
		#self.add_css("style_page.css")
		#self.add_js("page_handler.js")

	def __empty_all(self):
		"""
			Prevent data repeat
		"""
		self.js_files = []
		self.css_files = []
		self.aditional_templates = []

	def _add_css(self, css_file):
		"""
			Adds a css file to document
			@param css_file as css file path inside static/
		"""
		if css_file not in self.css_files:
			self.css_files.append(css_file)

	def _add_js(self, js_file):
		"""
			Adds a js file to document
			@param js_file as js file path inside static/
		"""
		if js_file not in self.js_files:
			self.js_files.append(js_file)


	def _add_aditional_template(self, template):
		"""
			Adds a template for include to document
			@param template as html file path inside static/
		"""
		if template not in self.aditional_templates:
			self.aditional_templates.append(template)

	def __initialProccess(self, request, kwargs):
		self.year = datetime.now().year
		self.inf = ConfiguracionInstitucion.objects.filter(year = self.year).last()
		if self.inf == None:
			self.inf = ConfiguracionInstitucion()
			self.inf.save()
		data_page = dict()
		data_page = self.get_data(request, kwargs)
		data_page["page_title"] = self.page_title
		data_page["aditional_templates"] = self.aditional_templates
		data_page["is_main"] = self.is_main

		data_page["js_files"] = self.js_files
		data_page["css_files"] = self.css_files

		is_logged = False
		if request.user.is_authenticated:
			is_logged = True

		data_page["is_logged"] = is_logged

		data_page["ie"] = {
			"nombre" : self.inf.nombre_institucion,
			"telefono" : self.inf.telefonos,
			"direccion" : str(self.inf.ciudad) + " - " + str(self.inf.direccion),
			"email" : self.inf.email,
			"corta_descripcion" : self.inf.descripcion_corta,
			"mostrar_docentes" : self.inf.mostrar_docentes
		}

		noticias_return = []
		noticias = NoticiasBlog.objects.all().order_by("fecha_publicacion")
		if len(noticias) > 0:
			for noti in noticias:
				noticias_return.append({
					"titulo" : noti.titulo,
					"pk" : noti.pk
				})
			data_page["noticias"] = noticias_return

		escuela_padres = []
		escuelas = EscuelaPadres.objects.all().order_by("fecha_publicacion")
		if len(escuelas) > 0:
			for escu in escuelas:
				escuela_padres.append({
					"titulo" : escu.titulo,
					"pk" : escu.pk
				})
			data_page["escuela_padres"] = escuela_padres

		return data_page

	def get(self, request, *args, **kwargs):
		"""
			Execute when the page makes a request
			@param request as request data
			@return render to response data
		"""
		data_page = self.__initialProccess(request, kwargs)
		return render(request, self.template, data_page)

	def get_data(self, request, *args):
		"""
			Returns the data to show
			@param request as request data
			is abstract
		"""
		pass



class IndexPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/index.html'
	page_title = "Inicio"
	is_main = True

	def get_data(self, request, kwargs):
		return dict()

class AdicionalPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/adicional.html'
	page_title = "Contenido adicional"

	def get_data(self, request, kwargs):
		adicional = {
			"mision" : self.inf.mision,
			"vision" : self.inf.vision,
			"historia" : self.inf.historia,
			"himno" : self.inf.himno,
			"adicional" : self.inf.adicional,
			"escudo" : self.inf.escudo,
			"manual_convivencia" : self.inf.manual_convivencia,
			"nit" : self.inf.nit,
			"dane" : self.inf.dane,
			"rector" : self.inf.nombre_rector,
		}
		data_return = {
			"adicional" : adicional
		}
		return data_return

class DocentesPageBlog(Main, View):
	"""
		view of index page
	"""
	template = 'blog/docentes.html'
	page_title = "Perfil docentes"

	def get_data(self, request, kwargs):
		data_return = {}
		if self.inf.mostrar_docentes:
			docentes = []
			for doc in Docente.objects.filter(estado = Docente.E_ACTIVO):
				docentes.append({
					"nombre" : doc.get_full_name(),
					"sede" : doc.sede,
					"descripcion" : doc.estudios,
					"email" : doc.email,
					"foto" : doc.foto
				})
			data_return = {
				"docentes" : docentes
			}
		return data_return

class ContactPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/contact.html'
	page_title = "Contacto"

	def get_data(self, request, kwargs):
		return dict(success = request.GET.get("success", None), error = request.GET.get("error", None))

	def post(self, request, *args, **kwargs):
		if request.POST.get("sendMessage", None) != None:
			nombre = request.POST.get("nombre", None)
			correo = request.POST.get("email", None)
			asunto = request.POST.get("asunto", None)
			mensaje = request.POST.get("mensaje", None)
			cont = ContactoBlog(nombre = nombre, email = correo, asunto = asunto, observaciones = mensaje)
			cont.save()
			return HttpResponseRedirect('/blog/contacto?success=True')
		return HttpResponseRedirect('/blog/contacto?error=True')

class GalleryPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/fotos.html'
	page_title = "Galería de fotos"

	def get_data(self, request, kwargs):
		return dict()

class CalendarioPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/calendario.html'
	page_title = "Calendario"

	def get_data(self, request, kwargs):
		data_return = {}
		self._add_css("calendario/fullcalendar.css")
		self._add_css("calendario/fullcalendar.print.css")
		self._add_js("calendario/jquery-ui.custom.min.js")
		self._add_js("calendario/fullcalendar.js")
		self._add_js("calendario_handler_noAdmin.js")
		data_return["form_action"] = "/adicional/calendario/"
		return data_return

class NoticiaPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/noticia.html'
	page_title = "Noticia o foro"

	def get_data(self, request, kwargs):
		try:
			pk = kwargs["pk"]
			noticia = NoticiasBlog.objects.get(pk = pk)
		except:
			return {"can_view" : False}

		noticia_data = {
			"titulo" : noticia.titulo,
			"cuerpo" : noticia.cuerpo,
			"imagen" : noticia.imagen,
			"fecha_edicion" : noticia.fecha_publicacion,
			"pk" : noticia.pk
		}

		data_return = {
			"can_view" : True,
			"noticia" : noticia_data
		}
		return data_return

class EscuelaPage(Main, View):
	"""
		view of index page
	"""
	template = 'blog/noticia.html'
	page_title = "Escuela de padres"

	def get_data(self, request, kwargs):
		try:
			pk = kwargs["pk"]
			escuela = EscuelaPadres.objects.get(pk = pk)
		except:
			return {"can_view" : False}

		escuela_data = {
			"titulo" : escuela.titulo,
			"cuerpo" : escuela.cuerpo,
			"imagen" : escuela.imagen,
			"fecha_edicion" : escuela.fecha_publicacion,
			"pk" : escuela.pk
		}

		data_return = {
			"can_view" : True,
			"noticia" : escuela_data
		}
		return data_return
