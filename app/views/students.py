# Own libraries
from app.views.main import Main, MainTable, save_file
from app.views.pdf import Consolidado
from app.models import *
from app.forms.students import *
from app.forms.various import *
from app.utils.pdf import show_pdf, getImpresionFormato, getImpresionOptions
from app.utils.funciones import *
from app.utils.macros import *

# Django libraries
from django.http import JsonResponse
from django.views import View
from django.db.models import Q
from django.conf import settings
from django.utils.formats import localize
from django.core.files.storage import default_storage
from django.shortcuts import HttpResponseRedirect, HttpResponse
from openpyxl import Workbook, load_workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.comments import Comment
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font

# System libraries
import xlwt
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import os

class Estudiantes(MainTable, View):
	"""
		view of Estudiantes page
	"""
	page_title = "Estudiantes"
	register_name = "estudiante"
	form_action = "/estudiantes/list/"
	model_object = Estudiante
	table_columns = dict(nombre = "Nombres", apellido = "Apellidos", grupo = "Grupo", documento ="Documento", sexo__format = "Sexo", estado__format = "Estado")
	return_edit_columns = ["nombre", "apellido", ["grupo", "pk"], "documento", "sexo", "estado", "tipo_documento", "celular1", "celular2"]
	form_edit = editEstudianteBasico
	form_add = addEstudiante
	mostrar_control_activos = True
	delete_register = False
	view_aparte = True
	can_view = True

	def _getFormatRow(self, column, obj):
		if column == "estado":
			return obj.getEstado()
		elif column == "sexo":
			return obj.getSexo()
		elif column == "zona":
			return obj.getZona()
		return ""

	def _addData(self, request):
		if request.user.es_admin():
			form_edit = self.form_add(request.POST)
			if form_edit.is_valid():
				documento = form_edit.cleaned_data['documento']
				try:
					user_repeat = User.objects.get(username = documento)
					self._add_error("Ya existe un estudiante con el documento ingresado")
				except:
					user = User.objects.create_user(username=documento, password=documento)
					new_object = form_edit.save(commit=False)
					new_object.user = user
					new_object.save()
					form_edit.save_m2m()
					salud = EstudianteSalud(estudiante = new_object)
					salud.save()
					adic = EstudianteAdicional(estudiante = new_object)
					adic.save()
					return True
			else:
				self._add_error(form_edit.errors)
		return False

	def _deleteData(self, request):
		object_id = request.POST.get("object_id", None)
		if object_id != None and request.user.es_admin():
			sed = self.model_object.objects.get(pk = object_id)
			if Estudiante.objects.filter(user = sed.user).count() <= 1:
				sed.user.delete()
			sed.delete()
			return True
		return False

	def _get_data_tables(self, request):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Estudiantes", "")

	def _getFilerTable(self, value):
		fil = Q(year = self.year)
		if self.user.es_estudiante():
			fil = fil & Q(user = self.user)
		elif self.user.es_docente():
			fil = fil & self.user.docente().filtroEstudiantesGrupo(self.year)
		if value != "":
			fil = fil & (Q(nombre__icontains = value) | Q(apellido__icontains = value) | Q(grupo__titulo__icontains=value) | Q(sexo__icontains = value) | Q(zona__icontains = value) | Q(estado__icontains = value) | Q(documento__icontains = value) | Q(pk__icontains = value))
			return fil
		return fil

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class StudentProfile(Main, View):
	template = "student/profile.html"
	page_title = "Perfil de estudiante"

	def clean_format(self, text):
		return str(text).replace("None", "-").replace("False", "No").replace("True", "Si")

	def get_data(self, request, kwargs):
		lista_years = []
		try:
			estudiante_id = int(kwargs['pk'])
			estudiante_initial = Estudiante.objects.get(Q(pk = estudiante_id))
			user_id = estudiante_initial.user.pk
			estudiante = Estudiante.objects.get(Q(user__pk = user_id) & Q(year = self.year))
			print(estudiante.copy_id)
			#estudiante = Estudiante.objects.get(Q(pk = estudiante_id) & Q(year = self.year))
			estudiantes_lista = Estudiante.objects.filter(~Q(pk = estudiante_id) & Q(user = estudiante.user)).order_by("year")
			if estudiante.year != self.year:
				for item in estudiantes_lista:
					lista_years.append(item.year)
					if int(item.year) == int(self.year):
						estudiante = item
		except Exception as e:
			if self.user.es_estudiante():
				estudiante = self.user.estudiante()
			else:
				self.can_view_page = False
				return {}

		propio_perfil = False
		if self.user.es_estudiante() and self.user != estudiante.user:
			self.can_view_page = False
			return {}
		else:
			if self.user.es_estudiante():
				propio_perfil = True

		acudiente = estudiante.getAcudiente()
		salud = estudiante.getSalud()
		adicional = estudiante.getAdicional()

		anecdotario = Anecdotario.objects.filter(Q(estudiante = estudiante))
		if len(anecdotario) == 0:
			form_edit_anecdotario = editEstudianteAnecdotario()
		else:
			form_edit_anecdotario = editEstudianteAnecdotario(instance = anecdotario[0])

		data_return = {}

		if not self.user.es_estudiante():
			data_return["form_edit_anecdotario"] = form_edit_anecdotario

		own_profile = False
		if estudiante == self.user.estudiante():
			own_profile = True

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Perfil de estudiante", "")

		edad = 0
		if estudiante.fecha_nacimiento != None:
			edad = date.today().year - estudiante.fecha_nacimiento.year

			cumpleanios = estudiante.fecha_nacimiento + relativedelta(years=edad)
			if cumpleanios > date.today():
				edad = edad - 1

		data_estudiante = {
			"copy_id" : estudiante.copy_id,
			"pk" : estudiante.pk,
			"nombre" : estudiante.nombre,
			"apellido" : estudiante.apellido,
			"grupo" : estudiante.grupo,
			"genero" : estudiante.getSexo(),
			"documento" : estudiante.documento,
			"estado" : estudiante.getEstado(),
			"creation_date" : estudiante.fecha_ingreso,
			"last_login" : self.clean_format(estudiante.user.last_login),
			"foto" : estudiante.foto,
			"email" : estudiante.email,
			"anio" : self.year,
			"tipo_documento" : estudiante.getTipoDocumento(),
			"municipio_expedicion" : estudiante.municipio_expedicion,
			"departamento_expedicion" : estudiante.departamento_expedicion,
			"fecha_nacimiento" : estudiante.fecha_nacimiento,
			"edad" : edad,
			"municipio_nacimiento" : estudiante.municipio_nacimiento,
			"departamento_nacimiento" : estudiante.departamento_nacimiento,
			"direccion_residencia" : estudiante.direccion_residencia,
			"municipio_residencia" : estudiante.municipio_residencia,
			"departamento_residencia" : estudiante.departamento_residencia,
			"celular1" : estudiante.celular1,
			"celular2" : estudiante.celular2,
			"zona" : estudiante.getZona(),
			"fecha_retiro" : self.clean_format(estudiante.fecha_retiro),
			"observaciones" : estudiante.observaciones,
		}

		data_salud = {
			"eps" : self.clean_format(salud.eps),
			"tipo_sangre" : self.clean_format(salud.getTipoSangre()),
			"puntaje_sisben" : self.clean_format(salud.puntaje_sisben),
			"ars" : self.clean_format(salud.ars),
			"enfermedades" : self.clean_format(salud.enfermedades),
			"alergias" : self.clean_format(salud.alergias),
			"medicamentos" : self.clean_format(salud.medicamentos),
			"limitaciones" : self.clean_format(salud.getLimitacionesTxt())
		}

		data_acudiente = {
			"nombre" : acudiente.nombre,
			"parentesco" : self.clean_format(acudiente.parentesco),
			"edad" : self.clean_format(acudiente.edad),
			"identificacion" : self.clean_format(acudiente.identificacion),
			"expedido_en" : self.clean_format(acudiente.expedido_en),
			"ocupacion" : self.clean_format(acudiente.ocupacion),
			"municipio_residencia" : self.clean_format(acudiente.municipio_residencia),
			"direccion_residencia" :self.clean_format(acudiente.direccion_residencia),
			"celular1" : self.clean_format(acudiente.celular1),
			"celular2" : self.clean_format(acudiente.celular2),
			"email" : self.clean_format(acudiente.email),
			"cabeza_familia" : self.clean_format(acudiente.cabeza_familia)
		}

		data_adicional = {
			"desplazado" : self.clean_format(adicional.desplazado),
			"victima_conflicto" : self.clean_format(adicional.victima_conflicto),
			"familias_accion" : self.clean_format(adicional.familias_accion),
			"cabeza_familia" : self.clean_format(adicional.alumna_madre_cabeza),
			"beneficiario_fuerza_publica" : self.clean_format(adicional.beneficiario_fuerza_publica),
			"beneficiario_heroe_nacion" : self.clean_format(adicional.beneficiario_heroe_nacion),
			"sabe_nadar" : self.clean_format(adicional.sabe_nadar),
			"dificultades_aprendizaje" : self.clean_format(adicional.dificultades_aprendizaje),
			"lugar_desplazamiento" : self.clean_format(adicional.lugar_desplazamiento),
			"etnia_indigena" : self.clean_format(adicional.etnia_indigena),
			"tipo_vivienda" : self.clean_format(adicional.tipo_vivienda),
			"estrato_socioeconomico" : self.clean_format(adicional.estrato_socioeconomico),
			"vive_con" : self.clean_format(adicional.vive_con),
			"personas_hogar" : self.clean_format(adicional.personas_hogar),
			"hermanos_ie" : self.clean_format(adicional.hermanos_ie),
			"ie_proviene" : self.clean_format(adicional.ie_proviene),
			"lugar_proviene" : self.clean_format(adicional.lugar_proviene)
		}
		notas = estudiante.getNotasActivas(self.year, get_tabla_notas(self.year), min_lost = self.min_lost)

		periodos = []
		for per in Periodo.objects.filter(year = self.year).order_by("es_final", "orden"):
			if per.isActive():
				periodos.append({
					"pk" : per.pk,
					"titulo" : per.titulo
				})

		data_return["own_profile"] = own_profile
		data_return["estudiante"] = data_estudiante
		data_return["acudiente"] = data_acudiente
		data_return["salud"] = data_salud
		data_return["adicional"] = data_adicional
		data_return["periodos"] = periodos
		data_return["propio_perfil"] = propio_perfil
		data_return["notas"] = notas
		data_return["periodos"] = periodos

		# Formularios
		data_return["form_edit_pass"] = changePass()
		if self.user.es_admin():
			data_return["form_edit_avatar"] = editEstudianteAvatar(instance = estudiante)
			data_return["edit_personal"] = editEstudiantePersonal(prefix='personal', instance = estudiante)
			data_return["edit_acudiente"] = editEstudianteAcudiente(prefix='acudiente', instance = acudiente)
			data_return["edit_salud"] = editEstudianteSalud(prefix='salud', instance = salud)
			data_return["edit_adicional"] = editEstudianteAdicional(prefix='adicional', instance = adicional)
			data_return["edit_limitaciones"] = editEstudianteLimitaciones(limitaciones = salud.getLimitaciones())
			data_return["form_action"] = "/estudiantes/list/view/" + str(estudiante_id)

		return data_return

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)

		if tipo_edit == "datos_completos" and object_id != None:
			try:
				estudiante = Estudiante.objects.get(pk = object_id)
				acudiente = estudiante.getAcudiente()
				salud = estudiante.getSalud()
				adicional = estudiante.getAdicional()
				if self.user.es_admin():
					acudiente_identificacion = request.POST.get("acudiente-identificacion", None)
					if str(acudiente_identificacion) != str(acudiente.identificacion):
						try:
							new_acudiente = Acudiente.objects.get(Q(identificacion = acudiente_identificacion) & ~Q(pk = acudiente.pk) & Q(year = self.year))
							if len(EstudianteAcudiente.objects.filter(acudiente = acudiente)) <= 1:
								acudiente.delete()
							acudiente = new_acudiente
							est_ac = EstudianteAcudiente(acudiente = acudiente, estudiante = estudiante)
							est_ac.save()
						except Exception as e:
							pass
					form_salud = editEstudianteSalud(prefix='salud', data = request.POST, instance = salud)
					form_acudiente = editEstudianteAcudiente(prefix='acudiente', data = request.POST, instance = acudiente)
					form_personal = editEstudiantePersonal(prefix='personal', data = request.POST, instance = estudiante)
					form_adicional = editEstudianteAdicional(prefix='adicional', data = request.POST, instance = adicional)
					if form_salud.is_valid() and form_acudiente.is_valid() and form_personal.is_valid() and form_adicional.is_valid():
						form_adicional.save()
						form_salud.save()
						form_acudiente.save()
						form_personal.save()
						return True
					self._add_error(form_salud.errors)
					self._add_error(form_acudiente.errors)
					self._add_error(form_personal.errors)
					self._add_error(form_adicional.errors)
			except Exception as e:
				print("Error: ", e)
				pass

		elif tipo_edit == "limitaciones" and object_id != None:
			try:
				estudiante = Estudiante.objects.get(pk = object_id)
				if self.user.es_admin:
					form = editEstudianteLimitaciones(data = request.POST)
					if form.is_valid():
						limitaciones = form.cleaned_data["limitaciones"]
						if limitaciones != None:
							for limi in LimitacionEstudiante.objects.filter(Q(estudiante = estudiante)):
								limi.delete()
							for limi in limitaciones:
								new_limi = LimitacionEstudiante(estudiante = estudiante, limitacion = limi)
								new_limi.save()
							return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass

		elif tipo_edit == "user_avatar" and object_id != None:
			try:
				estudiante = Estudiante.objects.get(pk = object_id)
				if self.user.es_admin:
					form = editEstudianteAvatar(data = request.POST, files = request.FILES, instance = estudiante)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		elif tipo_edit == "anecdotario" and object_id != None:
			try:
				estudiante = Estudiante.objects.get(pk = object_id)
				anecdotario = Anecdotario.objects.filter(Q(estudiante = estudiante))
				if len(anecdotario) == 0:
					anecdotario = Anecdotario(estudiante = estudiante)
					anecdotario.save()
				else:
					anecdotario = anecdotario[0]
				form_edit_anecdotario = editEstudianteAnecdotario(data = request.POST, instance = anecdotario)
				if form_edit_anecdotario.is_valid() and (self.user.es_admin or self.user.es_docente):
					form_edit_anecdotario.save()
					return True
				self._add_error(form_edit_anecdotario.errors)
			except:
				pass
		elif tipo_edit == "password" and object_id != None:
			try:
				estudiante = Estudiante.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and (self.user.es_admin or self.user == estudiante):
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						estudiante.user.set_password(pass1)
						estudiante.user.save()
						return True
					else:
						self._add_error("Las contraseñas no coinciden")
				else:
					self._add_error("No se puede editar la contraseña")
			except Exception as ex:
				print(ex)

		return False

	def _proccessInforme(self, request):
		estudiante_id = request.POST.get("object_id", "")
		form_name = request.POST.get("form_name", "")
		if str(form_name) == "constancia_estudio":
			motivo = request.POST.get("motivo", "")
			return HttpResponseRedirect("/pdf/constancia_estudio/" + str(estudiante_id) + "/" + str(self.year) + "?motivo=" + str(motivo))
		return HttpResponseRedirect("/estudiantes/list/view/" + str(estudiante_id))

class EditarNotasView(Main, View):
	template = "notas/notas.html"
	page_title = "Editar/ver notas"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def _editNota(self, request):
		valor = request.POST.get("valor_nota", 0.0)
		inasistencias = request.POST.get("inas", 0)
		estudiante_id = request.POST.get("estudiante_id", None)
		periodo_id = request.POST.get("periodo_id", None)
		asignacion_id = request.POST.get("asignacion_id", None)
		tipo = request.POST.get("tipo", None)
		return self.saveNota(valor, inasistencias, estudiante_id, periodo_id, asignacion_id, tipo = tipo)

	def saveNota(self, valor, inasistencias, estudiante_id, periodo_id, asignacion_id, tipo = "nota"):
		if self.user.es_estudiante() or tipo == None:
			return {"concept" : "error", "error" : "No puede editar", "tipo" : tipo}
		try:
			fil = Q(Q(year = self.year) & Q(pk = asignacion_id))
			if self.user.es_docente():
				fil = fil & Q(docente = self.user.docente())
			estudiante = Estudiante.objects.get(pk = estudiante_id)
			try:
				asignacion = AsignacionDocente.objects.get(fil)
			except:
				return {"concept" : "error", "error" : "Esta asignación no le pertenece", "tipo" : tipo}
			periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = self.year))
			tabla_notas = get_tabla_notas(self.year)

			nota_est = estudiante.getNota(tabla_notas, asignacion.asignatura, periodo)
			if nota_est == None:
				nota_est = estudiante.createNota(asignacion.asignatura, periodo, tabla_notas, valor, inasistencias)
			else:
				if tipo == "nota" or tipo == "nota_inas":
					valor = float(valor)
					if valor > MAX_NOTA:
						valor = MAX_NOTA
					elif valor < MIN_NOTA:
						valor = MIN_NOTA
					nota_est.valor = valor
				if tipo == "inas" or tipo == "nota_inas":
					nota_est.inasistencias = inasistencias
				if tipo == "habilitacion":
					if valor > MAX_NOTA:
						valor = MAX_NOTA
					elif valor < MIN_NOTA:
						valor = MIN_NOTA
					nota_est.habilitacion = valor
				nota_est.save()
			return {"concept" : "success", "fecha" : localize(nota_est.fecha_moficiacion), "tipo" : tipo}
		except Exception as ex:
			print(ex)
			return {"concept" : "error", "error" : "Revise los datos que ingresó", "tipo" : tipo}

	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		json_data = {"concept" : "error", "error" : "No hay datos"}
		if action == "editNota":
			json_data = self._editNota(request)

		return JsonResponse(json_data)

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Editar/ver notas", "")
		data_return = dict()

		periodo_id = request.GET.get("periodo", None)
		asignacion_id = request.GET.get("asignacion", None)
		tipo = str(kwargs["tipo"])
		es_recuperacion = False
		url_form = "/estudiantes/EditarNotas/Normal"
		if tipo == "Recuperacion":
			es_recuperacion = True
			url_form = "/estudiantes/EditarNotas/Recuperacion"
		form_nota = notaEstudiante(user = self.user, year = self.year)

		if self.user.es_estudiante():
			self.can_view_page = False
		else:
			estudiante_lista = []
			if periodo_id != None and asignacion_id != None:
				try:
					form_nota["periodo"].initial = periodo_id
					form_nota["asignacion"].initial = asignacion_id

					fil = Q(pk = asignacion_id)
					if self.user.es_docente():
						fil = fil & Q(docente = self.user.docente())

					periodo = Periodo.objects.get(pk = periodo_id)
					asignacion = AsignacionDocente.objects.get(pk = asignacion_id)
					tabla_notas = get_tabla_notas(self.year)
					nro = 1

					for es in Estudiante.objects.filter(Q(grupo = asignacion.grupo) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido"):
						nota_est = es.getNota(tabla_notas, asignacion.asignatura, periodo)
						add = True
						if es_recuperacion and nota_est.valor >= self.min_lost:
							add = False
						if add:
							valor = 0.0
							inas = 0
							fecha = ""
							habilitacion = 0.0
							if nota_est != None:
								valor = nota_est.valor
								inas = nota_est.inasistencias
								fecha = nota_est.fecha_moficiacion
								habilitacion = nota_est.habilitacion
							estudiante_lista.append({"nro": nro, "nombre" : es.apellido + " " + es.nombre, "pk" : es.pk, "habilitacion": habilitacion, "nota" : valor, "inas" : inas, "fecha" : fecha})
							nro += 1

					data_return["estudiante_lista"] = estudiante_lista
					data_return["asignacion"] = asignacion
					data_return["periodo"] = periodo

					self._add_js("notas.js")
					self._add_css("notas.css")
				except Exception as e:
					print(e)
					pass

		fechas_rango = ""
		fecha_habilitado = False
		try:
			per = Periodo.objects.filter(Q(year = self.year) & Q(fecha_apertura__lte = datetime.now()) & Q(fecha_cierre__gte = datetime.now()))[0]
			fechas_rango = str(localize(per.fecha_apertura)) + " - " + str(localize(per.fecha_cierre))
			fecha_habilitado = True
		except Exception as ex:
			print(ex)
			pass

		if self.user.es_admin():
			fecha_habilitado = True

		data_return["form_nota"] = form_nota
		data_return["fecha_habilitado"] = fecha_habilitado
		data_return["fechas_rango"] = fechas_rango
		data_return["es_recuperacion"] = es_recuperacion
		data_return["url_form"] = url_form
		return data_return


class NotasExcel(Main, View):
	template = "notas/excel.html"
	page_title = "Agregar notas por excel"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		json_data = {"concept" : "error", "error" : "No hay datos"}
		if action == "editNota":
			json_data = self._editNota(request)

		return JsonResponse(json_data)

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Notas a través de excel", "")
		data_return = dict()

		periodo_id = request.GET.get("periodo", None)
		asignacion_id = request.GET.get("asignacion", None)
		preview = request.GET.get("preview", None)
		file = request.GET.get("file", None)
		form_download = notaEstudiante(prefix = "download", user = self.user, year = self.year)
		form_upload = notaEstudianteExcel(user = self.user, year = self.year)

		if self.user.es_estudiante():
			self.can_view_page = False
		else:
			estudiantes_lista = []
			if periodo_id != None and asignacion_id != None:
				form_download["periodo"].initial = periodo_id
				form_download["asignacion"].initial = asignacion_id
				form_upload["periodo"].initial = periodo_id
				form_upload["asignacion"].initial = asignacion_id

				save = request.GET.get("save", None)

				file_path = os.path.join(settings.MEDIA_ROOT, "excel_nota", str(file))
				if preview != None and default_storage.exists(file_path):
					estudiantes_lista = []

					#asignacion = AsignacionDocente.objects.get(Q(pk = asignacion_id))
					#periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = self.year))

					asignacion = 1
					periodo = 1

					wb = load_workbook(file_path)
					worksheet = wb.active
					row_index = 1
					for row in worksheet.iter_rows():
						if row_index > 1:

							asignatura = Asignatura.objects.filter(Q(titulo = str(worksheet["E" + str(row_index)].value)) & Q(year = self.year))
							estudiante = Estudiante.objects.filter(Q(documento = worksheet["D" + str(row_index)].value) & Q(year = self.year))
							periodo = Periodo.objects.filter(Q(titulo = str(worksheet["F" + str(row_index)].value).upper()) & Q(year = self.year))

							if len(asignatura) > 0 and len(periodo) > 0 and len(estudiante) > 0:
								asignacion = AsignacionDocente.objects.filter(Q(year = self.year) & Q(asignatura = asignatura[0]) & Q(grupo = estudiante[0].grupo))

								if len(asignacion) > 0:

									row_data = {}
									row_data["pk"] = estudiante[0].pk
									row_data["estudiante"] = estudiante[0].get_full_name()
									row_data["periodo_pk"] = periodo[0].pk
									row_data["asignacion_pk"] = asignacion[0].pk
									row_data["periodo"] = periodo[0].titulo
									row_data["asignatura"] = asignatura[0].titulo

									row_data["nota"] = float(worksheet["A" + str(row_index)].value)
									habilitacion = worksheet["B" + str(row_index)].value
									if habilitacion == None or habilitacion == "":
										habilitacion = 0
									row_data["habilitacion"] = float(habilitacion)
									inas = worksheet["C" + str(row_index)].value
									if inas == None or inas == "":
										inas = 0
									row_data["inas"] = inas
									estudiantes_lista.append(row_data)

							"""
							row_data = {}
							row_data["pk"] = worksheet["B" + str(row_index)].value
							estudiante = Estudiante.objects.filter(Q(pk = row_data["pk"]) & Q(grupo = asignacion.grupo) & Q(year = self.year))
							if len(estudiante) > 0:
								estudiante = estudiante[0]
								row_data["pk"] = estudiante.pk
								row_data["estudiante"] = str(estudiante.apellido) + " " + str(estudiante.nombre)
								row_data["nota"] = float(worksheet["D" + str(row_index)].value)
								if row_data["nota"] > MAX_NOTA:
									row_data["nota"] = MAX_NOTA
								elif row_data["nota"] < MIN_NOTA:
									row_data["nota"] = MIN_NOTA
								row_data["inas"] = worksheet["E" + str(row_index)].value
								row_data["nro"] = row_index - 1
								estudiantes_lista.append(row_data)
							"""
						row_index += 1

					data_return["estudiantes_lista"] = estudiantes_lista
					#data_return["periodo"] = {
					#	"pk" : periodo.pk,
					#	"titulo" : periodo.titulo
					#}
					data_return["file"] = file
					#data_return["asignacion"] = asignacion

					data_return["tipo_add"] = 2

					if save != None:
						for row in estudiantes_lista:
							#response = EditarNotasView.saveNota(self, row["nota"], row["inas"], row["pk"], periodo.pk, asignacion.pk, tipo = "nota_inas")
							response = EditarNotasView.saveNota(self, row["nota"], row["inas"], row["pk"], row["periodo_pk"], row["asignacion_pk"], tipo = "nota_inas")
							response2 = EditarNotasView.saveNota(self, row["habilitacion"], row["inas"], row["pk"], row["periodo_pk"], row["asignacion_pk"], tipo = "habilitacion")

						data_return["normal_response"] = False
						data_return["return_response"] = HttpResponseRedirect("/estudiantes/excel/Notas?mensaje=Datos agregados con éxito")

		fechas_rango = ""
		fecha_habilitado = False
		try:
			per = Periodo.objects.filter(Q(year = self.year) & Q(fecha_apertura__lte = datetime.now()) & Q(fecha_cierre__gte = datetime.now()))[0]
			fechas_rango = str(localize(per.fecha_apertura)) + " - " + str(localize(per.fecha_cierre))
			fecha_habilitado = True
		except Exception as ex:
			pass

		if self.user.es_admin():
			fecha_habilitado = True

		data_return["form_download"] = form_download
		data_return["form_upload"] = form_upload
		data_return["fecha_habilitado"] = fecha_habilitado
		data_return["fechas_rango"] = fechas_rango
		return data_return

	def _addData(self, request):
		asignacion_id = request.POST.get("asignacion", None)
		periodo_id = request.POST.get("periodo", None)
		archivo = request.FILES['archivo']
		file = save_file(archivo, "excel_nota")
		return HttpResponseRedirect('/estudiantes/excel/Notas?preview&file=' + str(file) + '&periodo=' + str(periodo_id) + "&asignacion=" + str(asignacion_id))

	def _proccessInforme(self, request):
		response = HttpResponseRedirect('/estudiantes/excel/Notas?error')
		asignacion_id = request.POST.get("download-asignacion", None)
		periodo_id = request.POST.get("download-periodo", None)
		if periodo_id != None and asignacion_id != None:
			asignacion = AsignacionDocente.objects.get(Q(pk = asignacion_id) & Q(year = self.year))
			periodo = Periodo.objects.get(pk = periodo_id)
			nro = 1
			data_return = {}
			notas_return = []
			for estudiante in Estudiante.objects.filter(Q(grupo = asignacion.grupo) & Q(year = self.year) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido"):
				nota = estudiante.getNota(get_tabla_notas(self.year), asignacion.asignatura, periodo)
				notas_return.append({
					"nro" : nro,
					"pk" : estudiante.pk,
					"estudiante" : str(estudiante.apellido) + " " + str(estudiante.nombre),
					"nota" : nota.valor,
					"inas" : nota.inasistencias
				})
				nro += 1
			data_return["estudiantes_lista"] = notas_return
			data_return["asignacion"] = str(asignacion.grupo) + " - " + str(asignacion.asignatura)
			data_return["asignacion_pk"] = asignacion.pk
			data_return["periodo"] = {
				"titulo" : periodo.titulo,
				"pk" : periodo.pk
			}
			fill = PatternFill("solid", fgColor='B6B6B6')
			thin = Side(border_style="thin", color="000000")
			border = Border(top=thin, left=thin, right=thin, bottom=thin)

			wb = Workbook()
			sheet = wb.active
			sheet.title = "Listado"

			columns = ['Nro', "ID", "Estudiante", "Nota", "Inas."]
			row_num = 1
			for col_num in range(len(columns)):
				sheet.cell(row = row_num, column = col_num + 1).value = columns[col_num]
				sheet.cell(row = row_num, column = col_num + 1).fill = fill
				sheet.cell(row = row_num, column = col_num + 1).border = border
			for nota in notas_return:
				row_num += 1
				sheet.cell(row = row_num, column = 1).value = nota["nro"]
				sheet.cell(row = row_num, column = 1).border = border
				sheet.cell(row = row_num, column = 2).value = nota["pk"]
				sheet.cell(row = row_num, column = 2).border = border
				comment = Comment("No modifique este valor", "Da Vinci")
				sheet.cell(row = row_num, column = 2).comment = comment
				sheet.cell(row = row_num, column = 3).value = nota["estudiante"]
				sheet.cell(row = row_num, column = 3).border = border
				sheet.cell(row = row_num, column = 4).value = nota["nota"]
				sheet.cell(row = row_num, column = 4).border = border
				comment = Comment("Modifique este campo con el valor de la nota", "Da Vinci")
				sheet.cell(row = row_num, column = 4).comment = comment
				sheet.cell(row = row_num, column = 5).value = nota["inas"]
				sheet.cell(row = row_num, column = 5).border = border
				comment = Comment("Modifique este campo con el valor de las inasistencias, desde este campo hacia la derecha, puede modificar lo que desee", "Da Vinci")
				sheet.cell(row = row_num, column = 5).comment = comment
			response = HttpResponse(content=save_virtual_workbook(wb), content_type='application/ms-excel')
			response['Content-Disposition'] = 'attachment; filename=ListadoEstudiantes.xlsx'
		return response

class EditarNotasIndividualView(Main, View):
	template = "notas/individual.html"
	page_title = "Editar/ver notas individual"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		json_data = {"concept" : "error", "error" : "No hay datos"}
		if action == "editNota":
			json_data = self._editNota(request)

		return JsonResponse(json_data)

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Editar/ver notas individual", "")
		data_return = dict()

		periodo_id = request.GET.get("periodo", None)
		estudiante_id = request.GET.get("estudiante", None)
		form_nota = notaEstudianteIndividual(user = self.user, year = self.year)

		if self.user.es_estudiante():
			self.can_view_page = False
		else:
			asignaturas_lista = []
			if periodo_id != None and estudiante_id != None:
				try:
					form_nota["periodo"].initial = periodo_id
					form_nota["estudiante"].initial = estudiante_id
					estudiante = Estudiante.objects.get(Q(pk = estudiante_id) & Q(year = self.year))
					periodo = Periodo.objects.get(pk = periodo_id)
					fil = Q(grupo = estudiante.grupo)
					if self.user.es_docente():
						fil = fil & Q(docente = self.user.docente())

					nro = 1
					notas_return = []
					for asignacion in AsignacionDocente.objects.filter(fil):
						nota = estudiante.getNota(get_tabla_notas(self.year), asignacion.asignatura, periodo)
						notas_return.append({
							"nro" : nro,
							"nombre" : str(asignacion.asignatura.titulo),
							"nota" : nota.valor,
							"inas" : nota.inasistencias,
							"fecha" : nota.fecha_moficiacion,
							"asignacion_id" : asignacion.pk
						})
						nro += 1

					data_return["notas_return"] = notas_return
					data_return["estudiante"] = str(estudiante.get_full_name()) + " - " + str(estudiante.grupo)
					data_return["estudiante_pk"] = estudiante.pk
					data_return["periodo"] = {
						"titulo" : periodo.titulo,
						"pk" : periodo.pk
					}

					self._add_js("notas.js")
					self._add_css("notas.css")
				except Exception as e:
					print(e)
					self.can_view_page = False
					pass

		fechas_rango = ""
		fecha_habilitado = False
		try:
			per = Periodo.objects.filter(Q(year = self.year) & Q(fecha_apertura__lte = datetime.now()) & Q(fecha_cierre__gte = datetime.now()))[0]
			fechas_rango = str(localize(per.fecha_apertura)) + " - " + str(localize(per.fecha_cierre))
			fecha_habilitado = True
		except Exception as ex:
			print(ex)
			pass

		if self.user.es_admin():
			fecha_habilitado = True

		data_return["form_nota"] = form_nota
		data_return["fecha_habilitado"] = fecha_habilitado
		data_return["fechas_rango"] = fechas_rango
		return data_return



class FiltroNotas(Main, View):
	template = "form.html"
	page_title = "Filtro de notas"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):

		forms = []
		forms.append(dict(href="first", tab="Opciones básicas", form = FiltroNotasBasico(user = self.user, year = self.year)))
		forms.append(dict(href="three", tab="Impresión", form=ImpresionFormato()))
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Generar filtro de notas", "")

		# Data to return
		data_return = dict()
		data_return["forms"] = forms
		data_return["target"] = "target='_blank'"
		data_return["tipo_form"] = "informeForm"
		data_return["form_action"] = "/estudiantes/filtro_nota"

		return data_return

	def _proccessInforme(self, request):
		form_basic = FiltroNotasBasico(request.POST)
		#formatos = ImpresionFormato(request.POST)
		template_path = "pdf/filtro_notas.html"
		try:
			promedio_min = float(request.POST.get("promedio_min", MIN_NOTA))
			promedio_max = float(request.POST.get("promedio_max", MAX_NOTA))
			maximo_perdidas = int(request.POST.get("maximo_perdidas", 200))
			simular_notas = request.POST.get("simular_notas", False)

			grupo_pk = request.POST.get("grupo")
			grupo = Grupo.objects.get(Q(pk = grupo_pk))
			periodo_pk = request.POST.get("periodo")
			periodo = Periodo.objects.get(Q(pk = periodo_pk) & Q(year = self.year))
			all_estudiantes = Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year) & Q(estado = Estudiante.E_ACTIVO)).order_by("apellido")
		except Exception as e:
			self.can_view_page = False
			return self.error_pdf(request)
		filters = {
			"promedio_min" : promedio_min,
			"promedio_max" : promedio_max,
			"maximo_perdidas" : maximo_perdidas,
			"simular_notas" : simular_notas
		}
		data_return = Consolidado.getNotasData(self, self.year, periodo, grupo, all_estudiantes, filters)
		periodos_all = []
		filter_periodo = periodo.orden
		if simular_notas:
			filter_periodo = 6
		for per in Periodo.objects.filter(Q(year = self.year) & Q(orden__lte = filter_periodo)):
			simulado = ""
			if per.orden > periodo.orden:
				simulado = "**Simulado**"
			periodos_all.append({
				"pk" : per.pk,
				"titulo" : per.titulo,
				"simulado" : simulado
			})
		data_return["periodos_all"] = periodos_all
		data_return["grupo"] = {
			"pk" : grupo.pk,
			"titulo" : grupo.getGrupo()
		}

		data_return = getImpresionFormato(request.POST, data_return, self.year)
		return show_pdf(data_return, template_path, request.build_absolute_uri())

class GenerarListado(Main, View):
	template = "form.html"
	page_title = "Generar listado"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):

		forms = []
		forms.append(dict(href="first", tab="Opciones básicas", form = EstudiantesListadoBasica()))
		forms.append(dict(href="second", tab="Filtros", form = EstudiantesListadoFitros()))
		forms.append(dict(href="alone", tab="Campos a seleccionar", form=CamposExportar()))
		forms.append(dict(href="three", tab="Impresión", form=ImpresionFormato()))
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Generar listado de estudiantes", "")

		# Data to return
		data_return = dict()
		data_return["forms"] = forms
		data_return["target"] = "target='_blank'"
		data_return["tipo_form"] = "informeForm"
		data_return["form_action"] = "/estudiantes/listado"

		return data_return

	def _proccessInforme(self, request):
		# TODO: añadir fecha retiro y eso
		form_basic = EstudiantesListadoBasica(request.POST)
		form_filtros = EstudiantesListadoFitros(request.POST)
		form_exportar = CamposExportar(request.POST)
		if form_basic.is_valid() and form_filtros.is_valid() and form_exportar.is_valid():

			# Cabecera y demás
			Datos = ConfiguracionInformes.objects.last()
			DatosIns = ConfiguracionInstitucion.objects.last()
			cabecera = Datos.cabecera_informe_comun
			escudo = settings.MEDIA_URL + str(DatosIns.escudo)

			# Organizar filtros
			filT = Q()
			grupo_id = form_basic.cleaned_data["grupo"]
			if grupo_id != None:
				filT = filT & Q(grupo = grupo_id)
			grado_id = form_basic.cleaned_data["grado"]
			if grado_id != None:
				filT = filT & Q(grupo__grado = grado_id)
			sede_id = form_basic.cleaned_data["sede"]
			if sede_id != None:
				filT = filT & Q(grupo__grado__sede = sede_id)
			if not form_basic.cleaned_data["incluir_retirados"]:
				filT = filT & Q(estado = Estudiante.E_ACTIVO)
			if form_basic.cleaned_data["mostrar_solo_retirados"]:
				filT = filT & ~Q(estado = Estudiante.E_ACTIVO)
			sexo_fil = form_filtros.cleaned_data["sexo"]
			if sexo_fil != "":
				filT = filT & Q(sexo = sexo_fil)
			zona_fil = form_filtros.cleaned_data["zona"]
			if zona_fil != "":
				filT = filT & Q(zona = zona_fil)
			sangre_fil = form_filtros.cleaned_data["tipo_sangre"]
			if sangre_fil != "":
				filT = filT & Q(salud__tipo_sangre = sangre_fil)
			tipo_documento_fil = form_filtros.cleaned_data["tipo_documento"]
			if tipo_documento_fil != "":
				filT = filT & Q(tipo_documento = tipo_documento_fil)
			desplazados = form_filtros.cleaned_data["desplazados"]
			if desplazados == "1":
				filT = filT & Q(adicional__desplazado = True)
			if desplazados == "2":
				filT = filT & Q(adicional__desplazado = False)
			familias_accion = form_filtros.cleaned_data["familias_accion"]
			if familias_accion == "1":
				filT = filT & Q(adicional__familias_accion = True)
			if familias_accion == "2":
				filT = filT & Q(adicional__familias_accion = False)
			nacidos_antes = form_filtros.cleaned_data["nacidos_antes"]
			nacidos_despues = form_filtros.cleaned_data["nacidos_despues"]
			if nacidos_antes != "":
				filT = filT & Q(fecha_nacimiento__lte= nacidos_antes)
			elif nacidos_despues != "":
				filT = filT & Q(fecha_nacimiento__gte= nacidos_despues)
			ingresados_desde = form_filtros.cleaned_data["ingresados_desde"]
			ingresados_hasta = form_filtros.cleaned_data["ingresados_hasta"]
			if ingresados_hasta != "" and ingresados_hasta != "":
				filT = filT & Q(fecha_ingreso__range = (ingresados_desde, ingresados_hasta))
			if form_filtros.cleaned_data["aprobado"]:
				filT = filT & Q(aprobado = True)
			if form_filtros.cleaned_data["aprobado_pasado"]:
				filT = filT & Q(aprobado_pasado = True)

			self.mostrar_fecha_motivo = form_basic.cleaned_data["mostrar_motivos_no_activo"]
			agregar_nro_estudiante = form_basic.cleaned_data["agregar_nro_estudiante"]
			numerar_casillas_blanco = form_basic.cleaned_data["numerar_casillas_blanco"]
			self.mostrar_primero_nombre = form_basic.cleaned_data["mostrar_primero_nombre"]
			self.mostrar_nombre_separado = form_basic.cleaned_data["mostrar_nombre_separado"]

			columnas_blanco = form_basic.cleaned_data["columnas_blanco"]
			filas_blanco = form_basic.cleaned_data["filas_blanco"]
			if columnas_blanco == "":
				columnas_blanco = 0
			if filas_blanco == "":
				filas_blanco = 0

			orden = []
			dividir_grupos = form_filtros.cleaned_data["dividir_grupos"]
			orden.append('-grupo__grado__valor_numerico')
			ordenar_por = form_basic.cleaned_data["ordenar_por"]
			orden.append(ordenar_por)

			texto_adicional = form_exportar.cleaned_data["texto_adicional"]
			campos = form_exportar.cleaned_data["campos"]
			formato = form_exportar.cleaned_data["formato"]

			estudiante = Estudiante.objects.filter(filT).order_by(*orden)
			pages = []

			header = []
			if agregar_nro_estudiante:
				header.append({"column" : "Nro.", "min" : True})

			for cam in campos:
				header.extend(self._formatHeader(cam))

			for nro in range(int(columnas_blanco)):
				if numerar_casillas_blanco:
					header.append({"column": str(nro + 1), "medium" : True})
				else:
					header.append({"column": "", "medium" : True})

			nro = 1
			tables = []
			last_grupo = None
			for est in estudiante:
				if last_grupo != est.grupo and dividir_grupos and last_grupo != None:
					for nro_fila in range(int(filas_blanco)):
						data_blank_fila = [""]*len(header)
						data_blank_fila[0] = nro
						nro += 1
						tables.append(data_blank_fila)
					pages.append(dict(tabla = tables, title = str(last_grupo)))
					tables = []
					nro = 1
				data = [str(nro)]
				nro += 1
				for c in campos:
					try:
						first, second = c.split("__")
						if second == "format":
							data.extend(self._formatData(est, first))
						else:
							value = ""
							if hasattr(est, first):
								value = str(getattr(getattr(est, first), second)).replace("True", "Si").replace("False", "No").replace("None", "")
							data.append(value)
					except Exception as e:
						print(e)
						if hasattr(est, c):
							data.append(str(getattr(est, c)).replace("True", "Si").replace("False", "No").replace("None", ""))
						else:
							data.append("")
				for nro_col in range(int(columnas_blanco)):
					data.append("")
				tables.append(data)
				last_grupo = est.grupo

			for nro_fila in range(int(filas_blanco)):
				data_blank_fila = [""]*len(header)
				data_blank_fila[0] = nro
				nro += 1
				tables.append(data_blank_fila)

			if dividir_grupos:
				pages.append(dict(tabla=tables, title = str(last_grupo)))
			else:
				pages.append(dict(tabla=tables, title = "Todos los estudiantes"))

			if formato == "pdf":
				template_path = 'pdf/listado.html'
				return show_pdf(dict(all_pages = pages, thead = header, cabecera = cabecera, escudo = escudo, texto_adicional = texto_adicional), template_path, request.build_absolute_uri())
			else:
				fill = PatternFill("solid", fgColor='B6B6B6')
				thin = Side(border_style="thin", color="000000")
				border = Border(top=thin, left=thin, right=thin, bottom=thin)

				wb = Workbook()
				sheet = wb.active
				sheet.title = "Listado"

				row_num = 1
				for page in pages:
					sheet.cell(row = row_num, column = 1).value = page["title"]
					sheet.cell(row = row_num, column = 1).fill = fill
					sheet.cell(row = row_num, column = 1).border = border
					sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=len(header))
					row_num += 1
					for col_num in range(len(header)):
						sheet.cell(row = row_num, column = col_num + 1).value = header[col_num]["column"]
						sheet.cell(row = row_num, column = col_num + 1).fill = fill
						sheet.cell(row = row_num, column = col_num + 1).border = border
					row_num += 1

					for tabla in page["tabla"]:
						for col_num in range(len(tabla)):
							sheet.cell(row = row_num, column = col_num + 1).value = tabla[col_num]
							sheet.cell(row = row_num, column = col_num + 1).border = border
						row_num += 1
				response = HttpResponse(content=save_virtual_workbook(wb), content_type='application/ms-excel')
				response['Content-Disposition'] = 'attachment; filename=ListadoEstudiantes.xlsx'
			return response

		return show_error()

	def _formatData(self, obj, concept):
		if concept == "apellido_nombre":
			adicional = ""
			if self.mostrar_fecha_motivo and obj.estado != Estudiante.E_ACTIVO:
				adicional = " (" + str(obj.fecha_retiro) + ") " + str(obj.observaciones)
			if self.mostrar_nombre_separado and self.mostrar_primero_nombre:
				return [obj.nombre, obj.apellido + adicional]
			elif self.mostrar_nombre_separado and not self.mostrar_primero_nombre:
				return [obj.apellido, obj.nombre + adicional]
			elif not self.mostrar_nombre_separado and self.mostrar_primero_nombre:
				return [obj.nombre + " " + obj.apellido + adicional]
			else:
				return [obj.apellido + " " + obj.nombre + adicional]
			return obj.nombre, obj.apellido
		elif concept == "limitaciones":
			salud = obj.getSalud()
			return [salud.getLimitacionesTxt()]
		elif concept == "estado":
			return [obj.getEstado()]
		elif concept == "zona":
			return [obj.getZona()]
		elif concept == "sexo":
			return [obj.getSezo()]
		return [""]

	def _formatHeader(self, concept):
		concept = concept.replace("__format", "").replace("salud__", "").replace("adicional__", "").replace("acudiente__", "(ac)")
		if concept == "apellido_nombre":
			if self.mostrar_nombre_separado and self.mostrar_primero_nombre:
				return [{"column" : "Nombre"}, {"column": "Apellido"}]
			elif self.mostrar_nombre_separado and not self.mostrar_primero_nombre:
				return [{"column" : "Apellido"}, {"column": "Nombre"}]
			elif not self.mostrar_nombre_separado and self.mostrar_primero_nombre:
				return [{"column" : "Nombre y apellido"}]
			else:
				return [{"column" : "Apellido y nombre"}]
		elif concept == "pk":
			return [{"column": "PK", "min": True}]
		elif concept == "tipo_documento":
			return [{"column": "Tipo Doc", "min": True}]


		return [{"column":concept.replace("_", " ").capitalize(), "width":"auto"}]
		"""response = HttpResponse(content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="users.xls"'

		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet('Users')

		# Sheet header, first row
		row_num = 0

		font_style = xlwt.XFStyle()
		font_style.font.bold = True

		columns = ['Username', 'First name', 'Last name', 'Email address', ]

		for col_num in range(len(columns)):
			ws.write(row_num, col_num, columns[col_num], font_style)

		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()

		rows = User.objects.all().values_list('username', 'first_name', 'last_name', 'email')
		for row in rows:
			row_num += 1
			for col_num in range(len(row)):
				ws.write(row_num, col_num, row[col_num], font_style)

		wb.save(response)
		return response"""


class GenerarInformes(Main, View):
	template = "form.html"
	page_title = "Generar informes"

	def _preProccess(self, request):
		if self.user.es_estudiante():
			self.can_view_page = False

	def get_data(self, request, kwargs):

		forms = []
		forms.append(dict(href="first", tab="Opciones básicas", form = formInformes(user = self.user, year = self.year)))
		forms.append(dict(href="three", tab="Impresión", form=ImpresionFormato()))
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Generar informes", "")

		# Data to return
		data_return = dict()
		data_return["forms"] = forms
		data_return["target"] = "target='_blank'"
		data_return["tipo_form"] = "informeForm"
		data_return["form_action"] = "/estudiantes/informes"

		return data_return

	def _proccessInforme(self, request):
		# TODO: añadir fecha retiro y eso
		data = getImpresionOptions(request)
		aditional_header = "?cabecera_actual=" + str(data["cabecera_actual"]) + "&size=" + str(data["size"]) + "&orientacion=" + str(data["orientacion"]) + "&margen_horizontal=" + str(data["horizontal"]) + "&margen_vertical=" + str(data["vertical"])
		form_basic = formInformes(request.POST, user = self.user, year = self.year)
		if form_basic.is_valid():
			periodo = form_basic.cleaned_data["periodo"]
			grupo = form_basic.cleaned_data["grupo"]
			informe = form_basic.cleaned_data["informes"]
			if str(informe) == str(formInformes.BOLETIN):
				return HttpResponseRedirect("/pdf/boletines/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.COLSOLIDADO):
				return HttpResponseRedirect("/pdf/consolidado_grupo/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.AREAS_PENDIENTES):
				return HttpResponseRedirect("/pdf/consolidado_grupo/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header + "&areas_pendientes" )

			elif str(informe) == str(formInformes.PROMEDIO_ASIGNATURAS):
				return HttpResponseRedirect("/pdf/promedio_asignaturas/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.PROMEDIO_DOCENTES):
				return HttpResponseRedirect("/pdf/promedio_docentes/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)

			elif str(informe) == str(formInformes.MEJORES_PROMEDIOS_SEDES):
				return HttpResponseRedirect("/pdf/mejores_promedios_sedes/" + str(grupo.sede.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.MEJORES_PROMEDIOS_GRUPOS):
				return HttpResponseRedirect("/pdf/mejores_promedios_grupos/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.LISTADO_PROMEDIOS):
				return HttpResponseRedirect("/pdf/listado_promedios/" + str(grupo.pk) + "/" + str(periodo.pk) + "/" + str(self.year) + aditional_header)
			elif str(informe) == str(formInformes.ASISTENCIA):
				return HttpResponseRedirect("/pdf/asistencia/" + str(grupo.pk) + "/" + str(self.year) + aditional_header + "&all_grupos")
			return HttpResponseRedirect("/index")


class AgregarExcel(Main, View):
	template = "student/agregar_excel.html"
	page_title = "Agregar estudiantes por excel"

	def _preProccess(self, request):
		if not self.user.es_admin():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Agregar estudiantes por excel", "")

		#list = []
		#import collections
		#print([item for item, count in collections.Counter(list).items() if count > 1])

		data_return = dict()

		file = request.GET.get("file", None)
		preview = request.GET.get("preview", None)
		add_final = request.GET.get("add_final", None)

		data_return["file_name"] = file
		file_exists = False
		if file != None and preview != None:
			preview = True
			file_path = os.path.join(settings.MEDIA_ROOT, "excel_student", str(file))
			if default_storage.exists(file_path):
				file_exists = True

				wb = load_workbook(file_path)
				worksheet = wb["Sheet1"]
				excel_data = list()
				column_index = 0
				headers = []
				for row in worksheet.iter_cols():
					row_data = {}
					index = 0
					for cell in row:
						if column_index == 0:
							headers.append(str(cell.value))
						else:
							if cell.value != None:
								if index < len(headers):
									row_data[headers[index]] = excel_add_student(str(cell.value), headers[index])
									#row_data[headers[index]] = str(cell.value)
							else:
								row_data[headers[index]] = ""

						index += 1
					if column_index > 0:
						if len(row_data) > 0:
							excel_data.append(row_data)
					column_index += 1

				id_added = []
				if add_final != None:
					for data in excel_data:
						if "documento" in data:
							permission_add = True
							can_save = True
							added_user = False
							if data["documento"] == "":
								permission_add = False
							else:
								last_obj = Estudiante.objects.filter(Q(documento = data["documento"]) | (Q(nombre = data["nombre"]) & Q(apellido = data["apellido"])))
								for obj in last_obj:
									if str(obj.year) == str(self.year):
										permission_add = False
								if len(last_obj) >= 1:
									user = last_obj[0].user
								if len(last_obj) == 0:
									if User.objects.filter(Q(username = data["documento"])).count() == 1:
										user = User.objects.filter(Q(username = data["documento"]))[0]
									else:
										user = User.objects.create_user(username = data["documento"], password = data["documento"])
										added_user = True
							if permission_add:
								estudiante = Estudiante(user = user, year = self.year)
								for head in headers:
									if head != "limitaciones" and not "acudiente" in head and "__salud" not in head and "__adicional" not in head and head != "grupo" and head != "fecha_retiro" and head != "tipo_documento" and head != "sexo" and head != "fecha_nacimiento" and head != "celular1":
										if data[head] == "No":
											data[head] = False
										elif data[head] == "Si":
											data[head] = True
										setattr(estudiante, head, data[head])

								if "sexo" in data:
									sexo = data["sexo"]
									if len(sexo) > 1:
										sexo = sexo[0]
									estudiante.sexo = sexo

								if "celular1" in data:
									celulares = data["celular1"].split("-")
									if len(celulares) > 1:
										estudiante.celular1 = celulares[0][0:10]
										estudiante.celular2 = celulares[1][0:10]
									else:
										estudiante.celular1 = data["celular1"][0:10]

								if "tipo_documento" in data:
									tipo_documento = data["tipo_documento"]
									if tipo_documento == "TarjetaIdentidad":
										estudiante.tipo_documento = "TI"
									elif tipo_documento == "CedulaCiudadania":
										estudiante.tipo_documento = "CC"
									elif tipo_documento == "RegistroCivil":
										estudiante.tipo_documento = "RC"
									elif tipo_documento == "NUIP":
										estudiante.tipo_documento = "NU"
									elif tipo_documento == "NES":
										estudiante.tipo_documento = "NE"
									elif tipo_documento == "CedulaExtrangeria":
										estudiante.tipo_documento = "CE"
									else:
										estudiante.tipo_documento = tipo_documento

								if "fecha_retiro" in data:
									if data["fecha_retiro"] != "" and data["fecha_retiro"] != "0000-00-00":
										estudiante.fecha_retiro = data["fecha_retiro"]

								if "fecha_nacimiento" in data:
									if data["fecha_nacimiento"] != "" and data["fecha_nacimiento"] != "0000-00-00":
										estudiante.fecha_nacimiento = data["fecha_nacimiento"]

								if "grupo" in data:
									if type(data["grupo"]) == Grupo:
										estudiante.grupo = data["grupo"]
									else:
										grupo_title = data["grupo"].split(" - ")
										if len(grupo_title) > 0:
											sede_title = grupo_title[-1]
											grupo_title = grupo_title[:-1][0]
											grupo = Grupo.objects.filter(Q(titulo = grupo_title) & Q(sede__titulo = sede_title))
											if len(grupo) == 0:
												can_save = False
											else:
												estudiante.grupo = grupo[0]
								if can_save:
									estudiante.save()

									# Acudiente datos
									if "identificacion__acudiente" in data:
										try:
											acudiente = Acudiente.objects.get(Q(identificacion = data["identificacion__acudiente"]) & Q(year = self.year))
										except Exception as e:
											acudiente = Acudiente(identificacion = data["identificacion__acudiente"], year = self.year)

									for head in headers:
										if "acudiente" in head:
											if head != "cabeza_familia__acudiente" and head != "identificacion__acudiente" and head != "celular1__acudiente":
												setattr(acudiente, head.replace("__acudiente", ""), data[head])

									if "celular1__acudiente" in data:
										celulares = data["celular1__acudiente"].split("-")
										if len(celulares) > 1:
											acudiente.celular1 = celulares[0][0:10]
											acudiente.celular2 = celulares[1][0:10]
										else:
											acudiente.celular1 = data["celular1__acudiente"][0:10]

									if "cabeza_familia__acudiente" in data:
										if data["cabeza_familia__acudiente"] != "":
											acudiente.cabeza_familia = data["cabeza_familia__acudiente"]
									acudiente.save()

									acudiente_estudiante = EstudianteAcudiente(estudiante = estudiante, acudiente = acudiente)
									acudiente_estudiante.save()

									# Salud Datos
									if "limitaciones__salud" in data:
										for limi in data["limitaciones__salud"].split(","):
											try:
												limitacion = Limitacion.objects.get(nombre = limi)
												new_limi = LimitacionEstudiante(estudiante = estudiante, limitacion = limitacion)
												new_limi.save()
											except Exception as e:
												pass

									salud = EstudianteSalud(estudiante = estudiante)
									for head in headers:
										if "salud" in head:
											if head != "limitaciones__salud" and head != "tipo_sangre__salud":
												setattr(salud, head.replace("__salud", ""), data[head])

									if "tipo_sangre__salud" in data:
										salud.tipo_sangre = data["tipo_sangre__salud"].replace("P", "+").replace("N", "-")
									salud.save()

									# Adicional Datos
									adic = EstudianteAdicional(estudiante = estudiante)

									for head in headers:
										if "adicional" in head:
											if data[head] == "No":
												data[head] = False
											elif data[head] == "Si":
												data[head] = True
											if data[head] != "" and head != "sabe_nadar__adicional" and head != "dificultades_aprendizaje__adicional":
												setattr(adic, head.replace("__adicional", ""), data[head])

									if "sabe_nadar__adicional" in data:
										if data["sabe_nadar__adicional"] == "No":
											data["sabe_nadar__adicional"] = False
										elif data["sabe_nadar__adicional"] == "Si":
											data["sabe_nadar__adicional"] = True
										if data["sabe_nadar__adicional"] != "":
											adic.sabe_nadar = data["sabe_nadar__adicional"]

									if "dificultades_aprendizaje__adicional" in data:
										if data["dificultades_aprendizaje__adicional"] == "No":
											data["dificultades_aprendizaje__adicional"] = False
										elif data["dificultades_aprendizaje__adicional"] == "Si":
											data["dificultades_aprendizaje__adicional"] = True
										if data["dificultades_aprendizaje__adicional"] != "":
											adic.dificultades_aprendizaje = data["dificultades_aprendizaje__adicional"]

									adic.save()
									id_added.append(str(estudiante.pk))
								else:
									if added_user:
										user.delete()

					if len(id_added) > 0:
						file_register = os.path.join(settings.MEDIA_ROOT, "log", "excel_estudiantes.txt")
						if not default_storage.exists(file_path):
							with open(file_register, "w+") as file:
								file.close()
						with open(file_register, "r") as file:
							old_data = file.read()
							file.close()
						with open(file_register, "w") as file:
							today = datetime.now()
							file.write(str(old_data) + "\n" + str(today.strftime("%B %d, %Y %H:%M:%S")) + "| " + ", ".join(id_added))
							file.close()

					data_return["normal_response"] = False
					data_return["return_response"] = HttpResponseRedirect("/estudiantes/AgregarExcel")

					return data_return
					#default_storage.delete(file_path)
				data_return["excel_data"] = excel_data
				data_return["headers"] = headers

		form_file = formUploadExcel()
		data_return["form_file"] = form_file
		data_return["preview"] = preview
		data_return["file_exists"] = file_exists
		return data_return

	def _addData(self, request):
		if request.user.es_admin():
			form_add = formUploadExcel(files = request.FILES, data = request.POST)
			if form_add.is_valid():
				archivo = request.FILES['archivo']
				file = save_file(archivo, "excel_student")
				return HttpResponseRedirect('/estudiantes/AgregarExcel?preview&file=' + str(file))
			else:
				self._add_error(form_add.errors)
		return False


class AsistenciaDiaria(Main, View):
	template = "student/asistencia.html"
	page_title = "Asistencia diaria"

	def _preProccess(self, request):
		if not self.user.es_admin():
			self.can_view_page = False

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Asistencia diaria", "")
		data_return = dict()

		if self.user.es_estudiante():
			self.can_view_page = False
			return {}

		today = date.today()
		data_return["form_asistencia"] = asistenciaSelect(user = self.user, year = self.year)
		data_return["form_asistencia"]["fecha"].initial = today.strftime("%Y-%m-%d")

		grupo_id = request.GET.get("grupo", None)
		fecha = request.GET.get("fecha", None)
		if fecha != None:
			data_return["form_asistencia"]["fecha"].initial = fecha
		if grupo_id != None:
			data_return["form_asistencia"]["grupo"].initial = grupo_id

		if fecha != None and grupo_id != None:
			grupo = Grupo.objects.get(Q(pk = grupo_id))
			can_continue = True
			if self.user.es_docente():
				can_continue = self.user.docente().canAdminGroup(grupo, self.year)

			lista_estudiantes = []

			if can_continue:
				nro = 1
				for estudiante in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year)).order_by("apellido"):
					try:
						asis = Inasistencia.objects.get(Q(estudiante = estudiante) & Q(fecha = fecha))
						horas = asis.horas
						excusa = asis.excusa
						if horas == 1:
							concepto = "Retardo"
						else:
							concepto = "Inasistencia"
					except:
						horas = 0
						excusa = False
						concepto = "Sin registro"

					lista_estudiantes.append({
						"nro" : nro,
						"pk" : estudiante.pk,
						"estudiante" : str(estudiante.apellido) + " " + str(estudiante.nombre),
						"horas" : horas,
						"excusa" : excusa,
						"concepto" : concepto
					})
					nro += 1

			data_return["can_continue"] = can_continue
			data_return["lista_estudiantes"] = lista_estudiantes
			data_return["grupo"] = {
				"pk" : grupo.pk,
				"titulo" : grupo.getGrupo()
			}
			data_return["fecha"] = fecha

			self._add_js("asistencia.js")

		return data_return

	def _ajaxRequest(self, request):
		concept = {"concept" : "error"}
		ajax_action = request.POST.get("ajax_action", None)
		if ajax_action == "editAsistencia":
			horas = request.POST.get("horas", None)
			excusa = request.POST.get("excusa", None)
			estudiante_id = request.POST.get("estudiante_id", None)
			fecha = request.POST.get("fecha", None)
			if horas != None and excusa != None and estudiante_id != None and fecha != None:
				estudiante = Estudiante.objects.get(Q(pk = estudiante_id) & Q(year = self.year))
				try:
					asistencia = Inasistencia.objects.get(Q(fecha = fecha) & Q(estudiante = estudiante))
				except:
					asistencia = Inasistencia(fecha = fecha, estudiante = estudiante)
				if int(horas) == 0:
					asistencia.delete()
				else:
					asistencia.horas = int(horas)
					asistencia.excusa = bool(excusa)
					asistencia.save()
				concept = {"concept" : "success"}

		return JsonResponse(concept)
