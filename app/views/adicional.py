# Own libraries
from app.views.main import Main, MainTable
from app.models import *
from app.forms.adicional import *
from app.utils.funciones import *

# Django libraries
from django.views import View
from django.db.models import Q
from django.http import JsonResponse
from django.core import serializers
from django.utils.translation import ugettext_lazy as _


class NoticiasBlogView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Noticias del blog")
	register_name = _("noticia del blog")
	form_action = "/adicional/noticias/"
	model_object = NoticiasBlog
	table_columns =	dict(titulo = "Título", imagen = "Imagen", fecha_publicacion__date_format = "Fecha de Publicación")
	return_edit_columns = ["cuerpo", "titulo", "imagen"]
	form_edit = editNoticiasBlog
	form_add = editNoticiasBlog
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Noticias del blog"), "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(grupo__titulo__icontains=value) | Q(docente__nombre__icontains = value) | Q(docente__apellido__icontains = value) | Q(fecha_realizacion__icontains = value) | Q(titulo__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self._can_view_page = False


class IcfesView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = _("Preguntas Icfes")
	register_name = _("pregunta")
	form_action = "/adicional/icfes/"
	model_object = Icfes
	table_columns =	dict(cuerpo = "Cuerpo", A = "Resp. A", B = "Resp. B", C = "Resp. C", D = "Resp. D", area = "Área")
	return_edit_columns = []
	form_edit = editIcfes
	form_add = editIcfes
	delete_register = True
	docente_add_delete = True

	edit_aparte = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Preguntas Icfes"), "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(cuerpo__icontains = value) | Q(A__icontains = value) | Q(B__icontains=value) | Q(C__icontains = value) | Q(D__icontains = value) | Q(area__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if not self.user.es_estudiante():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self._can_view_page = False


class Dane(Main, View):
	template = "adicional/dane.html"
	page_title = "Dane"

	def getSedes(self):
		periodo_final = Periodo.objects.filter(Q(year = self.year) & Q(es_final = True))
		if len(periodo_final) == 0:
			self._can_view_page = False
			return {}

		sedes_all = []
		periodo_final = periodo_final[0]
		for sede in Sede.objects.all():
			grados_internal = []
			for grado in Grado.objects.all():
				grados_internal.append({
					"pk" : grado.pk,
					"titulo" : grado.titulo,
					"total" : 0,
					"masc" : {
						"Aprobados" : 0,
						"Reprobados" : 0,
						"Inactivos" : 0,
						"Retirados" : 0,
						"Desertores" : 0,
						"Cancelados" : 0
					},
					"fem" : {
						"Aprobados" : 0,
						"Reprobados" : 0,
						"Inactivos" : 0,
						"Retirados" : 0,
						"Desertores" : 0,
						"Cancelados" : 0
					}
				})
				estudiantes = Estudiante.objects.filter(Q(year = self.year) & Q(grupo__grado = grado) & Q(grupo__sede = sede))
				for est in estudiantes:
					motivo = "Aprobados"
					sexo = None
					if est.sexo == Estudiante.MASC:
						sexo = "masc"
					elif est.sexo == Estudiante.FEM:
						sexo = "fem"
					if sexo != None:
						if est.estado == Estudiante.E_ACTIVO:
							#if periodo_final.isActive():
							#	notas = est.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo_final.orden, min_lost = self.min_lost)
							#	perdidas = [comp for comp in notas["areas"] if comp["perdida"] == True]
							#	perdidas2 = [comp for comp in notas["asignaturas"] if comp["perdida"] == True]
							#	perdidas = len(perdidas) + len(perdidas2)
							#	if perdidas >= self.perdidas_pei:
							#		motivo = "Reprobados"
							if est.aprobado == False:
								motivo = "Reprobado"
							grados_internal[-1][sexo][motivo] += 1
						if est.estado == Estudiante.E_INACTIVO:
							grados_internal[-1][sexo]["Inactivos"] += 1
						if est.estado == Estudiante.E_RETIRADO:
							grados_internal[-1][sexo]["Retirados"] += 1
						if est.estado == Estudiante.E_DESERTOR:
							grados_internal[-1][sexo]["Desertores"] += 1
						if est.estado == Estudiante.E_CANCELADO:
							grados_internal[-1][sexo]["Cancelados"] += 1
						grados_internal[-1]["total"] += 1

			sedes_all.append({
				"pk" : sede.pk,
				"titulo" : sede.titulo,
				"grados" : grados_internal,
				"total" : sum([comp["total"] for comp in grados_internal]),
				"masc" : {
					"Aprobados" : sum([comp["masc"]["Aprobados"] for comp in grados_internal]),
					"Reprobados" : sum([comp["masc"]["Reprobados"] for comp in grados_internal]),
					"Inactivos" : sum([comp["masc"]["Inactivos"] for comp in grados_internal]),
					"Retirados" : sum([comp["masc"]["Retirados"] for comp in grados_internal]),
					"Desertores" : sum([comp["masc"]["Desertores"] for comp in grados_internal]),
					"Cancelados" : sum([comp["masc"]["Cancelados"] for comp in grados_internal])
				},
				"fem" : {
					"Aprobados" : sum([comp["fem"]["Aprobados"] for comp in grados_internal]),
					"Reprobados" : sum([comp["fem"]["Reprobados"] for comp in grados_internal]),
					"Inactivos" : sum([comp["fem"]["Inactivos"] for comp in grados_internal]),
					"Retirados" : sum([comp["fem"]["Retirados"] for comp in grados_internal]),
					"Desertores" : sum([comp["fem"]["Desertores"] for comp in grados_internal]),
					"Cancelados" : sum([comp["fem"]["Cancelados"] for comp in grados_internal])
				}
			})

			for grado in grados_internal:# + sedes_all:
				for gen in ["masc", "fem"]:
					for concepto in ["Aprobados", "Reprobados", "Inactivos", "Retirados", "Desertores", "Cancelados"]:
						val = grado[gen][concepto]
						if grado["total"] == 0:
							grado["total"] = 1
						grado[gen][concepto] = str(val) + " (" + str(int(int(val) * 100 / int(grado["total"]))) + "%)"

			sedes_all[-1]["grados"] = grados_internal

		return sedes_all

	def get_data(self, request, kwargs):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Dane", "")

		sedes_all = self.getSedes()

		data_return = {
			"sedes_all" : sedes_all
		}

		return data_return


class CalendarioView(Main, View):
	template = "adicional/calendario.html"
	page_title = "Calendario"
	form_action = "/adicional/calendario/"

	def get_data(self, request, kwargs):

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Calendario", "")

		self._add_css("calendario/fullcalendar.css")
		self._add_css("calendario/fullcalendar.print.css")
		self._add_js("calendario/jquery-ui.custom.min.js")
		self._add_js("calendario/fullcalendar.js")
		if self.user.es_admin():
			self._add_js("calendario_handler.js")
			self._add_aditional_template("varios/dialogo_agregar.html")
			self._add_aditional_template("varios/dialogo_editar.html")
			self._add_aditional_template("varios/dialogo_eliminar.html")
		else:
			self._add_js("calendario_handler_noAdmin.js")

		data_return = dict()

		data_return["form_add"] = editCalendar()
		data_return["form_edit"] = editCalendar()
		data_return["form_action"] = "/adicional/calendario/"
		data_return["delete_register"] = True

		return data_return

	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		if self.user.es_admin:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				#if self._checkPermission(object_id, user):
				sed = Calendario.objects.get(pk = object_id)
				sed.delete()
				return True
				self._add_error("No se pudo eliminar el objeto")
			else:
				self._add_error("Usted no tiene permisos para eliminar este objeto")
		return False


	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.user.es_admin:
			form_add = editCalendar(data = request.POST)
			if form_add.is_valid():
				form_add.save()
				return [True, form_add.cleaned_data["descripcion"]]
			self._add_error(form_add.errors)
		return False

	def _editData(self, request):
		"""
			edit a table register
		"""
		if self.user.es_admin:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				sed = Calendario.objects.get(pk = object_id)
				tipoEdit = request.POST.get("tipoEdit", None)
				if tipoEdit == "only_date":
					fecha_inicio = request.POST.get("fecha_inicio", None)
					fecha_fin = request.POST.get("fecha_fin", None)
					sed.fecha_inicio = fecha_inicio
					sed.fecha_fin = fecha_fin
					sed.save()
					return True
				else:
					form_edit = editCalendar(data = request.POST, instance=sed)
					if form_edit.is_valid():
						form_edit.save()
						return True

					self._add_error(form_edit.errors)
		return False

	def _requestData(self, request):
		date_start = request.POST.get("start", 0)
		date_end = request.POST.get("end", 0)
		dates = []
		from datetime import datetime
		fecha_desde = datetime.utcfromtimestamp(int(date_start)).strftime('%Y-%m-%d')
		fecha_hasta = datetime.utcfromtimestamp(int(date_end)).strftime('%Y-%m-%d')

		dates = []
		for d in Calendario.objects.filter(Q(fecha_inicio__range = (fecha_desde, fecha_hasta)) | Q(fecha_fin__range = (fecha_desde, fecha_hasta))):
			dates.append({
				"allDay": True,
				"title": d.descripcion,
				"start": d.fecha_inicio,
				"end": d.fecha_fin,
				"id": d.pk
			})

		return JsonResponse(dates, safe=False)


	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		obj_id = 0

		if not self.user.is_anonymous and self.user.es_admin:
			if action == "editar_modal":
				data = self._editData(request)
			if action == "agregar_modal":
				data = self._addData(request)
			if action == "eliminar_modal":
				data = self._deleteData(request)

		if action == "request_data":
			return self._requestData(request)

		if type(data) is list or type(data) is tuple:
			error = not(data[0])
			obj_id = data[1]
		else:
			error = not(data)
		print(self.errors)
		data = {
			"errores" : ", ".join(self.errors),
			"error": error,
			"obj_id" : obj_id
		}
		return JsonResponse(data)


class SalidasEscolares(MainTable, View):
	"""
		view of Asignación Docente
	"""
	#TODO: Añadir botón para vista previa
	page_title = "Salidas escolares"
	register_name = "registro"
	form_action = "/adicional/salidas_escolares/"
	model_object = Salida
	table_columns =	dict(archivo = "Archivo", fecha_modificacion__date_format = "Fecha de modificación", year = "Año", observaciones = "Observaciones")
	return_edit_columns = []
	form_edit = editSalidaEscolar
	form_add = editSalidaEscolar
	delete_register = True
	docente_add_delete = True

	edit_aparte = True
	#view_aparte = True
	#can_view = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Salidas escolares", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(observaciones__icontains = value) | Q(fecha_modificacion__icontains = value))
		return fil

	def _preProccess(self, request):
		user = request.user
		if self.user.es_admin() or self.user.es_docente():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class EstadisticasView(Main, View):
	template = "adicional/estadisticas.html"
	page_title = "Estadísticas"
	form_action = "/adicional/estadisticas/"

	def get_data(self, request, kwargs):

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Estadísticas", "")
		#self._add_css("estadisticas/estadisticas.css")
		#self._add_js("estadisticas/estadisticas.js")
		data_return = dict()
		data_return["form_estadisticas"] = formEstadisticas(user = self.user, year = self.year)
		tipo = request.GET.get("tipo", None)
		periodo_id = request.GET.get("periodo", None)
		grupo_id = request.GET.get("grupo", None)

		all_categorias = []
		all_series = []
		if tipo != None:
			periodo = None
			grupo = None
			if periodo_id != None:
				data_return["form_estadisticas"]["tipo"].initial = tipo
				data_return["form_estadisticas"]["periodo"].initial = periodo_id
				data_return["form_estadisticas"]["grupo"].initial = grupo_id
				try:
					periodo = Periodo.objects.get(Q(pk = periodo_id) & Q(year = self.year))
				except Exception as e:
					print(e)
					pass
			if grupo_id != None:
				try:
					grupo = Grupo.objects.get(Q(pk = grupo_id))
				except:
					pass
			grafica_titulo = ""
			tipo = int(tipo)
			if tipo == formEstadisticas.PROMEDIOS_BAJOS_GRADOS:
				grafica_titulo = "Promedios bajos por grados"
				if periodo != None:
					grafica_titulo += " en " + str(periodo.titulo)
					for grado in Grado.objects.all():
						nro_promedio_bajo = 0
						for st in Estudiante.objects.filter(Q(grupo__grado = grado) & Q(year = self.year)):
							notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
							other_asignaturas = []
							periodo_asignatura = next((comp for comp in notas["promedios"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != None:
								if periodo_asignatura["perdida"]:
									nro_promedio_bajo += 1
						all_series.append({
							"titulo" : grado.titulo,
							"data" : [nro_promedio_bajo]
						})
					all_categorias.append(periodo.titulo)
			elif tipo == formEstadisticas.PROMEDIOS_BAJOS_GRUPOS:
				grafica_titulo = "Promedios bajos por grupos "
				if periodo != None:
					grafica_titulo += " en " + str(periodo.titulo)
					for grupo in Grupo.objects.all():
						nro_promedio_bajo = 0
						for st in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
							notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
							other_asignaturas = []
							periodo_asignatura = next((comp for comp in notas["promedios"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != None:
								if periodo_asignatura["perdida"]:
									nro_promedio_bajo += 1
						all_series.append({
							"titulo" : grupo.getGrupo(),
							"data" : [nro_promedio_bajo]
						})
					all_categorias.append(periodo.titulo)
			elif tipo == formEstadisticas.DESEMPLENO_SEDE_PERIODO:
				grafica_titulo = "Desempeño de las sedes"
				if periodo != None:
					grafica_titulo += " en " + str(periodo.titulo)
					for sede in Sede.objects.all():
						valoraciones = {}
						for valoracion in Valoracion.objects.all().order_by("min"):
							valoraciones[valoracion.titulo] = 0
						nro_superior = nro_alto = nro_medio = nro_bajo = 0
						for st in Estudiante.objects.filter(Q(grupo__sede = sede) & Q(year = self.year)):
							notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
							other_asignaturas = []
							periodo_asignatura = next((comp for comp in notas["promedios"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != None:
								if periodo_asignatura["valoracion"] != "-":
									valoraciones[periodo_asignatura["valoracion"]] += 1
						all_series.append({
							"titulo" : sede.titulo,
							"data" : [value for key, value in valoraciones.items()]
						})
				all_categorias = [valoracion.nombre for valoracion in Valoracion.objects.all().order_by("min")]

			elif tipo == formEstadisticas.DESEMPENO_ASIGNATURA:
				grafica_titulo = "Desempeño de las asignaturas "
				if periodo != None and grupo != None:
					grafica_titulo += " en " + str(periodo.titulo) + ", grupo: " + str(grupo.getGrupo())
					all_asignaturas = []
					for asignacion in AsignacionDocente.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
						asignatura_data = {
							"pk" : asignacion.asignatura.pk,
							"titulo" : asignacion.asignatura.titulo
						}
						for valoracion in Valoracion.objects.all().order_by("min"):
							asignatura_data[valoracion.titulo] = 0
						all_asignaturas.append(asignatura_data)

					valoraciones = {}
					for valoracion in Valoracion.objects.all().order_by("min"):
						valoraciones[valoracion.titulo] = 0
					nro_superior = nro_alto = nro_medio = nro_bajo = 0
					for st in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
						notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
						other_asignaturas = []
						[other_asignaturas.extend(area["asignaturas"]) for area in notas["areas"]]
						for asignatura in notas["asignaturas"] + other_asignaturas:
							periodo_asignatura = next((comp for comp in asignatura["periodos"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != False:
								if periodo_asignatura["valoracion"] != "-":
									asig_obj = next((comp for comp in all_asignaturas if comp["pk"] == asignatura["pk"]), False)
									if asig_obj != False:
										asig_obj[periodo_asignatura["valoracion"]] += 1
					for asignatura in all_asignaturas:
						all_series.append({
							"titulo" : asignatura["titulo"],
							"data" : [asignatura[valoracion.titulo] for valoracion in Valoracion.objects.all().order_by("min")]
						})
				all_categorias = [valoracion.nombre for valoracion in Valoracion.objects.all().order_by("min")]

			elif tipo == formEstadisticas.PROMEDIO_ASIGNATURAS_GRUPOS:
				grafica_titulo = "Promedios de las asignaturas "
				if periodo != None and grupo != None:
					grafica_titulo += " en " + str(periodo.titulo) + ", grupo: " + str(grupo.getGrupo())
					all_asignaturas = []
					for asignacion in AsignacionDocente.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
						asignatura_data = {
							"pk" : asignacion.asignatura.pk,
							"titulo" : asignacion.asignatura.titulo,
							"promedio" : 0
						}
						all_asignaturas.append(asignatura_data)
					nro_promedio = 0
					for st in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
						nro_promedio += 1
						notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
						other_asignaturas = []
						[other_asignaturas.extend(area["asignaturas"]) for area in notas["areas"]]
						for asignatura in notas["asignaturas"] + other_asignaturas:
							periodo_asignatura = next((comp for comp in asignatura["periodos"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != False:
								if periodo_asignatura["valoracion"] != "-":
									asig_obj = next((comp for comp in all_asignaturas if comp["pk"] == asignatura["pk"]), False)
									if asig_obj != False:
										asig_obj["promedio"] += periodo_asignatura["valor"]
					if nro_promedio == 0:
						nro_promedio = 1
					for asignatura in all_asignaturas:
						all_series.append({
							"titulo" : asignatura["titulo"],
							"data" : [asignatura["promedio"] / nro_promedio]
						})
				all_categorias = ["promedio"]

			elif tipo == formEstadisticas.PROMEDIOS_GRUPO:
				grafica_titulo = "Promedios de los grupos en el periodo"
				if periodo != None:
					grafica_titulo += " en " + str(periodo.titulo)
					for grupo in Grupo.objects.all():
						nro_ests = 0
						prom = 0
						for st in Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year)):
							notas = st.getNotasActivas(self.year, get_tabla_notas(self.year), periodo_hasta = periodo.orden, min_lost = self.min_lost)
							other_asignaturas = []
							periodo_asignatura = next((comp for comp in notas["promedios"] if comp["pk"] == periodo.pk), False)
							if periodo_asignatura != None:
								prom += periodo_asignatura["valor"]
								nro_ests += 1

						if nro_ests == 0:
							nro_ests = 1
						prom = prom/nro_ests
						all_series.append({
							"titulo" : grupo.getGrupo(),
							"data" : [prom]
						})
				all_categorias = ["promedio"]

			data_return["grafica_titulo"] = grafica_titulo
			data_return["ver_estadisticas"] = True
			data_return["all_categorias"] = all_categorias
			data_return["all_series"] = all_series

		return data_return
