# Own libraries
from app.views.main import Main, MainTable
from app.models import *
from app.forms.students import *
from app.forms.planeador import *

# Django libraries
from django.db.models import Q
from django.views import View


class TemasView(MainTable, View):
	"""
		view of Grupos page
	"""
	page_title = "Planeador (temas)"
	register_name = "item"
	form_action = "/planeador/temas"
	model_object = PlaneadorTemas
	table_columns = dict(titulo = "Título", grado = "Grado", asignatura = "Asignatura", duracion = "Duración", estandar = "Estándar", competencia = "Competencia")
	return_edit_columns = [["grado", "pk"], ["asignatura", "pk"], "duracion", "titulo", "estandar", "competencia"]
	form_edit = editTemas
	form_add = editTemas
	view_aparte = False
	can_view = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planeador(temas)", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(duracion__icontains = value) | Q(grado__titulo__icontains = value) | Q(grado__sede__titulo__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(competencia__icontains = value) | Q(estandar__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False

class ActividadesView(MainTable, View):
	"""
		view of Grupos page
	"""
	page_title = "Planeador (actividades)"
	register_name = "item"
	form_action = "/planeador/actividades"
	model_object = PlaneadorActividades
	table_columns = dict(titulo = "Título", grado = "Grado", asignatura = "Asignatura", valoracion = "Valoración", metodologia = "Metodología", fecha__date_format = "Fecha")
	return_edit_columns = [["grado", "pk"], ["asignatura", "pk"], "valoracion", "titulo", "metodologia", "fecha"]
	form_edit = editActividad
	form_add = editActividad
	view_aparte = False
	can_view = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planeador (Actividades)", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(valoracion__titulo__icontains = value) | Q(grado__titulo__icontains = value) | Q(grado__sede__titulo__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(metodologia__icontains = value) | Q(fecha__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False


class EvaluacionView(MainTable, View):
	"""
		view of Grupos page
	"""
	page_title = "Planeador (evaluación)"
	register_name = "item"
	form_action = "/planeador/evaluacion"
	model_object = PlaneadorEvaluacion
	table_columns = dict(titulo = "Título", grado = "Grado", asignatura = "Asignatura", valoracion = "Valoración", tipo = "Tipo", fecha__date_format = "Fecha")
	return_edit_columns = [["grado", "pk"], ["asignatura", "pk"], ["valoracion", "pk"], "titulo", "tipo", "fecha"]
	form_edit = editEvaluacion
	form_add = editEvaluacion
	view_aparte = False
	can_view = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planeador (evaluación)", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(valoracion__titulo__icontains = value) | Q(grado__titulo__icontains = value) | Q(grado__sede__titulo__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(tipo__icontains = value) | Q(fecha__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False


class SuperacionView(MainTable, View):
	"""
		view of Grupos page
	"""
	page_title = "Planeador (superacion)"
	register_name = "item"
	form_action = "/planeador/superacion"
	model_object = PlaneadorSuperacion
	table_columns = dict(titulo = "Título", grado = "Grado", asignatura = "Asignatura", valoracion = "Valoración", tipo = "Tipo", fecha__date_format = "Fecha")
	return_edit_columns = [["grado", "pk"], ["asignatura", "pk"], "valoracion", "titulo", "tipo", "fecha"]
	form_edit = editSuperacion
	form_add = editSuperacion
	view_aparte = False
	can_view = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Planeador (superación)", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(valoracion__titulo__icontains = value) | Q(grado__titulo__icontains = value) | Q(grado__sede__titulo__icontains = value) | Q(asignatura__titulo__icontains = value) | Q(tipo__icontains = value) | Q(fecha__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False
