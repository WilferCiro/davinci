# Own libraries
from app.views.main import Main, MainTable
from app.models import *
from app.forms.students import *
from app.forms.settings import *

# Django libraries
from django.db.models import Q
from django.views import View

class ConfiguracionInformesView(Main, View):
	template = "form.html"
	page_title = "Config. Informes"

	def get_data(self, request, kwargs):

		forms = []
		form_comun = None

		inf = ConfiguracionInformes.objects.filter(year = self.year).last()
		form_comun = ComunInformes(instance = inf)
		if inf == None:
			inf = ConfiguracionInformes(year = self.year)
			inf.save()
			form_comun = ComunInformes()

		forms.append(dict(completa="si", href="first", tab="Comunes", form = form_comun))

		ayuda = "Puede agregar los siguientes items para ser reemplazados en los informes " +\
			"<ul>" +\
			"<li><b>*AÑO*</b> pone el año actual</li> " +\
			"<li><b>*CABECERA_GENERAL*</b> pone la cabecera general</li> " +\
			"<li><b>*CABECERA_BOLETINES*</b> pone la cabecera de los boletines</li> " +\
			"<li><b>*CONTENIDO*</b> pone el contenido</li> " +\
			"<li><b>*ESTUDIANTE.DATO*</b> pone el DATO del estudiante, ejemplo: ESTUDIANTE.NOMBRE, ESTUDIANTE.APELLIDO</li> " +\
			"</ul>"

		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Config. informes", "")
		return dict(forms = forms, ayuda=ayuda, tipo_form = "editData", action_form="/configuracion/institucion/")


	def _editData(self, request):
		if request.user.es_admin():
			inf = ConfiguracionInformes.objects.filter(year = self.year).last()
			form = ComunInformes(request.POST, instance = inf)
			if form.is_valid():
				form.save()
				return True
		return False

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False


class ConfiguracionInstitucionView(Main, View):
	template = "form.html"
	page_title = "Config. Institución"

	def get_data(self, request, kwargs):

		forms = []
		form_comun = None
		ayuda = None

		inf = ConfiguracionInstitucion.objects.filter(year = self.year).last()
		if inf != None:
			try:
				ayuda = "<center>Actual Escudo: <br /><img src='"+inf.escudo.url+"' width='240px'></center> "
			except:
				pass
			form_comun = ComunInstitucion(instance = inf)
		if inf == None:
			inf = ConfiguracionInstitucion(year = self.year)
			inf.save()
			form_comun = ComunInstitucion()

		forms.append(dict(href="first", tab="Configuración general", form = form_comun))

		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Config. institución", "")
		return dict(forms = forms, ayuda = ayuda, tipo_form = "editData", action_form="/configuracion/datos_institucion/")

	def _editData(self, request):
		if request.user.es_admin():
			inf = ConfiguracionInstitucion.objects.filter(year = self.year).last()
			form = ComunInstitucion(data = request.POST, files = request.FILES, instance = inf)
			if form.is_valid():
				form.save()
				return True
		return False

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

class ConfiguracionPeriodos(MainTable, View):
	"""
		view of Periodos page
	"""
	page_title = "Lista de periodos"
	register_name = "periodo"
	form_action = "/configuracion/periodos/"
	model_object = Periodo
	table_columns = dict(titulo = "Título", fecha_inicio = "Fecha Inicio", fecha_final = "Fecha Final", fecha_apertura = "Fecha Apertura", fecha_cierre = "Fecha Cierre", es_final = "Es Final")
	return_edit_columns = ["fecha_inicio", "fecha_final", "fecha_cierre", "fecha_apertura"]
	form_edit = editPeriodo

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Lista de periodos", "")

	def _getFilerTable(self, value):
		fil = Q(year = self.year)
		if value != "":
			fil = fil & (Q(titulo__icontains = value) | Q(fecha_inicio__icontains = value) | Q(fecha_final__icontains=value) | Q(fecha_apertura__icontains = value) | Q(fecha_cierre__icontains = value))
			return fil
		return fil

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True

class ConfiguracionSedes(MainTable, View):
	"""
		view of Periodos page
	"""
	page_title = "Lista de sedes"
	register_name = "sede"
	form_action = "/configuracion/sedes/"
	model_object = Sede
	table_columns = dict(titulo = "Título", dane = "Dane", hora_apertura = "Hora apertura", hora_cierre = "Hora Cierre", observaciones = "Observaciones")
	return_edit_columns = ["dane", "observaciones", "hora_apertura", "hora_cierre"]
	form_edit = editSede

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Lista de periodos", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(observaciones__icontains = value) | Q(dane=value) | Q(hora_apertura__icontains = value) | Q(hora_cierre__icontains = value))
			return fil
		return Q()
	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class ConfiguracionAsignaturas(MainTable, View):
	"""
		view of Students page
	"""
	page_title = "Lista de asignaturas"
	register_name = "asigatura"
	form_action = "/configuracion/asignaturas/"
	model_object = Asignatura
	table_columns = dict(titulo = "Título", abreviatura = "Abreviatura", promedio = "Promedio", comportamiento = "Comportamiento", porcentaje = "Porcentaje %", area_fundamental = "Área Fundamental")
	return_edit_columns = ["titulo", "abreviatura", "promedio", "comportamiento"]
	form_edit = editAsignatura

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Asignaturas", "")

	def _getFilerTable(self, value):
		fil = Q(year = self.year)
		if value != "":
			fil = fil & (Q(titulo__icontains = value) | Q(abreviatura=value) | Q(promedio__icontains = value) | Q(porcentaje__icontains = value) | Q(area_fundamental__titulo__icontains = value))
			return fil
		return fil

	def _preProccess(self, request):
		self.order_table = 1
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class ConfiguracionAreasFundamentales(MainTable, View):
	"""
		view of Áreas fundamentales page
	"""
	page_title = "Áreas fundamentales"
	register_name = "área fundamental"
	form_action = "/configuracion/areas_fundamentales/"
	model_object = AreaFundamental
	table_columns = dict(titulo = "Título", abreviatura = "Abreviatura")
	return_edit_columns = ["abreviatura"]
	form_edit = editAreaFundamental

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Áreas fundamentales", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(observaciones__icontains = value) | Q(abreviatura=value))
			return fil
		return Q()
	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class ConfiguracionGrados(MainTable, View):
	"""
		view of Grados page
	"""
	page_title = "Grados"
	register_name = "grado"
	form_action = "/configuracion/grados/"
	model_object = Grado
	table_columns = dict(titulo = "Título", valor_numerico = "Valor numérico", observaciones = "Observaciones")
	return_edit_columns = ["observaciones"]
	form_edit = editGrado

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Grados", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(observaciones__icontains = value) | Q(valor_numerico__icontains=value) | Q(sede__titulo__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class MensajesBlog(MainTable, View):
	"""
		view of mensajes blog
	"""
	page_title = "Mensajes del blog"
	register_name = "mensaje del blog"
	form_action = "/app_blog/mensajes/"
	model_object = ContactoBlog
	table_columns = dict(email = "E Mail", nombre = "Nombre", asunto = "Asunto", observaciones = "Observaciones", fecha__date_format="Fecha")
	delete_register = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Mensajes del blog", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(email__icontains = value) | Q(observaciones__icontains = value) | Q(nombre__icontains=value) | Q(asunto__icontains = value) | Q(fecha__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True


class ListadoGrupos(MainTable, View):
	"""
		view of Grupos page
	"""
	page_title = "Grupos"
	register_name = "grupo"
	form_action = "/configuracion/ListadoGrupos/"
	model_object = Grupo
	table_columns = dict(titulo = "Título", grado = "Grado", sede = "Sede", nro_estudiantes__format = "Nro. estudiantes", docente__format = "Director de grupo")
	view_aparte = True
	can_view = True

	def _getFormatRow(self, column, obj):
		if column == "nro_estudiantes":
			return Estudiante.objects.filter(Q(year = self.year) & Q(grupo = obj)).count()
		elif column == "docente":
			return obj.getDirector(self.year)
		return ""

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Grupos", "")

	def _getFilerTable(self, value):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(grado__titulo__icontains = value) | Q(sede__titulo__icontains = value))
			return fil
		return Q()

	def _preProccess(self, request):
		if self.user.es_admin():
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		if self.user.es_estudiante():
			self.can_view_page = False


class GrupoProfile(Main, View):
	template = "groups/profile.html"
	page_title = "Perfil de grupo"

	def get_data(self, request, kwargs):
		try:
			grupo_id = int(kwargs['pk'])
			grupo = Grupo.objects.get(pk = grupo_id)
		except Exception as e:
			self.can_view_page = False
			return {}

		estudiantes = Estudiante.objects.filter(Q(grupo = grupo) & Q(year = self.year) & Q(estado = Estudiante.E_ACTIVO))
		data_return = {}

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Perfil del grupo", "")

		lista_estudiantes = []
		for est in estudiantes:
			lista_estudiantes.append({
				"pk" : est.pk,
				"nombre" : est.nombre,
				"apellido" : est.apellido,
				"url" : est.getProfileURL()
			})

		datos_grupo = {
			"nombre" : grupo.getGrupo(),
			"pk" : grupo.pk,
			"grado" : grupo.getGrado(),
			"director" : grupo.getDirector(self.year),
			"nro_estudiantes" : len(lista_estudiantes)
		}
		periodos = []
		for per in Periodo.objects.filter(year = self.year).order_by("es_final", "orden"):
			if per.isActive():
				periodos.append({
					"pk" : per.pk,
					"titulo" : per.titulo
				})

		data_return["periodos"] = periodos

		data_return["datos_grupo"] = datos_grupo
		data_return["lista_estudiantes"] = lista_estudiantes

		directorObj = grupo.getDirectorObj(self.year)
		if directorObj == None:
			data_return["edit_director"] = editGrupo()
		else:
			data_return["edit_director"] = editGrupo(instance = directorObj)

		data_return["form_action"] = "/configuracion/ListadoGrupos/view/" + str(grupo.pk)

		return data_return


	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)
		if tipo_edit == "director" and object_id != None:
			try:
				grupo = Grupo.objects.get(pk = object_id)
				directorObj = grupo.getDirectorObj(self.year)
				if directorObj == None:
					form = editGrupo(data = request.POST)
					new_obj = form.save(commit=False)
					new_obj.grupo = grupo
					new_obj.save()
					form.save_m2m()
				else:
					form = editGrupo(instance = directorObj, data = request.POST)
					form.save()
				return True
			except Exception as e:
				print("-------", e)
				pass
		return False
