# Da Vinci

## TODO:
- [ ] Cambiar modelo de select por select ajax en formulario de tablas
- [ ] URGENTE: agregar botón de guardar notas, y selección de juicios especiales
- [ ] URGENTE: informes de estudiante en tabla de estudiantes
- [ ] Asistencia, retardos y faltas (leves, graves, gravísimas)
- [ ] Asistencia de docentes
- [ ] Ingreso de recuperaciones
- [ ] Ingreso de notas por estudiante individual
- [ ] Generación de informes académicos (Boletines, listado de notas, etc)
- [ ] Generación de formatos
- [ ] Ingreso de datos por excel (Notas, juicios valorativos, asignaciones)
- [ ] Planeador del estudiante
- [ ] Filtro de notas
- [ ] Restaurante (Opcional)
- [ ] Icfes (Opcional)
- [ ] Listado de estudiantes excel y PDF.


- [ ] Añadir historico estudiantes

## Instalación de dependencias
Instalar xampp, https://www.apachefriends.org/xampp-files/7.3.0/xampp-linux-x64-7.3.0-0-installer.run
```
sudo chmod +x xampp-linux-x64-7.3.0-0-installer.run
sudo ./xampp-linux-x64-7.3.0-0-installer.run
```
Dejar las opciones por defecto (para más información https://www.apachefriends.org/es/download.html),
Ejecutar el servidor local
```
sudo /opt/lampp/lampp start
```
Abrir la página "localhost/phpmyadmin" en el navegador, y crear base de datos llamada "davinci"
```
sudo pacman -S python-mysqlclient
sudo apt install libmysqlclient-dev
```
```
sudo pip3 install cffi django-cleanup django_select2 django-bootstrap-datepicker-plus xhtml2pdf django-ckeditor django-bootstrap4 mysqlclient openpyxl xlwt django-imagekit django_cron weasyprint django-google-maps python-dateutil django-cors-headers django-rest_framework openpyxl
```
```
python manage.py makemigrations
python manage.py migrate
```
Si es primera vez:
```
python manage.py createsuperuser
```
siempre que desee ejecutar el servidor
```
python manage.py runserver
```
