import json
from datetime import datetime

now = datetime.now() # current date and time
rta = input("Está seguro que desea aumentar la versión? y/n: ")
if rta == "y":
	new_rta = ""
	items = []
	while new_rta != "exit":
		new_rta = input("Escriba un cambio en la nueva versión ('exit' para salir):\n ")
		if new_rta != "exit":
			items.append(new_rta)
	with open('version_control.json') as json_file:
		data = json.load(json_file)
		json_file.close()
	last_version = float(data[0]["nro"])
	last_version += 0.1
	data_append = {
		"nro" : str(round(last_version, 2)),
		"fecha" : now.strftime("%d %b, %Y"),
		"items" : items
	}
	data.insert(0, data_append)
	with open('version_control.json', 'w') as outfile:
		json.dump(data, outfile, indent=2)

	print("Se agregaron los siguientes datos: ", data_append)
