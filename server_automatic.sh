#!/bin/bash
############ Este archivo debe estar en /home/user

# Variables
FOLDER_COPY="myApp_copy"
FOLDER_APP="myApp"
COPIA_SEGURIDAD="last_copy"

# Descargar nuevo código
rm -rf ${FOLDER_COPY}
git clone https://gitlab.com/WilferCiro/davinci.git ${FOLDER_COPY}

# Realizar copia de seguridad
rm -rf ${COPIA_SEGURIDAD}
cp -r ${FOLDER_APP} ${COPIA_SEGURIDAD}

# Eliminar código viejo a reemplazar
rm -rf ${FOLDER_APP}/app

# Copiar código nuevo
mv ${FOLDER_COPY}/app ${FOLDER_APP}
rm -rf ${FOLDER_COPY}

# Retornar los archivos Media
cp -rf ${COPIA_SEGURIDAD}/app/static/media ${FOLDER_APP}/app/static

# Realizar nuevas migraciones de django
cd ${FOLDER_APP}
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic
